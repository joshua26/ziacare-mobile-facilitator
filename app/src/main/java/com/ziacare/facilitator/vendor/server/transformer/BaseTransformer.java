package com.ziacare.facilitator.vendor.server.transformer;

import com.google.gson.annotations.SerializedName;
import com.ziacare.facilitator.data.model.api.ErrorModel;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class BaseTransformer {

    @SerializedName("msg")
    public String msg = "Insufficient balance ";

    @SerializedName("status")
    public Boolean status = false;

    @SerializedName("status_code")
    public String status_code = "RETROFIT_FAILED";

    @SerializedName("token")
    public String token = "";

    @SerializedName("total")
    public int total = 0 ;

    @SerializedName("new_token")
    public String new_token = "";

    @SerializedName("web_checkout")
    public String webCheckout = "";

    @SerializedName("has_morepages")
    public Boolean has_morepages = false;

    @SerializedName("errors")
    public ErrorModel error;

    @SerializedName("user_code")
    public String user_code;

    @SerializedName("new_password")
    public String new_password;

    @SerializedName("hasRequirements")
    public boolean hasRequirements(){
        return false;
    }

    public boolean checkEmpty(Object obj){
        return obj !=  null;
    }
}
