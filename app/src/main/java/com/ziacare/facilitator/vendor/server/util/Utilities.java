package com.ziacare.facilitator.vendor.server.util;

import android.os.Build;
import android.os.StrictMode;

import com.ziacare.facilitator.vendor.android.java.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Utilities {

    public static void getInternet() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public static String readKernelVersion() {
        try {
            Process p = Runtime.getRuntime().exec("uname -a");
            InputStream is = null;
            if (p.waitFor() == 0) {
                is = p.getInputStream();
            } else {
                is = p.getErrorStream();
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(is), 1024);
            String line = br.readLine();
            br.close();
            return line;
        } catch (Exception ex) {
            return "ERROR: " + ex.getMessage();
        }
    }


    public static String getDeviceModelNumber() {
        String manufacturer = Build.VERSION.CODENAME;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private static  String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }
    // get System info.
    public static String OSNAME = System.getProperty("os.name");
    public static String OSVERSION = System.getProperty("os.version");
    public static String RELEASE = Build.VERSION.RELEASE;
    public static String DEVICE = Build.DEVICE;
    public static String MODEL = Build.MODEL;
    public static String PRODUCT = Build.PRODUCT;
    public static String BRAND = Build.BRAND;
    public static String DISPLAY = Build.DISPLAY;
    public static String CPU_ABI = Build.CPU_ABI;
    public static String CPU_ABI2 = Build.CPU_ABI2;
    public static String UNKNOWN = Build.UNKNOWN;
    public static String HARDWARE = Build.HARDWARE;
    public static String ID = Build.ID;
    public static String MANUFACTURER = Build.MANUFACTURER;
    public static String SERIAL = Build.SERIAL;
    public static String USER = Build.USER;
    public static String HOST = Build.HOST;

    public static void logAll(){
        Log.d("Device info",
                "Release "+ RELEASE+"\n"+
                "DEVICE "+ DEVICE+"\n"+
                "MODEL "+ MODEL+"\n"+
                "BRAND "+ BRAND+"\n"+
                        "ID " + ID + "\n"+
                "SERIALNUMBER "+ SERIAL);
    }

}
