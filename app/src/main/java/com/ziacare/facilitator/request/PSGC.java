package com.ziacare.facilitator.request;

import android.content.Context;

import com.ziacare.facilitator.config.Keys;
import com.ziacare.facilitator.config.Url;
import com.ziacare.facilitator.data.model.api.PSGCModel;
import com.ziacare.facilitator.vendor.server.request.APIRequest;
import com.ziacare.facilitator.vendor.server.request.APIResponse;
import com.ziacare.facilitator.vendor.server.transformer.CollectionTransformer;
import com.ziacare.facilitator.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public class PSGC {

    public static PSGC getDefault(){return new PSGC();}

    public void getProvince(Context context){
        APIRequest apiRequest = new APIRequest<CollectionTransformer<PSGCModel>>(context,Url.PSGC_URL) {
            @Override
            public Call<CollectionTransformer<PSGCModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestPSGCTransformer(Url.getProvince(), Keys.PSGC_AUTHORIZATION, getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ProvinceResponse(this));
            }
        };

        apiRequest
                .showDefaultProgressDialog("Loading Province")
                .execute();
    }

    public void getProvinceBySKU(Context context, String sku){
        APIRequest apiRequest = new APIRequest<SingleTransformer<PSGCModel>>(context,Url.PSGC_URL) {
            @Override
            public Call<SingleTransformer<PSGCModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestPSGCBySKUTransformer(Url.getProvinceInfo(), Keys.PSGC_AUTHORIZATION, getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ProvinceBySKUResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.REFERENCE,sku)
                .addParameter(Keys.TYPE,"sku")
                .showDefaultProgressDialog("Loading Province")
                .execute();
    }

    public void getMunicipality(Context context, int reference){
        APIRequest apiRequest = new APIRequest<CollectionTransformer<PSGCModel>>(context,Url.PSGC_URL) {
            @Override
            public Call<CollectionTransformer<PSGCModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestPSGCTransformer(Url.getMunicipality(), Keys.PSGC_AUTHORIZATION, getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new MunicipalityResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.REFERENCE,reference)
                .addParameter(Keys.TYPE,"id")
                .showDefaultProgressDialog("Loading City")
                .execute();
    }

    public void getMunicipalityBySKU(Context context, String sku){
        APIRequest apiRequest = new APIRequest<CollectionTransformer<PSGCModel>>(context,Url.PSGC_URL) {
            @Override
            public Call<CollectionTransformer<PSGCModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestPSGCTransformer(Url.getMunicipality(), Keys.PSGC_AUTHORIZATION, getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new MunicipalityResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.REFERENCE,sku)
                .addParameter(Keys.TYPE,"sku")
                .showDefaultProgressDialog("Loading City")
                .execute();
    }

    public interface RequestService{
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<PSGCModel>> requestPSGCTransformer(@Path(value = "p", encoded = true) String p, @Header("PSGC-Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<PSGCModel>> requestPSGCBySKUTransformer(@Path(value = "p", encoded = true) String p, @Header("PSGC-Authorization") String authorization, @Part List<MultipartBody.Part> part);

    }

    public class ProvinceResponse extends APIResponse<CollectionTransformer<PSGCModel>> {
        public ProvinceResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class ProvinceBySKUResponse extends APIResponse<SingleTransformer<PSGCModel>> {
        public ProvinceBySKUResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class MunicipalityResponse extends APIResponse<CollectionTransformer<PSGCModel>> {
        public MunicipalityResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
