package com.ziacare.facilitator.request;

import android.content.Context;

import com.ziacare.facilitator.config.Keys;
import com.ziacare.facilitator.config.Url;
import com.ziacare.facilitator.data.model.api.CardModel;
import com.ziacare.facilitator.data.model.api.TransactionHistoryModel;
import com.ziacare.facilitator.data.model.api.UserModel;
import com.ziacare.facilitator.data.preference.UserData;
import com.ziacare.facilitator.vendor.server.request.APIRequest;
import com.ziacare.facilitator.vendor.server.request.APIResponse;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.transformer.CollectionTransformer;
import com.ziacare.facilitator.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public class Beneficiary {

    public static Beneficiary getDefault(){
        return new Beneficiary();
    }

    public void scanRefNo(Context context, String user_code) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<TransactionHistoryModel>>(context) {
            @Override
            public Call<SingleTransformer<TransactionHistoryModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.scanRefNo(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ScanUserCodeResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.USERCODE, user_code)
                .showDefaultProgressDialog("Loading...")
                .execute();

    }

    public void scanRefNo(Context context, String user_code, String province_sku) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<TransactionHistoryModel>>(context) {
            @Override
            public Call<SingleTransformer<TransactionHistoryModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.scanRefNo(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ScanUserCodeResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.USERCODE, user_code)
                .addParameter(Keys.PROVINCE_SKU,province_sku)
                .showDefaultProgressDialog("Loading...")
                .execute();

    }

    public void generateNewPassword(Context context, String user_code) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.generateNewPassword(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new GenerateNewPasswordResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.USERCODE, user_code)
                .showDefaultProgressDialog("Loading...")
                .execute();

    }

    public void editRecord(Context context, String user_code, String city_sku, String city_name ,String brgy_sku, String brgy_name, String street_name, String zipcode, String phone_number, String email, String tin_no, String sss_no, String phic_no){
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.editRecord(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new EditRecordResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.USERCODE, user_code)
                .addParameter(Keys.CITY_SKU,city_sku)
                .addParameter(Keys.CITY_NAME,city_name)
                .addParameter(Keys.BRGY_SKU,brgy_sku)
                .addParameter(Keys.BRGY_NAME,brgy_name)
                .addParameter(Keys.STREET_NAME,street_name)
                .addParameter(Keys.ZIPCODE,zipcode)
                .addParameter(Keys.PHONE_NUMBER,phone_number)
                .addParameter(Keys.EMAIL,email)
                .addParameter(Keys.TIN_NO,tin_no)
                .addParameter(Keys.SSS_NO,sss_no)
                .addParameter(Keys.PHIC_NO,phic_no)
                .showDefaultProgressDialog("Updating Record...")
                .execute();
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<TransactionHistoryModel>> requestSingleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<CardModel>> requestCollectionTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestBaseTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

    }

    public class ScanUserCodeResponse extends APIResponse<SingleTransformer<TransactionHistoryModel>> {
        public ScanUserCodeResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }


    public class GenerateNewPasswordResponse extends APIResponse<BaseTransformer> {
        public GenerateNewPasswordResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class EditRecordResponse extends APIResponse<BaseTransformer> {
        public EditRecordResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
