package com.ziacare.facilitator.request;

import android.content.Context;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ziacare.facilitator.config.Keys;
import com.ziacare.facilitator.config.Url;
import com.ziacare.facilitator.data.model.api.CardModel;
import com.ziacare.facilitator.data.model.api.TransactionHistoryModel;
import com.ziacare.facilitator.data.model.api.UserModel;
import com.ziacare.facilitator.data.preference.UserData;
import com.ziacare.facilitator.vendor.server.request.APIRequest;
import com.ziacare.facilitator.vendor.server.request.APIResponse;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.transformer.CollectionTransformer;
import com.ziacare.facilitator.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public class Cards {

    public static Cards getDefault(){
        return new Cards();
    }

    public void scanCard(Context context, String reference_code) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getScanCard(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ScanCardResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.REFERENCE_CODE, reference_code)
                .showDefaultProgressDialog("Loading...")
                .execute();

    }

    public void cardList(Context context, SwipeRefreshLayout swipeRefreshLayout){
        APIRequest apiRequest = new APIRequest<CollectionTransformer<CardModel>>(context){
            @Override
            public Call<CollectionTransformer<CardModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionTransformer(Url.getListOfCards(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {EventBus.getDefault().post(new CardListResponse(this)); }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(0)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true)
                .execute();
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserModel>> requestSingleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<CardModel>> requestCollectionTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestBaseTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

    }

    public class ScanCardResponse extends APIResponse<BaseTransformer> {
        public ScanCardResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class CardListResponse extends APIResponse<CollectionTransformer<CardModel>> {
        public CardListResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
