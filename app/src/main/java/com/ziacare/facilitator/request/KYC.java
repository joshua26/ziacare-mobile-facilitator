package com.ziacare.facilitator.request;

import android.content.Context;

import com.ziacare.facilitator.config.Keys;
import com.ziacare.facilitator.config.Url;
import com.ziacare.facilitator.data.model.api.RequestModel;
import com.ziacare.facilitator.data.model.api.UserModel;
import com.ziacare.facilitator.data.preference.UserData;
import com.ziacare.facilitator.vendor.server.request.APIRequest;
import com.ziacare.facilitator.vendor.server.request.APIResponse;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.transformer.CollectionTransformer;
import com.ziacare.facilitator.vendor.server.transformer.SingleTransformer;
import com.ziacare.facilitator.vendor.server.util.Utilities;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public class KYC {

    public static KYC getDefault(){ return new KYC();}

    public void getValidIDList(Context context) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<RequestModel>>(context) {
            @Override
            public Call<CollectionTransformer<RequestModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestList(Url.getValidIDList(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ValidIDListResponse(this));
            }
        };

        apiRequest
                .showDefaultProgressDialog("Loading Valid ID List...")
                .execute();
    }

    public void validateID(Context context, File id, String documentType, String user_code) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestRegisterTransformer(Url.getValidateID(), Utilities.ID, getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ValidateIDResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.IMAGE, id)
                .addParameter(Keys.DOCUMENT_TYPE, documentType)
                .addParameter(Keys.USERCODE, user_code)
                .showDefaultProgressDialog("Validating ID...")
                .execute();
    }

    public void validateSelfie(Context context, File selfie , String user_code) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestRegisterTransformer(Url.getValidateSelfie(), Utilities.ID,getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ValidateSelfieResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.IMAGE, selfie)
                .addParameter(Keys.USERCODE, user_code)
                .showDefaultProgressDialog("Validating Selfie...")
                .execute();
    }

    public void KYC(Context context, File selfie, File id, String user_code) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestRegisterTransformer(Url.getKYC(), Utilities.ID ,getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new KYCResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.SELFIE, selfie)
                .addParameter(Keys.VALID_ID, id)
                .addParameter(Keys.USERCODE, user_code)
                .showDefaultProgressDialog("Verifying...")
                .execute();
    }

    public void uploadID(Context context, File id, String documentType, String user_code) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.uploadID(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UploadIDResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.IMAGE, id)
                .addParameter(Keys.ID_TYPE, documentType)
                .addParameter(Keys.USERCODE, user_code)
                .showDefaultProgressDialog("Uploading ID...")
                .execute();
    }

    public void uploadSelfie(Context context, File smile, File left, File right , File video,String user_code) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.uploadSelfie(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UploadSelfieResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.FRONT_PIC, smile)
                .addParameter(Keys.LEFT_PIC, left)
                .addParameter(Keys.RIGHT_PIC,right)
                .addParameter(Keys.VIDEO, video)
                .addParameter(Keys.USERCODE, user_code)
                .showDefaultProgressDialog("Uploading Selfies...")
                .execute();
    }

    public interface RequestService {

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserModel>> requestSingleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<RequestModel>> requestList(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestBaseTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestRegisterTransformer(@Path(value = "p", encoded = true) String p, @Header("device-id") String device_id, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


    }

    public class ValidIDListResponse extends APIResponse<CollectionTransformer<RequestModel>> {
        public ValidIDListResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class ValidateIDResponse extends APIResponse<BaseTransformer> {
        public ValidateIDResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class ValidateSelfieResponse extends APIResponse<BaseTransformer> {
        public ValidateSelfieResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class KYCResponse extends APIResponse<BaseTransformer> {
        public KYCResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class UploadIDResponse extends APIResponse<BaseTransformer> {
        public UploadIDResponse(APIRequest apiRequest) { super(apiRequest); }
    }

    public class UploadSelfieResponse extends APIResponse<BaseTransformer> {
        public UploadSelfieResponse(APIRequest apiRequest) { super(apiRequest); }
    }

}
