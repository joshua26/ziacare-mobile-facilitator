package com.ziacare.facilitator.request;

import android.content.Context;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ziacare.facilitator.config.Keys;
import com.ziacare.facilitator.config.Url;
import com.ziacare.facilitator.data.model.api.TransactionHistoryModel;
import com.ziacare.facilitator.data.model.api.UserModel;
import com.ziacare.facilitator.data.preference.UserData;
import com.ziacare.facilitator.vendor.server.request.APIRequest;
import com.ziacare.facilitator.vendor.server.request.APIResponse;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.transformer.CollectionTransformer;
import com.ziacare.facilitator.vendor.server.transformer.SingleTransformer;
import com.ziacare.facilitator.vendor.server.util.Utilities;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Auth {

    public static Auth getDefault(){
        return new Auth();
    }


    public void login(Context context, String email, String password) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getLogin(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LoginResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.EMAIL, email)
                .addParameter(Keys.PASSWORD, password)
                .showDefaultProgressDialog("Logging in...")
                .execute();
    }

    public void login(Context context, String email, String password, String province_sku) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getLogin(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LoginResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.EMAIL, email)
                .addParameter(Keys.PASSWORD, password)
                .addParameter(Keys.PROVINCE_SKU,province_sku)
                .showDefaultProgressDialog("Logging in...")
                .execute();
    }

    public void forgot(Context context, String phone) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestRegisterTransformer(Url.getForgotPass(), Utilities.ID, "", getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ForgotResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.PHONE_NUMBER, phone)
                .showDefaultProgressDialog("Submitting ...")
                .execute();
    }

    public void forgotpassOTP(Context context, String otp, String phone_number) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestRegisterTransformer(Url.getForgotPassOTP(), Utilities.ID, "",getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ForgotOTPResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.OTP, otp)
                .addParameter(Keys.PHONE_NUMBER, phone_number)
                .showDefaultProgressDialog("Verifying...")
                .execute();
    }

    public void resetPassword(Context context, String phone_number, String password, String passwordConfirm) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestRegisterTransformer(Url.getResetPassword(), Utilities.ID, "",getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ResetPasswordResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.PHONE_NUMBER, phone_number)
                .addParameter(Keys.PASSWORD,password)
                .addParameter(Keys.PASSWORD_CONFIRM,passwordConfirm)
                .showDefaultProgressDialog("Loading...")
                .execute();
    }


    public void logout(Context context, String userCode) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getLogout(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LogoutResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.USERCODE, userCode)
                .showDefaultProgressDialog("Logging out...")
                .execute();
    }


    public void refreshToken(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getRefreshToken(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RefreshTokenResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();

    }


    public void transactionList(Context context, String userCode, SwipeRefreshLayout swipeRefreshLayout){
        APIRequest apiRequest = new APIRequest<CollectionTransformer<TransactionHistoryModel>>(context){
            @Override
            public Call<CollectionTransformer<TransactionHistoryModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionTransformer(Url.getListOfRegistered(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {EventBus.getDefault().post(new TransListResponse(this)); }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                //.addParameter(Keys.USERCODE,userCode)
                .setPerPage(0)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true)
                .execute();
    }

    public APIRequest registerFacilitator(Context context) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getRegisterFacilitator(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RegisterResponse(this));
            }
        };

        apiRequest
                .showDefaultProgressDialog("Registering...");

        return apiRequest;
    }

    public void getprofileScan(Context context, String user_code, String ref_code) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getAttachCard(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ProfileScanResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.REFERENCE_CODE, ref_code)
                .addParameter(Keys.USERCODE, user_code)
                .showDefaultProgressDialog("Verifying...")
                .execute();
    }

    public APIRequest updateProfile(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getUpdateProfile(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UpdateProfileResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Saving Changes...");
        return apiRequest;
    }

    public void update_avatar(Context context, File file) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getUpdateAvatar(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UpdateAvatarResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.AVATAR, file)
                .showDefaultProgressDialog("Updating Avatar...")
                .execute();

    }

    public void update_pass(Context context, String currentPass, String password, String passConfirm) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getUpdatePassword(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UpdateAvatarResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.PASSWORD, password)
                .addParameter(Keys.CURRENT_PASSWORD, currentPass)
                .addParameter(Keys.PASSWORD_CONFIRM, passConfirm)
                .showDefaultProgressDialog("Updating password...")
                .execute();

    }

    public void myProfile(Context context){
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getMyProfile(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new MyProfileResponse(this));
            }
        };
        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Loading . . .")
                .execute();
    }

    public APIRequest validate(Context context) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestRegisterTransformer(Url.getValidate(),  Utilities.ID, getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ValidateCodeResponse(this));
            }
        };

        apiRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION));
//        apiRequest
//                .showDefaultProgressDialog("Validating ...");

        return apiRequest;
    }

    public void register(Context context, String firstname, String middlename, String lastname, String gender, String birthdate, String province,
                         String provinceSku, String city, String citySku, String brgy, String street, String zipcode,
                         String phoneNumber, String email, String password, String passwordConfirm,
                         String tin, String sss, String phic) {

        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestRegisterTransformer(Url.getCreateAccount(), Utilities.ID, getAuthorization() ,getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RegisterResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.FNAME, firstname)
                .addParameter(Keys.LNAME, lastname)
                .addParameter(Keys.GENDER, gender)
                .addParameter(Keys.BIRTHDATE, birthdate)
                .addParameter(Keys.PROVINCE_NAME, province)
                .addParameter(Keys.PROVINCE_SKU, provinceSku)
                .addParameter(Keys.CITY_NAME, city)
                .addParameter(Keys.CITY_SKU, citySku)
                .addParameter(Keys.BRGY_NAME, brgy)
                .addParameter(Keys.STREET_NAME, street)
                .addParameter(Keys.ZIPCODE, zipcode)
                .addParameter(Keys.PHONE_NUMBER, phoneNumber)
                .addParameter(Keys.PASSWORD, password)
                .addParameter(Keys.PASSWORD_CONFIRM, passwordConfirm);
        if (middlename != null) {
            apiRequest.addParameter(Keys.MNAME, middlename);
        }
        if(tin != null){
            apiRequest.addParameter(Keys.TIN_NO,tin);
        }
        if(email != null){
            apiRequest.addParameter(Keys.EMAIL,email);
        }
        if(sss != null){
            apiRequest.addParameter(Keys.SSS_NO,sss);
        }
        if(phic != null){
            apiRequest.addParameter(Keys.PHIC_NO,phic);
        }
        apiRequest.showDefaultProgressDialog("Registering...")
                .execute();
    }

    public void OTP(Context context, String otp, String phone_number) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestRegisterTransformer2(Url.getOTPVerification(), Utilities.ID, getAuthorization() ,getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new OTPResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.OTP, otp)
                .addParameter(Keys.PHONE_NUMBER, phone_number)
                .showDefaultProgressDialog("Verifying...")
                .execute();
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserModel>> requestSingleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<TransactionHistoryModel>> requestCollectionTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestBaseTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestRegisterTransformer(@Path(value = "p", encoded = true) String p, @Header("device-id") String device_id, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserModel>> requestRegisterTransformer2(@Path(value = "p", encoded = true) String p, @Header("device-id") String device_id, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


    }


    public class LoginResponse extends APIResponse<SingleTransformer<UserModel>> {
        public LoginResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class LogoutResponse extends APIResponse<BaseTransformer> {
        public LogoutResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class ForgotResponse extends APIResponse<BaseTransformer> {
        public ForgotResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class RegisterResponse extends APIResponse<BaseTransformer> {
        public RegisterResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class RefreshTokenResponse extends APIResponse<SingleTransformer<UserModel>> {
        public RefreshTokenResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class TransListResponse extends APIResponse<CollectionTransformer<TransactionHistoryModel>> {
        public TransListResponse(APIRequest apiRequest) { super(apiRequest); }
    }

    public class ProfileScanResponse extends APIResponse<SingleTransformer<UserModel>> {
        public ProfileScanResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class UpdateProfileResponse extends APIResponse<SingleTransformer<UserModel>> {
        public UpdateProfileResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class UpdateAvatarResponse extends APIResponse<BaseTransformer> {
        public UpdateAvatarResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class MyProfileResponse extends APIResponse<SingleTransformer<UserModel>> {
        public MyProfileResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class ValidateCodeResponse extends APIResponse<SingleTransformer<UserModel>> {
        public ValidateCodeResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class OTPResponse extends APIResponse<SingleTransformer<UserModel>> {
        public OTPResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class ResetPasswordResponse extends APIResponse<BaseTransformer> {
        public ResetPasswordResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class ForgotOTPResponse extends APIResponse<BaseTransformer> {
        public ForgotOTPResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

}
