package com.ziacare.facilitator.android.fragment.profile;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.ProfileActivity;
import com.ziacare.facilitator.android.dialog.CalendarDialog;
import com.ziacare.facilitator.config.Keys;
import com.ziacare.facilitator.data.model.api.UserModel;
import com.ziacare.facilitator.data.preference.UserData;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.ImageManager;
import com.ziacare.facilitator.vendor.android.java.Log;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.transformer.SingleTransformer;
import com.ziacare.facilitator.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;

/**
 * Created by Evanson on 2019-10-02.
 */

public class UpdateProfileFragment extends BaseFragment implements BSImagePicker.OnSingleImageSelectedListener, ImageManager.Callback,CalendarDialog.DateTimePickerListener,
        View.OnClickListener{
    public static final String TAG = UpdateProfileFragment.class.getName().toString();

    private ProfileActivity activity;

    @BindView(R.id.fnameET)             EditText fnameET;
    @BindView(R.id.mnameET)             EditText mnameET;
    @BindView(R.id.lnameET)             EditText lnameET;
    @BindView(R.id.emailET)             EditText emailET;
    @BindView(R.id.emgNameET)           EditText emgNameET;
    @BindView(R.id.emgNoET)             EditText emgNoET;
    @BindView(R.id.emgRelationET)       EditText emgRelationET;
    @BindView(R.id.phoneET)             EditText phoneET;
    @BindView(R.id.profileIMG)          ImageView profileIMG;
    @BindView(R.id.dobTXT)              TextView dobTXT;
    @BindView(R.id.uploadBTN)           TextView uploadBTN;
    @BindView(R.id.streetET)                        EditText streetET;
    @BindView(R.id.zipcodeET)                       EditText zipcodeET;
    @BindView(R.id.tinET)                           EditText tinET;
    @BindView(R.id.sssET)                           EditText sssET;
    @BindView(R.id.phicET)                          EditText phicET;
    @BindView(R.id.cityET)                          EditText cityET;
    @BindView(R.id.brgyET)                          EditText brgyET;
    @BindView(R.id.telNoET)                          EditText telNoET;
    @BindView(R.id.saveBTN)                         TextView saveBTN;



    public static UpdateProfileFragment newInstance() {
        UpdateProfileFragment fragment = new UpdateProfileFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_update_profile;
    }

    @Override
    public void onViewReady() {
        activity = (ProfileActivity) getContext();
        activity.setSettingsTitle("Update Profile");
        saveBTN.setOnClickListener(this);
        uploadBTN.setOnClickListener(this);
        profileIMG.setOnClickListener(this);
        dobTXT.setOnClickListener(this);
        getProfile();

        emgNoET.addTextChangedListener(new TextWatcher() {
            int prevL = 0;
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                prevL = emgNoET.getText().toString().length();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int length = editable.length();
                if((prevL < length) && (length == 3 || length == 7 || length == 11)) {
                    String data = emgNoET.getText().toString();
                    emgNoET.setText(data +"-");
                    emgNoET.setSelection(length+1);
                }
            }
        });


        phoneET.addTextChangedListener(new TextWatcher() {
            int prevL = 0;
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                prevL = phoneET.getText().toString().length();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int length = editable.length();
                if((prevL < length) && (length == 3 || length == 7 || length == 11)) {
                    String data = phoneET.getText().toString();
                    phoneET.setText(data +"-");
                    phoneET.setSelection(length+1);
                }
            }
        });


        telNoET.addTextChangedListener(new TextWatcher() {
            int prevL = 0;
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                prevL = telNoET.getText().toString().length();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int length = editable.length();
                if((prevL < length) && (length == 3 )) {
                    String data = telNoET.getText().toString();
                    telNoET.setText(data +"-");
                    telNoET.setSelection(length+1);
                }
            }
        });
    }

    public void getProfile(){
        Auth.getDefault().myProfile(activity);
    }

    private void setDetails(UserModel data) {
        Glide.with(activity)
                .load(data.avatar.thumb_path)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.user_ic)
                        .error(R.drawable.user_ic))
                .into(profileIMG);
        fnameET.setText(data.firstname);
        mnameET.setText(data.middlename);
        lnameET.setText(data.lastname);
        dobTXT.setText(data.birthday);
        emailET.setText(data.email);
        phoneET.setText(data.phone_number);
        emgNameET.setText(data.emergency_name);
        emgNoET.setText(data.emergency_contact);
        emgRelationET.setText(data.emergency_relationship);
        streetET.setText(data.streetName);
        zipcodeET.setText(data.zipcode);
        tinET.setText(data.tinNo);
        sssET.setText(data.sssNo);
        phicET.setText(data.phicNo);
        cityET.setText(data.city);
        brgyET.setText(data.brgy);
        telNoET.setText(data.tel_no);
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void openFileChooser() {
        BSImagePicker pickerDialog = new BSImagePicker.Builder("com.ziacare.facilitator.fileprovider").build();
        pickerDialog.show(getChildFragmentManager(), "picker");
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);
            Bitmap reducedSize = sizeReducer(bitmap,800);
            ImageManager.getFileFromBitmap(activity, reducedSize, "image1", this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File file;
    @Override
    public void success(File file) {
        this.file = file;
        Auth.getDefault().update_avatar(activity,file);
    }

    @Subscribe
    public void onResponse(Auth.UpdateAvatarResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if (baseTransformer.status){
            Glide.with(activity)
                    .load(file)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.user_ic)
                            .error(R.drawable.user_ic))
                    .into(profileIMG);
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }


    @Subscribe
    public void onResponse(Auth.UpdateProfileResponse response){
        SingleTransformer<UserModel> baseTransformer = response.getData(SingleTransformer.class);
        if (baseTransformer.status){
            activity.onBackPressed();
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
        }

        else {
            ToastMessage.show(activity, baseTransformer.msg, ToastMessage.Status.FAILED);
            if (!ErrorResponseManger.first(baseTransformer.error.emergency_contact).equals("")){
                emgNoET.setError(ErrorResponseManger.first(baseTransformer.error.emergency_contact));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.birthdate).equals("")){
                dobTXT.setError(ErrorResponseManger.first(baseTransformer.error.birthdate));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.phone_number).equals("")){
               // phoneET.setError(ErrorResponseManger.first(baseTransformer.error.phone_number));
              telNoET.setHint("Ex: Tel no: 888888");
                phoneET.setError("Your Phone number is invalid format or already taken");
                phoneET.setHint(" Ex: 09123456789");
                emgNoET.setHint(" Ex: 09123456789");
            }
            if (!ErrorResponseManger.first(baseTransformer.error.email).equals("")){
                emailET.setError(ErrorResponseManger.first(baseTransformer.error.email));
            }if (!ErrorResponseManger.first(baseTransformer.error.firstname).equals("")){
                fnameET.setError(ErrorResponseManger.first(baseTransformer.error.firstname));
            }if (!ErrorResponseManger.first(baseTransformer.error.middlename).equals("")){
                mnameET.setError(ErrorResponseManger.first(baseTransformer.error.middlename));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.lastname).equals("")){
                lnameET.setError(ErrorResponseManger.first(baseTransformer.error.lastname));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.street_name).equals("")){
                streetET.setError(ErrorResponseManger.first(baseTransformer.error.street_name));
            }if (!ErrorResponseManger.first(baseTransformer.error.zipcode).equals("")){
                zipcodeET.setError(ErrorResponseManger.first(baseTransformer.error.zipcode));
            }if (!ErrorResponseManger.first(baseTransformer.error.tin_no).equals("")){
                tinET.setError(ErrorResponseManger.first(baseTransformer.error.tin_no));
            }if (!ErrorResponseManger.first(baseTransformer.error.sss_no).equals("")){
                sssET.setError(ErrorResponseManger.first(baseTransformer.error.sss_no));
            }if (!ErrorResponseManger.first(baseTransformer.error.phic_no).equals("")){
                phicET.setError(ErrorResponseManger.first(baseTransformer.error.phic_no));
            }if (!ErrorResponseManger.first(baseTransformer.error.city).equals("")){
                cityET.setError(ErrorResponseManger.first(baseTransformer.error.city));
            }if (!ErrorResponseManger.first(baseTransformer.error.brgy).equals("")){
                brgyET.setError(ErrorResponseManger.first(baseTransformer.error.brgy));
            }
        }
    }

    @Subscribe
    public void onResponse(Auth.MyProfileResponse response){
        SingleTransformer<UserModel> singleTransformer = response.getData(SingleTransformer.class);
        if (singleTransformer.status){
            setDetails(singleTransformer.data);
        } else {
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void forDisplay(String date) { dobTXT.setText(date); }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.dobTXT:
                CalendarDialog.newInstance(this::forDisplay).show(getChildFragmentManager(), TAG);
                break;
            case R.id.uploadBTN:
                openFileChooser();
                break;
            case R.id.profileIMG:
                openFileChooser();
                break;
            case R.id.saveBTN:

                attemptSave();

                break;
        }
    }

    private void attemptSave(){

        Auth.getDefault().updateProfile(getContext())
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.FIRSTNAME, fnameET.getText().toString().trim())
                .addParameter(Keys.MIDDLENAME, mnameET.getText().toString().trim())
                .addParameter(Keys.LASTNAME, lnameET.getText().toString().trim())
                .addParameter(Keys.EMAIL, emailET.getText().toString().trim())
                .addParameter(Keys.PHONE_NUMBER, phoneET.getText().toString().trim())
                .addParameter(Keys.TEL_NO, telNoET.getText().toString().trim())
                //.addParameter(Keys.EMERGENCY_NAME, emgNameET.getText().toString().trim())
                //.addParameter(Keys.EMERGENCY_CONTACT, emgNoET.getText().toString().trim())
                //.addParameter(Keys.EMERGENCY_RELATIONSHIP, emgRelationET.getText().toString().trim())
                .addParameter(Keys.BIRTHDATE, dobTXT.getText().toString().trim())
                //.addParameter(Keys.CITY,cityET.getText().toString().trim())
                //.addParameter(Keys.BRGY,brgyET.getText().toString().trim())
                .addParameter(Keys.STREET_NAME,streetET.getText().toString().trim())
                .addParameter(Keys.ZIPCODE,zipcodeET.getText().toString().trim())
                .addParameter(Keys.TIN_NO,tinET.getText().toString().trim())
                .addParameter(Keys.SSS_NO,sssET.getText().toString().trim())
                .addParameter(Keys.PHIC_NO,phicET.getText().toString().trim())
                .execute();

    }

    public static Bitmap sizeReducer(Bitmap image, int maxResolution) {

        if (maxResolution <= 0)
            return image;

        int width = image.getWidth();
        int height = image.getHeight();
        float ratio = (width >= height) ? (float)maxResolution/width :(float)maxResolution/height;

        int finalWidth = (int) ((float)width * ratio);
        int finalHeight = (int) ((float)height * ratio);

        image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);

        if (image.getWidth() == image.getHeight())
            return image;
        else {
            //fit height and width
            int left = 0;
            int top = 0;

            if(image.getWidth() != maxResolution)
                left = (maxResolution - image.getWidth()) / 2;

            if(image.getHeight() != maxResolution)
                top = (maxResolution - image.getHeight()) / 2;

            Bitmap bitmap = Bitmap.createBitmap(maxResolution, maxResolution, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            canvas.drawBitmap(image, left, top, null);
            canvas.save();
            canvas.restore();

            return  bitmap;
        }
    }
}