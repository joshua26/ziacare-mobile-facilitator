package com.ziacare.facilitator.android.dialog;

import android.content.Context;
import android.os.Build;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.ziacare.facilitator.R;

import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.vendor.android.base.BaseDialog;
import com.ziacare.facilitator.vendor.android.java.OtpEditText;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

public class OTPDialog extends BaseDialog {
    public static final String TAG = OTPDialog.class.getName().toString();

    private Callback callback;
    private Context context;

    @BindView(R.id.OTPET)
    OtpEditText OTPET;
    @BindView(R.id.mobileNumberTXT)
    TextView mobileNumberTXT;
    @BindView(R.id.countTimer)
    TextView countTimer;
    @BindView(R.id.requestOTPBTN)
    TextView requestOTPBTN;
    @BindView(R.id.proceedBTN) TextView proceedBTN;

    CountDownTimer timer;
    int counter = 120;
    String number;
    String fragmentTag;
    String pass;
    String passCon;

    public static OTPDialog newInstance(Context context, Callback callback, String number, String fragmentTag) {
        OTPDialog dialog = new OTPDialog();
        dialog.context = context;
        dialog.callback = callback;
        dialog.number = number;
        dialog.fragmentTag = fragmentTag;
        return dialog;
    }

    public static OTPDialog newInstance(Context context, Callback callback, String number, String fragmentTag, String pass, String passCon) {
        OTPDialog dialog = new OTPDialog();
        dialog.context = context;
        dialog.callback = callback;
        dialog.number = number;
        dialog.fragmentTag = fragmentTag;
        dialog.pass = pass;
        dialog.passCon = passCon;
        return dialog;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.dialog_otp;
    }

    @Override
    public void onViewReady() {
        mobileNumberTXT.setText(number);

        OTPET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 4){
                    proceedBTN.setEnabled(true);
                    proceedBTN.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                }/*else{

                }*/
            }
        });
    }

    public interface Callback{
        void onSuccess(String OTP);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onStart() {
        super.onStart();
        setDialogMatchParent();
        EventBus.getDefault().register(this);
        proceedBTN.setEnabled(false);
        proceedBTN.setBackgroundTintList(getResources().getColorStateList(R.color.gray));
        startTimer();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        timer.cancel();
        super.onStop();
    }

    @OnClick(R.id.proceedBTN)
    void onProceed(){
        if(OTPET.getText().length() == 4){
            switch (fragmentTag){
                case "forgot":
                    Auth.getDefault().forgotpassOTP(context,OTPET.getText().toString(),number);
                    break;
                default:
                   // Auth.getDefault().update_phone_OTP(context,OTPET.getText().toString(),number);
                    break;
            }

        }
    }

    @OnClick(R.id.requestOTPBTN)
    void onRequest(){
        switch (fragmentTag){
            case "forgot":
                Auth.getDefault().resetPassword(context,number,pass,passCon);
                break;
            default:
               // Auth.getDefault().update_phone_number(context,number);
                break;
        }
    }

    /*@Subscribe
    public void onResponse(Auth.UpdatePhoneNumberResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(context,baseTransformer.msg, ToastMessage.Status.SUCCESS);
            startTimer();
        }else{
            ToastMessage.show(context,baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }*/

    @Subscribe
    public void onResponse(Auth.ResetPasswordResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(context,baseTransformer.msg, ToastMessage.Status.SUCCESS);
            startTimer();
        }else{
            ToastMessage.show(context,baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    /*@Subscribe
    public void onResponse(Auth.OTPUpdateResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(context,baseTransformer.msg,ToastMessage.Status.SUCCESS);
            callback.onSuccess("success");
            dismiss();
        }else{
            ToastMessage.show(context,baseTransformer.msg,ToastMessage.Status.FAILED);
        }
    }*/

    @Subscribe
    public void onResponse(Auth.ForgotOTPResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(context,baseTransformer.msg,ToastMessage.Status.SUCCESS);
            callback.onSuccess("success");
            dismiss();
        }else{
            ToastMessage.show(context,baseTransformer.msg,ToastMessage.Status.FAILED);
        }
    }


    private void startTimer(){
        try{
            timer = new CountDownTimer(120000,1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    requestOTPBTN.setEnabled(false);
                    requestOTPBTN.setTextColor(getResources().getColor(R.color.gray));
                    countTimer.setVisibility(View.VISIBLE);
                    countTimer.setText(String.valueOf(counter));
                    counter--;
                    if(counter<=0){
                        timer.cancel();
                        timer.onFinish();
                    }
                }
                @Override
                public void onFinish() {
                    countTimer.setVisibility(View.INVISIBLE);
                    requestOTPBTN.setEnabled(true);
                    counter = 120;
                    requestOTPBTN.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
            }.start();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
