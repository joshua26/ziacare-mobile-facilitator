package com.ziacare.facilitator.android.fragment.register;

import android.widget.EditText;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.RegisterActivity;
import com.ziacare.facilitator.android.dialog.OTPDialog;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.Keyboard;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

public class ResetPasswordFragment extends BaseFragment implements OTPDialog.Callback {
    public static final String TAG = ResetPasswordFragment.class.getName().toString();

    private RegisterActivity activity;

    @BindView(R.id.passET)                              EditText passET;
    @BindView(R.id.confirmpassET)                       EditText confirmpassET;

    String phone;

    public static ResetPasswordFragment newInstance(String phone) {
        ResetPasswordFragment fragment = new ResetPasswordFragment();
        fragment.phone = phone;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_reset_password;
    }

    @Override
    public void onViewReady() {
        activity = (RegisterActivity) getContext();
        activity.setTitle("Reset Password");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @OnClick(R.id.saveBTN)
    void saveBTN(){
        Auth.getDefault().resetPassword(activity,phone
                ,passET.getText().toString().trim()
                ,confirmpassET.getText().toString().trim());
    }

    @Subscribe
    public void onResponse(Auth.ResetPasswordResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if (baseTransformer.status){
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
            OTPDialog.newInstance(activity,this,phone,"forgot",passET.getText().toString().trim(),confirmpassET.getText().toString().trim()).show(getChildFragmentManager(), TAG);
        } else {
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.FAILED);
            if (!ErrorResponseManger.first(baseTransformer.error.password).equals("")){
                passET.setError(ErrorResponseManger.first(baseTransformer.error.password));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.password_confirmation).equals("")){
                confirmpassET.setError(ErrorResponseManger.first(baseTransformer.error.password_confirmation));
            }

        }
    }

    @Override
    public void onSuccess(String OTP) {
        if(OTP.equalsIgnoreCase("success")){
            Keyboard.hideKeyboard(activity);
            activity.startLandingActivity("login");
        }
    }
}
