package com.ziacare.facilitator.android.fragment.kyc;

import static android.app.Activity.RESULT_OK;

import static com.ziacare.facilitator.vendor.android.java.facedetect.Utils.Imageutils.GALEERY_REQUEST_CODE;
import static com.ziacare.facilitator.vendor.android.java.facedetect.Utils.Imageutils.SCANNER_REQUEST_CODE;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.asksira.bsimagepicker.BSImagePicker;
import com.iceteck.silicompressorr.SiliCompressor;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraOptions;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.VideoResult;
import com.ziacare.facilitator.BuildConfig;
import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.KYCActivity;
import com.ziacare.facilitator.request.KYC;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.PermissionChecker;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.android.java.facedetect.FaceCenterCircleView.FaceCenterCrop;
import com.ziacare.facilitator.vendor.android.java.facedetect.Utils.Imageutils;
import com.ziacare.facilitator.vendor.android.java.facedetect.Utils.ProgressBarUtil.ProgressBarData;
import com.ziacare.facilitator.vendor.android.java.facedetect.Utils.ProgressBarUtil.ProgressUtils;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SelfieFragment extends BaseFragment implements BSImagePicker.OnSingleImageSelectedListener, Imageutils.ImageAttachmentListener {
    public static final String TAG = SelfieFragment.class.getName().toString();

    private KYCActivity activity;

    private static final int REQUEST_CODE = 1;
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION_RESULT = 1;

    String user_code;

    Imageutils imageutils;
    Imageutils.ImageAttachmentListener imageAttachmentListener;
    FaceCenterCrop faceCenterCrop;
    FaceCenterCrop.FaceCenterCropListener faceCenterCropListener;

    @BindView(R.id.ivProfile) CircleImageView ivProfile;
    @BindView(R.id.ivProfile2) CircleImageView ivProfile2;
    @BindView(R.id.ivProfile3) CircleImageView ivProfile3;

    //video
    @BindView(R.id.videoTXT) TextView videoTXT;
    @BindView(R.id.cameraLayout) View cameraLayout;
    @BindView(R.id.mainLayout) View mainLayout;
    @BindView(R.id.cameraView) CameraView cameraView;
    @BindView(R.id.start) ImageView start;
    @BindView(R.id.timerTXT) TextView timerTXT;
    CountDownTimer timer;
    int timerSec = 5;
    private File videoFile;
    private File mVideoFolder;
    private String mVideoFileName;
    private AnimationDrawable splashAnimation;
    private String outputDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).getAbsolutePath();

    File smile, left, right;
    ProgressUtils progressUtils;
    int num = 0;

    public static SelfieFragment newInstance(String user_code, boolean isAddBene) {
        SelfieFragment fragment = new SelfieFragment();
        fragment.user_code = user_code;
        fragment.isAddBene = isAddBene;
        return fragment;
    }

    @BindView(R.id.idIV)
    ImageView idIV;

    File selfie;
    boolean isAddBene;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_selfie;
    }

    @Override
    public void onViewReady() {
        activity = (KYCActivity) getContext();

        //FOR FACE DETECTION
        imageutils = new Imageutils(activity, this, true);
        imageutils.setImageAttachment_callBack(this);
        progressUtils = new ProgressUtils(activity);
        faceCenterCrop = new FaceCenterCrop(activity, 100, 100, 1);

        createVideoFolder();

        //String filePath = SiliCompressor.with(Context).compressVideo(mVideoFileName, outputDir);

        cameraView.setLifecycleOwner(getViewLifecycleOwner());

        cameraView.addCameraListener(new CameraListener() {
            @Override
            public void onCameraOpened(@NonNull CameraOptions options) {
                super.onCameraOpened(options);
            }

            @Override
            public void onCameraClosed() {
                super.onCameraClosed();
            }

            @Override
            public void onVideoTaken(@NonNull VideoResult result) {
                super.onVideoTaken(result);
                videoFile = result.getFile();
               // compressVideo(videoFile);
                Log.d("VIDEO SIZE",Formatter.formatShortFileSize(getContext(),videoFile.length()));
            }

            @Override
            public void onVideoRecordingStart() {
                super.onVideoRecordingStart();
                start.setVisibility(View.INVISIBLE);
                timerTXT.setVisibility(View.VISIBLE);
                //activity.showNavbar(false);
                Toast.makeText(getContext(), "Video Recording Started", Toast.LENGTH_LONG).show();

                timer = new CountDownTimer(5000, 850) {

                    public void onTick(long millisUntilFinished) {
                        Log.d(TAG, "seconds remaining: " + millisUntilFinished / 1000);
                        timerTXT.setText(String.valueOf(timerSec));
                        timerSec--;
                    }

                    public void onFinish() {
                        timerSec = 5;
                    }
                }.start();
            }

            @Override
            public void onVideoRecordingEnd() {
                super.onVideoRecordingEnd();
                start.setVisibility(View.VISIBLE);
                mainLayout.setVisibility(View.VISIBLE);
                cameraLayout.setVisibility(View.GONE);
                timerTXT.setVisibility(View.GONE);
                videoTXT.setText(videoFile.getName());
                timer.cancel();
                cameraView.stopVideo();
                cameraView.close();
                //activity.showNavbar(true);
                Toast.makeText(getContext(), "Video Recording Successful", Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        activity.setTitle("Selfie Verification");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
        if(timer != null){
            timer.cancel();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        cameraView.close();
        if(timer != null){
            timer.cancel();
        }
    }

    @Subscribe
    public void onResponse(KYC.ValidateSelfieResponse response) {
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if (baseTransformer.status) {
            ToastMessage.show(activity, baseTransformer.msg, ToastMessage.Status.SUCCESS);
            KYC.getDefault().KYC(activity, selfie, activity.getId(), user_code);
        } else {
            ToastMessage.show(activity, baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(KYC.KYCResponse response) {
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if (baseTransformer.status) {
            // ToastMessage.show(activity,baseTransformer.msg,ToastMessage.Status.SUCCESS);
            activity.openKYCFragment(user_code);
        } else {
            ToastMessage.show(activity, baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(KYC.UploadSelfieResponse response) {
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if (baseTransformer.status) {
            ToastMessage.show(activity, baseTransformer.msg, ToastMessage.Status.SUCCESS);
            if (isAddBene) {
                activity.openValidateIDFragment(user_code, true);
            } else {
                activity.onBackPressed();
            }
        } else {
            ToastMessage.show(activity, baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @OnClick(R.id.addBTN)
    void onAddIDBTN() {
        checkWriteStoragePermission();
        //imagePicker();
    }

    @OnClick(R.id.ivProfile)
    public void onViewClicked() {
        num = 1;
        imageutils.imagepicker(1, num);
        // Log.e("NUM on buttom",String.valueOf(num));
        // checkWriteStoragePermission();
    }

    @OnClick(R.id.ivProfile2)
    public void onViewClicked2() {
        num = 2;
        imageutils.imagepicker(1, num);
        //Log.e("NUM on buttom",String.valueOf(num));
        // checkWriteStoragePermission();
    }

    @OnClick(R.id.ivProfile3)
    public void onViewClicked3() {
        num = 3;
        imageutils.imagepicker(1, num);
        //Log.e("NUM on buttom",String.valueOf(num));
        //checkWriteStoragePermission();
    }

    @OnClick(R.id.videoTXT)
    void onVideoTXT() {
        mainLayout.setVisibility(View.GONE);
        cameraLayout.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.start)
    void onVideoStart() {
        checkWriteStoragePermission();
    }

    @OnClick(R.id.submitBTN)
    void onSubmitBTN() {
        //KYC.getDefault().validateSelfie(activity,selfie,user_code);
        /*String filepath = "";
        try {
            filepath = SiliCompressor.with(activity).compressVideo(mVideoFileName, outputDir);
            File video = new File(filepath);

            Log.d("VIDEO SIZE Compressed",Formatter.formatShortFileSize(getContext(),video.length()));
            KYC.getDefault().uploadSelfie(activity, getSmile(), getLeft(), getRight(), video, user_code);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }*/

        KYC.getDefault().uploadSelfie(activity, getSmile(), getLeft(), getRight(), videoFile, user_code);
    }

    private void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, REQUEST_CODE);
    }

    /*public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_CODE){
            try{
                Bitmap photo = (Bitmap)data.getExtras().get("data");
                Log.e("NUM",String.valueOf(num));
                if(num == 1){
                    ivProfile.setImageBitmap(photo);
                    File front = imageutils.bitmapToFile(photo,"front");
                    setSmile(new Compressor(activity).compressToFile(front));
                    Log.e("HEEERE","HEEERE1");
                }else if(num == 2){
                    ivProfile2.setImageBitmap(photo);
                    File left = imageutils.bitmapToFile(photo,"left");
                    setLeft(new Compressor(activity).compressToFile(left));
                    Log.e("HEEERE","HEEERE2");
                }else if(num == 3){
                    ivProfile3.setImageBitmap(photo);
                    File right = imageutils.bitmapToFile(photo,"right");
                    setRight(new Compressor(activity).compressToFile(right));
                    Log.e("HEEERE","HEEERE3");
                }

               *//* SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String currentDateandTime = sdf.format(new Date());
                File file = imageutils.bitmapToFile(photo,"image");
                selfie = new Compressor(activity).compressToFile(file);*//*

               // Log.d("id file size", Formatter.formatShortFileSize(getContext(),selfie.length()));
            }catch (NullPointerException e){
                e.printStackTrace();
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
    }*/

    private void imagePicker() {
        BSImagePicker pickerDialog = new BSImagePicker.Builder(BuildConfig.APPLICATION_ID + ".fileprovider").build();
        pickerDialog.show(getChildFragmentManager(), "picker");
    }

    private void createVideoFolder() {
        File movieFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        mVideoFolder = new File(movieFile, "ziacare-facilitator");
        if (!mVideoFolder.exists()) {
            mVideoFolder.mkdirs();
        }
    }

    private File createVideoFileName() throws IOException {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String prepend = "VIDEO_" + timestamp + "_";
        videoFile = File.createTempFile(prepend, ".mp4", mVideoFolder);
        mVideoFileName = videoFile.getAbsolutePath();
        return videoFile;
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        try {
            idIV.setImageURI(uri);
            File file = new File(uri.getPath());

            long kilobytes = file.length() / 1024;
            long mb = kilobytes / 1024;
            if (mb > 5) {
                selfie = new Compressor(activity).compressToFile(file);
            } else {
                selfie = file;
            }

            Log.d("id file size", Formatter.formatShortFileSize(getContext(), selfie.length()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION_RESULT) {
            if (activity.isAllPermissionResultGranted(grantResults)) {
                //openCamera();
                try {
                    cameraView.takeVideo(createVideoFileName());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    private void checkWriteStoragePermission() {
        String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

        // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
        if (PermissionChecker.checkPermissions(activity, PERMISSIONS, REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION_RESULT)) {
            //openCamera();
            try {
                cameraView.takeVideo(createVideoFileName());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
                /*if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    Toast.makeText(getContext(), "Permission required", Toast.LENGTH_SHORT).show();
                }*/
            requestPermissions(PERMISSIONS, REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION_RESULT);
        }
        /*} else {
            openCamera();
        }*/
    }

    private FaceCenterCrop.FaceCenterCropListener getFaceCropResult() {
        if (faceCenterCropListener == null)
            faceCenterCropListener = new FaceCenterCrop.FaceCenterCropListener() {
                @Override
                public void onTransform(Bitmap updatedBitmap) {
                    Log.d("Time log", "Output is set");
                    ivProfile.setImageBitmap(updatedBitmap);
                    Toast.makeText(activity, "We detected a face", Toast.LENGTH_SHORT).show();
                    progressUtils.dismissDialog();
                }

                @Override
                public void onFailure() {
                    Toast.makeText(activity, "No face was detected", Toast.LENGTH_SHORT).show();
                    progressUtils.dismissDialog();

                }
            };

        return faceCenterCropListener;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: ");
        try {
            super.onActivityResult(requestCode, resultCode, data);
            imageutils.onActivityResult(requestCode, resultCode, data, num);

            if (requestCode == SCANNER_REQUEST_CODE && resultCode == RESULT_OK) {
                Log.d(TAG, "onActivityResult: " + SCANNER_REQUEST_CODE);
            } else if (requestCode == GALEERY_REQUEST_CODE && resultCode == RESULT_OK) {
                Log.d(TAG, "onActivityResult: " + GALEERY_REQUEST_CODE);

            }
        } catch (Exception ex) {
            Toast.makeText(activity, ex.toString(),
                    Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void image_attachment(int from, String filename, Bitmap file, Uri uri, int num) {

        Log.d(TAG, "getImageAttachmentCallback: " + from + "\n" + num);

        if (from == SCANNER_REQUEST_CODE) {
            if (num == 1) {
                ivProfile.setImageBitmap(file);
                BitmapDrawable drawable = (BitmapDrawable) ivProfile.getDrawable();
                Bitmap bitmap = drawable.getBitmap();
                setSmile(imageutils.bitmapToFile(bitmap, "smile"));
            }
            if (num == 2) {
                ivProfile2.setImageBitmap(file);
                BitmapDrawable drawable = (BitmapDrawable) ivProfile2.getDrawable();
                Bitmap bitmap = drawable.getBitmap();
                setLeft(imageutils.bitmapToFile(bitmap, "left"));
            }
            if (num == 3) {
                ivProfile3.setImageBitmap(file);
                BitmapDrawable drawable = (BitmapDrawable) ivProfile3.getDrawable();
                Bitmap bitmap = drawable.getBitmap();
                setRight(imageutils.bitmapToFile(bitmap, "right"));
            }
        } else if (from == GALEERY_REQUEST_CODE) {
            Log.d("Time log", "IA callback triggered");

            ProgressBarData progressBarData = new ProgressBarData.ProgressBarBuilder()
                    .setCancelable(true)
                    .setProgressMessage("Processing")
                    .setProgressMessageColor(Color.parseColor("#4A4A4A"))
                    .setBackgroundViewColor(Color.parseColor("#FFFFFF"))
                    .setProgressbarTintColor(Color.parseColor("#FAC42A")).build();

            // ivProfile.setImageBitmap(file);

            progressUtils.showDialog(progressBarData);

            faceCenterCrop.detectFace(file, getFaceCropResult());
        }
    }

    /*private File compressVideo(File videoFile){
        String filepath = "";
        File video;

        try {
            filepath = SiliCompressor.with(activity).compressVideo(getImageContentUri(activity,videoFile), outputDir);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        video = new File(filepath);
        Log.d("VIDEO SIZE Compressed",Formatter.formatShortFileSize(getContext(),video.length()));

        return video;
    }

    public static Uri getImageContentUri(Context context, File imageFile) {
        String filePath = imageFile.getAbsolutePath();
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[] { MediaStore.Images.Media._ID },
                MediaStore.Images.Media.DATA + "=? ",
                new String[] { filePath }, null);
        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            cursor.close();
            return Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + id);
        } else {
            if (imageFile.exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, filePath);
                return context.getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return null;
            }
        }
    }*/

    public File getSmile() {
        return smile;
    }

    public void setSmile(File smile) {
        this.smile = smile;
    }

    public File getLeft() {
        return left;
    }

    public void setLeft(File left) {
        this.left = left;
    }

    public File getRight() {
        return right;
    }

    public void setRight(File right) {
        this.right = right;
    }
}
