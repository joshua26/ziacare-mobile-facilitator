package com.ziacare.facilitator.android.fragment.register;

import android.util.Log;
import android.widget.EditText;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.RegisterActivity;
import com.ziacare.facilitator.config.Keys;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.request.APIRequest;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

public class SignUpStep3Fragment extends BaseFragment {
    public static final String TAG = SignUpStep3Fragment.class.getName().toString();

    public static SignUpStep3Fragment newInstance() {
        SignUpStep3Fragment fragment = new SignUpStep3Fragment();
        return fragment;
    }

    private RegisterActivity registerActivity;

    APIRequest apiRequest;

    @BindView(R.id.tinET) EditText tinET;
    @BindView(R.id.sssET) EditText sssET;
    @BindView(R.id.phicET) EditText phicET;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_signup_step3;
    }

    @Override
    public void onViewReady() {
        registerActivity = (RegisterActivity) getContext();
    }

    @Override
    public void onStart() {
        super.onStart();
        registerActivity.setTitle("Step 3: Government ID Nos.");
        EventBus.getDefault().register(this);
        try{
            tinET.setText(registerActivity.getTin());
            sssET.setText(registerActivity.getSss());
            phicET.setText(registerActivity.getPhic());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @OnClick(R.id.signUpBTN)
    void onNext(){
        softSave();
    }

    private void softSave(){
        try{
            registerActivity.setTin(tinET.getText().toString());
            registerActivity.setSss(sssET.getText().toString());
            registerActivity.setPhic(phicET.getText().toString());

            apiRequest = Auth.getDefault().validate(registerActivity)
                    .addParameter(Keys.TIN_NO, tinET.getText().toString())
                    .addParameter(Keys.SSS_NO, sssET.getText().toString())
                    .addParameter(Keys.PHIC_NO, phicET.getText().toString())
                    .addParameter(Keys.STEP,"3")
                    .showDefaultProgressDialog("Validating Step 3");
            apiRequest.execute();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onResponse(Auth.ValidateCodeResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if (baseTransformer.status) {
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
            registerActivity.openOTPFragment();
        } else {
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.FAILED);
            if (!ErrorResponseManger.first(baseTransformer.error.tin_no).equals("")) {
                tinET.setError(ErrorResponseManger.first(baseTransformer.error.tin_no));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.sss_no).equals("")) {
                sssET.setError(ErrorResponseManger.first(baseTransformer.error.sss_no));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.phic_no).equals("")) {
                phicET.setError(ErrorResponseManger.first(baseTransformer.error.phic_no));
            }
        }
    }

}
