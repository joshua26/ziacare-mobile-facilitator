package com.ziacare.facilitator.android.activity;

import android.os.Build;
import android.view.View;

import androidx.annotation.RequiresApi;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.fragment.scan.CardListFragment;
import com.ziacare.facilitator.android.fragment.scan.ScanFragment;
import com.ziacare.facilitator.android.fragment.scan.ScanZiacareCardFragment;
import com.ziacare.facilitator.android.route.RouteActivity;

import butterknife.OnClick;

public class ScanActivity extends RouteActivity {
    public static final String TAG = ScanActivity.class.getName();

    private String scanCode = "";

    @Override
    public int onLayoutSet() {
        return R.layout.activity_scan;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onViewReady() {
       // getActivityIconBTN().setVisibility(View.VISIBLE);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {

        scanCode = getFragmentBundle().getString("scanCode");

        switch (fragmentName){
            case "list":
                openCardListFragment();
                break;
            case "card":
                openScanZiacareCardFragment();
                break;
            case "scan":
            default:
                openScanFragment(scanCode);
                break;
        }
    }

    public void openScanFragment(String user_code){
        switchFragment(ScanFragment.newInstance(user_code));
    }
    public void openScanZiacareCardFragment(){switchFragment(ScanZiacareCardFragment.newInstance());}
    public void openCardListFragment(){switchFragment(CardListFragment.newInstance());}

    @OnClick(R.id.activityIconBTN)
    void onIconBTN(){
        openScanZiacareCardFragment();
    }
}
