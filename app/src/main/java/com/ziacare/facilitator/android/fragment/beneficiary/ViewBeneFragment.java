package com.ziacare.facilitator.android.fragment.beneficiary;

import android.util.Log;
import android.widget.TextView;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.BeneficiaryActivity;
import com.ziacare.facilitator.android.fragment.DefaultFragment;
import com.ziacare.facilitator.data.model.api.TransactionHistoryModel;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.request.Beneficiary;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

public class ViewBeneFragment extends BaseFragment {
    public static final String TAG = ViewBeneFragment.class.getName().toString();

    public static ViewBeneFragment newInstance(TransactionHistoryModel model) {
        ViewBeneFragment fragment = new ViewBeneFragment();
        fragment.model = model;
        return fragment;
    }

    BeneficiaryActivity activity;

    TransactionHistoryModel model;

    @BindView(R.id.userIDTXT) TextView userIDTXT;
    @BindView(R.id.firstNameTXT) TextView firstNameTXT;
    @BindView(R.id.lastNameTXT) TextView lastNameTXT;
    @BindView(R.id.middleNameTXT) TextView middleNameTXT;
    @BindView(R.id.birthdateTXT) TextView birthdateTXT;
    @BindView(R.id.genderTXT) TextView genderTXT;
    @BindView(R.id.provinceTXT) TextView provinceTXT;
    @BindView(R.id.cityTXT) TextView cityTXT;
    @BindView(R.id.brgyTXT) TextView brgyTXT;
    @BindView(R.id.streetTXT) TextView streetTXT;
    @BindView(R.id.zipcodeTXT) TextView zipcodeTXT;
    @BindView(R.id.emailTXT) TextView emailTXT;
    @BindView(R.id.phoneTXT) TextView phoneTXT;
    @BindView(R.id.tinTXT) TextView tinTXT;
    @BindView(R.id.sssTXT) TextView sssTXT;
    @BindView(R.id.phicTXT) TextView phicTXT;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_view_bene;
    }

    @Override
    public void onViewReady() {
        activity = (BeneficiaryActivity) getContext();
        setDetails(model);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        activity.setTitle("View Profile");
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        Beneficiary.getDefault().scanRefNo(activity,model.qrcode);
    }

    private void setDetails(TransactionHistoryModel model) {
        userIDTXT.setText(model.qrcode);
        firstNameTXT.setText(model.firstname);
        lastNameTXT.setText(model.lastname);
        middleNameTXT.setText(model.middlename);
        birthdateTXT.setText(model.birthdate);
        genderTXT.setText(model.gender);
        provinceTXT.setText(model.province);
        cityTXT.setText(model.city);
        brgyTXT.setText(model.brgy);
        streetTXT.setText(model.streetName);
        zipcodeTXT.setText(model.zipcode);
        emailTXT.setText(model.email);
        phoneTXT.setText(model.phoneNumber);
        tinTXT.setText(model.tinNo);
        sssTXT.setText(model.sssNo);
        phicTXT.setText(model.phicNo);
    }

    @OnClick(R.id.updateProfileBTN)
    void onUpdate(){
        activity.openEditBeneFragment(model);
    }

    @Subscribe
    public void onResponse(Beneficiary.ScanUserCodeResponse response){
        SingleTransformer<TransactionHistoryModel> singleTransformer = response.getData(SingleTransformer.class);
        if(singleTransformer.status){
            setDetails(singleTransformer.data);
        }
        else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }
}

