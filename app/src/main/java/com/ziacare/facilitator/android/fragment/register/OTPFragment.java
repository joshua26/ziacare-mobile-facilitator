package com.ziacare.facilitator.android.fragment.register;

import android.os.Build;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.RegisterActivity;
import com.ziacare.facilitator.data.model.api.UserModel;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.OtpEditText;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.request.APIRequest;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

public class OTPFragment extends BaseFragment {
    public static final String TAG = OTPFragment.class.getName().toString();

    public static OTPFragment newInstance() {
        OTPFragment fragment = new OTPFragment();
        return fragment;
    }

    RegisterActivity registerActivity;

    private APIRequest apiRequest;

    @BindView(R.id.OTPET)
    OtpEditText OTPET;
    @BindView(R.id.mobileNumberTXT)
    TextView mobileNumberTXT;
    @BindView(R.id.countTimer)
    TextView countTimer;
    @BindView(R.id.requestOTPBTN)
    LinearLayout requestOTPBTN;
    @BindView(R.id.signUpBTN) TextView signUpBTN;

    CountDownTimer timer;

    public int counter = 120;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_otp;
    }

    @Override
    public void onViewReady() {
        registerActivity = (RegisterActivity) getContext();
        mobileNumberTXT.setText(registerActivity.getPhone().replace("-", ""));
        registerActivity.getActivityBackBTN().setVisibility(View.VISIBLE);
        registerActivity.getActivityBackBTN().setImageDrawable(getResources().getDrawable(R.drawable.ic_home_alt));
        registerActivity.getActivityBackBTN().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerActivity.startMainActivity("home");
            }
        });

        OTPET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 4){
                    signUpBTN.setEnabled(true);
                    signUpBTN.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                }/*else{

                }*/
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        registerActivity.setTitle("Step 4: OTP Verification");
        registerActivity.setCounterFlag(false);
        signUpBTN.setEnabled(false);
        signUpBTN.setBackgroundTintList(getResources().getColorStateList(R.color.gray));
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @OnClick(R.id.requestOTPBTN)
    void onRequest() {
        apiRequest();
    }

    @OnClick(R.id.signUpBTN)
    void onSignUp() {
        if(OTPET.getText().length() == 4){
            Auth.getDefault().OTP(registerActivity,OTPET.getText().toString(),registerActivity.getPhone().replace("-", ""));
        }
    }

    @Subscribe
    public void onResponse(Auth.OTPResponse response) {
        SingleTransformer<UserModel> singleTransformer = response.getData(SingleTransformer.class);
        if (singleTransformer.status) {
            timer.cancel();
            ToastMessage.show(registerActivity, singleTransformer.msg, ToastMessage.Status.SUCCESS);
            registerActivity.startKYCActivity("selfie", singleTransformer.data.qrcode, true);
        } else {
            ToastMessage.show(registerActivity, singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(Auth.RegisterResponse loginResponse) {
        BaseTransformer baseTransformer = loginResponse.getData(BaseTransformer.class);
        if (baseTransformer.status) {
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.SUCCESS);

            try{
                timer = new CountDownTimer(120000,1000) {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onTick(long millisUntilFinished) {
                        requestOTPBTN.setEnabled(false);
                        requestOTPBTN.setBackgroundTintList(getResources().getColorStateList(R.color.gray));
                        countTimer.setVisibility(View.VISIBLE);
                        countTimer.setText(String.valueOf(counter));
                        counter--;
                        if(counter<=0){
                            timer.cancel();
                            timer.onFinish();
                        }
                    }
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onFinish() {
                        countTimer.setVisibility(View.INVISIBLE);
                        requestOTPBTN.setEnabled(true);
                        counter = 120;
                        //registerActivity.setCounterFlag(true);
                        requestOTPBTN.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                    }
                }.start();
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    private void apiRequest(){
        Auth.getDefault().register(registerActivity,
                registerActivity.getFname(),
                registerActivity.getMname(),
                registerActivity.getLname(),
                registerActivity.getGender(),
                registerActivity.getDob(),
                registerActivity.getProvince(),
                registerActivity.getProvinceSku(),
                registerActivity.getCity(),
                registerActivity.getCitySku(),
                registerActivity.getBrgy(),
                registerActivity.getStreet(),
                registerActivity.getZipcode(),
                registerActivity.getPhone().replace("-", ""),
                registerActivity.getEmail(),
                registerActivity.getPass(),
                registerActivity.getConfirmpass(),
                registerActivity.getTin(),
                registerActivity.getSss(),
                registerActivity.getPhic());
    }

}
