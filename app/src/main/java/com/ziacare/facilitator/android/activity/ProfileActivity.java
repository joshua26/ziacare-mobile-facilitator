package com.ziacare.facilitator.android.activity;


import android.view.View;
import android.widget.TextView;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.fragment.profile.ChangePassFragment;
import com.ziacare.facilitator.android.fragment.profile.FAQFragment;
import com.ziacare.facilitator.android.fragment.profile.LegalFragment;
import com.ziacare.facilitator.android.fragment.profile.PrivacyPolicyFragment;
import com.ziacare.facilitator.android.fragment.profile.UpdateProfileFragment;
import com.ziacare.facilitator.android.route.RouteActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class ProfileActivity extends RouteActivity {
    public static final String TAG = ProfileActivity.class.getName().toString();

    @BindView(R.id.activityBackBTN)                     View activityBackBTN;
    @BindView(R.id.activityTitleTXT)                    TextView activityTitleTXT;

    ProfileActivity profileActivity;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_profile;
    }

    @Override
    public void onViewReady() {
        profileActivity = this;
    }

    public void setSettingsTitle(String title){
        activityTitleTXT.setText(title);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {


        switch (fragmentName){
            case "policy":
                openPolicy();
                break;
            case "legal":
                openLegal();
                break;
            case "faq":
                openFAQ();
                break;
            case "pass":
                openUpdatePassFragment();
                break;
            case "update_profile":
                openUpdateProfileFragment();
                break;
        }
    }

    public void openPolicy(){ switchFragment(PrivacyPolicyFragment.newInstance()); }
    public void openLegal(){ switchFragment(LegalFragment.newInstance()); }
    public void openFAQ(){ switchFragment(FAQFragment.newInstance()); }
    public void openUpdateProfileFragment(){ switchFragment(UpdateProfileFragment.newInstance()); }
    public void openUpdatePassFragment(){ switchFragment(ChangePassFragment.newInstance()); }


    @OnClick(R.id.activityBackBTN)
    void activityBackBTN(){
       onBackPressed();
    }

}
