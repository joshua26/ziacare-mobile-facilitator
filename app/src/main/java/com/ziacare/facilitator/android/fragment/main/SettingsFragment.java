package com.ziacare.facilitator.android.fragment.main;

import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AlertDialog;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.MainActivity;
import com.ziacare.facilitator.data.model.api.UserModel;
import com.ziacare.facilitator.data.preference.UserData;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SettingsFragment extends BaseFragment implements View.OnClickListener{
    public static final String TAG = SettingsFragment.class.getName().toString();

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    @BindView(R.id.logoutBTN)                   TextView logoutBTN;
    @BindView(R.id.avatarIV)                    ImageView avatarIV;
    @BindView(R.id.nameTXT)                     TextView nameTXT;
    @BindView(R.id.privacyBTN)                  View privacyBTN;
    @BindView(R.id.legalBTN)                    View legalBTN;
    @BindView(R.id.faqBTN)                      View faqBTN;
    @BindView(R.id.editBTN)                     View editBTN;
    @BindView(R.id.changePassBTN)               View changePassBTN;

    private MainActivity activity;


    @Override
    public int onLayoutSet() {
        return R.layout.fragment_settings;
    }

    @Override
    public void onViewReady() {
        activity = (MainActivity) getContext();
        privacyBTN.setOnClickListener(this);
        logoutBTN.setOnClickListener(this);
        privacyBTN.setOnClickListener(this);
        legalBTN.setOnClickListener(this);
        faqBTN.setOnClickListener(this);
        editBTN.setOnClickListener(this);
        changePassBTN.setOnClickListener(this);
        getProfile();
    }

    public void getProfile(){
        Auth.getDefault().myProfile(activity);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Subscribe
    public void onResponse(Auth.LogoutResponse logoutResponse){
        BaseTransformer baseTransformer = logoutResponse.getData(BaseTransformer.class);
        if(baseTransformer.status){
            activity.startLandingActivity("");
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
            UserData.clearData();
        }else{
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.FAILED);
        }
        Log.e("Message", logoutResponse.getData(BaseTransformer.class).msg);
    }

    @Subscribe
    public void onResponse(Auth.MyProfileResponse response){
        SingleTransformer<UserModel> singleTransformer = response.getData(SingleTransformer.class);
        if (singleTransformer.status){
            Glide.with(activity)
                    .load(singleTransformer.data.avatar.thumb_path)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.user_ic)
                            .error(R.drawable.user_ic))
                    .into(avatarIV);
            nameTXT.setText(singleTransformer.data.name);
        } else {
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.changePassBTN:
                activity.startProfileActivity("pass");
                break;
            case R.id.editBTN:
                activity.startProfileActivity("update_profile");
                break;
            case R.id.logoutBTN:
                DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked
                            Auth.getDefault().logout(getContext(), UserData.getUserModel().code);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            dialog.dismiss();
                            break;
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Are you sure to logout?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
                break;
            case R.id.privacyBTN:
                activity.startProfileActivity("policy");
                break;
            case R.id.legalBTN:
                activity.startProfileActivity("legal");
                break;
            case R.id.faqBTN:
                activity.startProfileActivity("faq");
                break;
            /*case R.id.scannerBTN:
                activity.startScanActivity("verify");
                break;*/

        }

    }
    @OnClick(R.id.changePassBTN)
    void changePassBTN(){
        activity.startProfileActivity("pass");
    }
    @OnClick(R.id.rateBTN)
    void rateBTN(){
        Toast.makeText(activity, "soon", Toast.LENGTH_SHORT).show();
       // startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/apps/testing/com.malasakit.registration")));
    }

}
