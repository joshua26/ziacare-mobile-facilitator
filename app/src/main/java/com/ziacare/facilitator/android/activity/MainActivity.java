package com.ziacare.facilitator.android.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;


import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.fragment.main.HomeFragment;
import com.ziacare.facilitator.android.fragment.main.SettingsFragment;
import com.ziacare.facilitator.android.route.RouteActivity;
import com.ziacare.facilitator.data.preference.UserData;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class MainActivity extends RouteActivity {
    public static final String TAG = MainActivity.class.getName().toString();

    MainActivity mainActivity;

    @BindView(R.id.homeBTN)                            View homeBTN;
    @BindView(R.id.homeIV)                             ImageView homeIV;
    @BindView(R.id.homeTXT)                            TextView homeTXT;
    @BindView(R.id.settingBTN)                         View settingBTN;
    @BindView(R.id.settingIV)                          ImageView settingIV;
    @BindView(R.id.settingTXT)                         TextView settingTXT;
    @BindView(R.id.scanBTN)                            View scanBTN;
    @BindView(R.id.titleTXT)                           TextView titleTXT;


    @Override
    public int onLayoutSet() {
        return R.layout.activity_main;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getContext();
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "home":
                selectHome();
                break;
            case "settings":
                selectSetting();
                break;
        }
    }

    public void openSettingsFragment(){
        switchFragment(SettingsFragment.newInstance());
    }
    public void openHomeFragment(){
        switchFragment(HomeFragment.newInstance(),true);
    }


    public void selectHome(){
        openHomeFragment();
        titleTXT.setText("Welcome "+ UserData.getUserModel().firstname);
        homeIV.setImageResource(R.drawable.ic_home_alt);
        homeTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
        settingIV.setImageResource(R.drawable.settings);;
        settingTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.black));
    }



    public void selectSetting(){
        openSettingsFragment();
        titleTXT.setText("Settings");
        homeIV.setImageResource(R.drawable.ic_home);
        homeTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.black));
        settingIV.setImageResource(R.drawable.settings_alt);
        settingTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
    }


    @OnClick(R.id.homeBTN)
    void homeBTN(){ selectHome(); }

    @OnClick(R.id.settingBTN)
    void settingBTN(){ selectSetting(); }

    @OnClick(R.id.scanBTN)
    void scanBTN(){
        mainActivity.startRegisterActivity("signup");
    }

    public void setTitle(String title){
        if(titleTXT != null){
            titleTXT.setText(title);
        }
    }

}
