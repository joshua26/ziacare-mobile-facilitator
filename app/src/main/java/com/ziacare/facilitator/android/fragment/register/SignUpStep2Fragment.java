package com.ziacare.facilitator.android.fragment.register;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.RegisterActivity;
import com.ziacare.facilitator.config.Keys;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.request.APIRequest;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

public class SignUpStep2Fragment extends BaseFragment {
    public static final String TAG = SignUpStep2Fragment.class.getName().toString();

    public static SignUpStep2Fragment newInstance() {
        SignUpStep2Fragment fragment = new SignUpStep2Fragment();
        return fragment;
    }

    private RegisterActivity registerActivity;

    APIRequest apiRequest;

    @BindView(R.id.phoneET)
    EditText phoneET;
    @BindView(R.id.emailET)
    EditText emailET;
    @BindView(R.id.passET)
    EditText passET;
    @BindView(R.id.confirmpassET)
    EditText confirmpassET;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_signup_step2;
    }

    @Override
    public void onViewReady() {
        registerActivity = (RegisterActivity) getContext();

        phoneET.addTextChangedListener(new TextWatcher() {
            int prevL = 0;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                prevL = phoneET.getText().toString().length();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int length = editable.length();
                if ((prevL < length) && (length == 3 || length == 7 || length == 11)) {
                    String data = phoneET.getText().toString();
                    phoneET.setText(data + "-");
                    phoneET.setSelection(length + 1);
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        registerActivity.setTitle("Step 2: Contact Information");
        EventBus.getDefault().register(this);
        try{
            phoneET.setText(registerActivity.getPhone());
            emailET.setText(registerActivity.getEmail());
            passET.setText(registerActivity.getPass());
            confirmpassET.setText(registerActivity.getConfirmpass());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @OnClick(R.id.signUpBTN)
    void onNext() {
        softSave();
    }

    private void softSave() {
        try {

            String phone = phoneET.getText().toString().trim().replace("-", "");

            registerActivity.setPhone(phoneET.getText().toString().trim());

            registerActivity.setPass(passET.getText().toString());
            registerActivity.setConfirmpass(confirmpassET.getText().toString());

            apiRequest = Auth.getDefault().validate(registerActivity)
                    .addParameter(Keys.PHONE_NUMBER, phone)
                    .addParameter(Keys.PASSWORD, passET.getText().toString())
                    .addParameter(Keys.PASSWORD_CONFIRM, confirmpassET.getText().toString());
            if (!emailET.getText().toString().isEmpty()) {
                registerActivity.setEmail(emailET.getText().toString());
                apiRequest.addParameter(Keys.EMAIL, emailET.getText().toString());
            }
            apiRequest.addParameter(Keys.STEP,"2")
                      .showDefaultProgressDialog("Validating Step 2")
                      .execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onResponse(Auth.ValidateCodeResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if (baseTransformer.status) {
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
            registerActivity.openStep3Fragment();
        } else {
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.FAILED);
            if (!ErrorResponseManger.first(baseTransformer.error.phone_number).equals("")) {
                // phoneET.setError(ErrorResponseManger.first(baseTransformer.error.phone_number));
                phoneET.setError("Your Phone number is invalid format or already taken");
                phoneET.setHint(" Ex: 09123456789");
            }
            if(!ErrorResponseManger.first(baseTransformer.error.email).equals("")){
                emailET.setError(ErrorResponseManger.first(baseTransformer.error.email));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.password).equals("")) {
                passET.setError(ErrorResponseManger.first(baseTransformer.error.password));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.password_confirmation).equals("")) {
                confirmpassET.setError(ErrorResponseManger.first(baseTransformer.error.password_confirmation));
            }
        }
    }

}
