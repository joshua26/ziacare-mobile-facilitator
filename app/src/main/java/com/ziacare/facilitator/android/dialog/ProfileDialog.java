package com.ziacare.facilitator.android.dialog;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ziacare.facilitator.R;
import com.ziacare.facilitator.data.model.api.SampleModel;
import com.ziacare.facilitator.vendor.android.base.BaseDialog;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ProfileDialog extends BaseDialog {
	public static final String TAG = ProfileDialog.class.getName().toString();
	private static String qrcodetemp;
	private Callback callback;
	private Context context;
	private SampleModel model;

	@BindView(R.id.nameTXT) 							TextView nameTXT;
	@BindView(R.id.addressTXT)                          TextView addressTXT;
	@BindView(R.id.avatarIV) 							ImageView avatarIV;
	@BindView(R.id.idNoTXT)                             TextView idNoTXT;
	@BindView(R.id.bdayTXT)                             TextView bdayTXT;
	@BindView(R.id.dateIssuedTXT)                       TextView dateIssuedTXT;
	@BindView(R.id.genderTXT)                           TextView genderTXT;
	@BindView(R.id.emailTXT)                            TextView emailTXT;
	@BindView(R.id.mobNoTXT)                            TextView mobNoTXT;
	@BindView(R.id.telNoTXT)                            TextView telNoTXT;
	@BindView(R.id.contactTXT)                          TextView contactTXT;
	@BindView(R.id.contactNumberTXT)                    TextView contactNumberTXT;
	@BindView(R.id.relationTXT)                         TextView relationTXT;

	public static ProfileDialog newInstance(Context context, Callback callback, SampleModel model) {
		ProfileDialog dialog = new ProfileDialog();
		dialog.context = context;
		dialog.callback = callback;
		dialog.model = model;
		dialog.setCancelable(false);
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_profile;
	}

	@Override
	public void onViewReady() {
		setDetails(model);

	}

	private void setDetails(SampleModel data) {
		Glide.with(getContext())
				.load(data.bitmap)
				.apply(new RequestOptions()
						.placeholder(R.drawable.user_ic)
						.error(R.drawable.user_ic))
				.into(avatarIV);
		nameTXT.setText(data.name);
		addressTXT.setText(data.address);
		idNoTXT.setText(data.idNo);
		bdayTXT.setText(data.dob);
		dateIssuedTXT.setText(data.dateIssued);
		genderTXT.setText(data.gender);
		emailTXT.setText(data.email);
		mobNoTXT.setText(data.mobileNo);
		telNoTXT.setText(data.telNo);

	}

	public interface Callback {
		void onSuccess(ProfileDialog dialog);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@OnClick(R.id.backBTN)
	void backBTN(){
		callback.onSuccess(this);
	}
}
