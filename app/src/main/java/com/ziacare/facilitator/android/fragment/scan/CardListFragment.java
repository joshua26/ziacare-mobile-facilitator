package com.ziacare.facilitator.android.fragment.scan;

import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.ScanActivity;
import com.ziacare.facilitator.android.adapter.CardListRecyclerViewAdapter;
import com.ziacare.facilitator.android.fragment.DefaultFragment;
import com.ziacare.facilitator.data.model.api.CardModel;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.request.Cards;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class CardListFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = CardListFragment.class.getName().toString();

    public static CardListFragment newInstance() {
        CardListFragment fragment = new CardListFragment();
        return fragment;
    }

    ScanActivity activity;
    private LinearLayoutManager manager;
    private CardListRecyclerViewAdapter adapter;

    @BindView(R.id.swipeRL) SwipeRefreshLayout swipeRL;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.placeHolderTXT) TextView placeHolderTXT;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.searchET) EditText searchET;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_card_list;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onViewReady() {
        activity = (ScanActivity) getContext();
        activity.setTitle("Ziacare Cards");
        swipeRL.setOnRefreshListener(this);
        activity.getActivityIconBTN().setVisibility(View.VISIBLE);
        activity.getActivityIconBTN().setImageDrawable(getResources().getDrawable(R.drawable.ic_plus));
        activity.getActivityIconBTN().setImageTintList(getResources().getColorStateList(R.color.colorPrimary));

        manager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(manager);

        searchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(searchET.getText().toString().trim() != null && searchET.getText().length() != 0){
                    adapter.getFilter().filter(charSequence.toString().toLowerCase());
                }
                else{
                    onRefresh();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        onRefresh();
    }

    @Subscribe
    public void onResponse(Cards.CardListResponse response){
        CollectionTransformer<CardModel> collectionTransformer = response.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            adapter = new CardListRecyclerViewAdapter(activity,collectionTransformer.data);
            recyclerView.setAdapter(adapter);
            adapter.setNewData(collectionTransformer.data);
            progressBar.setVisibility(View.GONE);
            if (adapter.getData().size() == 0){
                recyclerView.setVisibility(View.GONE);
                placeHolderTXT.setVisibility(View.VISIBLE);
            } else {
                recyclerView.setVisibility(View.VISIBLE);
                placeHolderTXT.setVisibility(View.GONE);
            }
        }else{
            progressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            placeHolderTXT.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        Cards.getDefault().cardList(activity,swipeRL);
    }
}
