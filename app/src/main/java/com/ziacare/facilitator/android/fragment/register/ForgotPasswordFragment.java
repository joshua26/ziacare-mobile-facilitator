package com.ziacare.facilitator.android.fragment.register;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.RegisterActivity;
import com.ziacare.facilitator.data.preference.UserData;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

public class ForgotPasswordFragment extends BaseFragment {
    public static final String TAG = ForgotPasswordFragment.class.getName().toString();

    private RegisterActivity activity;

    @BindView(R.id.submitBTN)                   TextView submitBTN;
    @BindView(R.id.phoneET) EditText phoneET;

    String phone;
    public static ForgotPasswordFragment newInstance() {
        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_forgot_password;
    }

    @Override
    public void onViewReady() {
        activity = (RegisterActivity) getContext();

        phoneET.addTextChangedListener(new TextWatcher() {
            int prevL = 0;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                prevL = phoneET.getText().toString().length();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int length = editable.length();
                if ((prevL < length) && (length == 3 || length == 7 || length == 11)) {
                    String data = phoneET.getText().toString();
                    phoneET.setText(data + "-");
                    phoneET.setSelection(length + 1);
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setTitle("Forgot Password");
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @OnClick(R.id.submitBTN)
    void submitBTNClicked(){
        phone = phoneET.getText().toString().trim().replace("-", "");
        Auth.getDefault().forgot(activity,phone);
    }



    @Subscribe
    public void onResponse(Auth.ForgotResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if (baseTransformer.status){
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
            activity.openResetPasswordFragment(phone);
        } else {
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.FAILED);
            if (!ErrorResponseManger.first(baseTransformer.error.phone_number).equals("")) {
                phoneET.setError(ErrorResponseManger.first(baseTransformer.error.phone_number));
            }
        }

    }




}
