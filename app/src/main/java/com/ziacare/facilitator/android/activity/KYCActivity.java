package com.ziacare.facilitator.android.activity;

import android.view.View;
import android.widget.LinearLayout;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.fragment.kyc.KYCFragment;
import com.ziacare.facilitator.android.fragment.kyc.SelfieFragment;
import com.ziacare.facilitator.android.fragment.kyc.ValidateIDFragment;
import com.ziacare.facilitator.android.route.RouteActivity;

import java.io.File;

import butterknife.BindView;

public class KYCActivity extends RouteActivity {
    public static final String TAG = KYCActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_kyc;
    }

    File id;
    File selfie;
    String idCode;

    @BindView(R.id.navbar)
    public LinearLayout navbar;

    @Override
    public void onViewReady() {

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        String user_code = getFragmentBundle().getString("user_code");
        boolean isAddBene = getFragmentBundle().getBoolean("isAddBene");
        switch (fragmentName){
            case "id":
                openValidateIDFragment(user_code,isAddBene);
                break;
            case "selfie":
                openSelfieFragment(user_code,isAddBene);
                break;
            default:

                break;
        }
    }

    public void openValidateIDFragment(String user_code, boolean isAddBene){
        switchFragment(ValidateIDFragment.newInstance(user_code, isAddBene),false);
    }
    public void openSelfieFragment(String user_code, boolean isAddBene){switchFragment(SelfieFragment.newInstance(user_code,isAddBene),false);}

    public void openKYCFragment(String user_code){switchFragment(KYCFragment.newInstance(user_code),false);}

    public File getId() {
        return id;
    }

    public void setId(File id) {
        this.id = id;
    }

    public File getSelfie() {
        return selfie;
    }

    public void setSelfie(File selfie) {
        this.selfie = selfie;
    }

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }

    public void showNavbar(boolean b){
        if(navbar != null){
            navbar.setVisibility(b ? View.VISIBLE : View.GONE);
        }
    }
}
