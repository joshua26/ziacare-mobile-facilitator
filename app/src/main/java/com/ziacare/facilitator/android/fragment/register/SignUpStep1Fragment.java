package com.ziacare.facilitator.android.fragment.register;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.chivorn.smartmaterialspinner.SmartMaterialSpinner;
import com.ziacare.facilitator.BuildConfig;
import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.RegisterActivity;
import com.ziacare.facilitator.android.dialog.CalendarDialog;
import com.ziacare.facilitator.config.Keys;
import com.ziacare.facilitator.data.model.api.PSGCModel;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.request.PSGC;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.request.APIRequest;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.transformer.CollectionTransformer;
import com.ziacare.facilitator.vendor.server.transformer.SingleTransformer;
import com.ziacare.facilitator.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class SignUpStep1Fragment extends BaseFragment implements CalendarDialog.DateTimePickerListener, AdapterView.OnItemSelectedListener {
    public static final String TAG = SignUpStep1Fragment.class.getName().toString();

    public static SignUpStep1Fragment newInstance() {
        SignUpStep1Fragment fragment = new SignUpStep1Fragment();
        return fragment;
    }

    private RegisterActivity registerActivity;

    APIRequest apiRequest;

    @BindView(R.id.fnameET)
    EditText fnameET;
    @BindView(R.id.mnameET)
    EditText mnameET;
    @BindView(R.id.lnameET)
    EditText lnameET;
    @BindView(R.id.spinnerGender)
    Spinner spinnerGender;
    @BindView(R.id.dobTXT)
    TextView dobTXT;
    @BindView(R.id.provinceSpinner)
    SmartMaterialSpinner<String> provinceSpinner;
    @BindView(R.id.citySpinner)
    SmartMaterialSpinner<String> citySpinner;
    @BindView(R.id.brgyET)
    EditText brgyET;
    @BindView(R.id.streetET)
    EditText streetET;
    @BindView(R.id.zipcodeET)
    EditText zipcodeET;

    ArrayList<String> provinceName, provinceSKU, cityName, citySKU;
    String pSKU, cSKU;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_signup_step1;
    }

    @Override
    public void onViewReady() {
        registerActivity = (RegisterActivity) getContext();

        provinceName = new ArrayList<>();
        provinceSKU = new ArrayList<>();
        cityName = new ArrayList<>();
        citySKU = new ArrayList<>();

        if (registerActivity.getProvinceName().isEmpty()) {
            switch(BuildConfig.FLAVOR){
                case "bataan":
                    PSGC.getDefault().getProvinceBySKU(registerActivity,"D01");
                    break;
                default:
                    PSGC.getDefault().getProvince(registerActivity);
                    break;
            }
        } else {
            try {
                provinceName.clear();
                provinceSKU.clear();
                provinceName.addAll(registerActivity.getProvinceName());
                provinceSKU.addAll(registerActivity.getProvinceSKU());
                provinceSpinner.setItem(registerActivity.getProvinceName());
                provinceSpinner.setSelection(registerActivity.getProvincePosition());
                if (!registerActivity.getCityName().isEmpty()) {
                    cityName.clear();
                    citySKU.clear();
                    cityName.addAll(registerActivity.getCityName());
                    citySKU.addAll(registerActivity.getCitySKU());
                    citySpinner.setItem(registerActivity.getCityName());
                    citySpinner.setSelection(registerActivity.getCityPosition());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        provinceSpinner.setOnItemSelectedListener(this);

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    citySpinner.setHint(citySpinner.getSelectedItem());
                    registerActivity.setCityPosition(i);
                    setcSKU(citySKU.get(i));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(registerActivity,
                R.array.gender, R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(adapter2);
    }

    @Override
    public void onStart() {
        super.onStart();
        registerActivity.setTitle("Step 1: Basic Information");
        EventBus.getDefault().register(this);
        try {
            dobTXT.setText(registerActivity.getDob());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @OnClick(R.id.dobTXT)
    void onDobTXT() {
        CalendarDialog.newInstance(this::forDisplay).show(getChildFragmentManager(), TAG);
    }

    @OnClick(R.id.signUpBTN)
    void onNext() {
        softSave();

    }

    @Subscribe
    public void onResponse(PSGC.ProvinceResponse response) {
        try {
            CollectionTransformer<PSGCModel> collectionTransformer = response.getData(CollectionTransformer.class);
            if (collectionTransformer.status) {
                if (provinceName.isEmpty() && provinceSKU.isEmpty()) {
                    for (int i = 0; i < collectionTransformer.data.size(); i++) {
                        provinceName.add(collectionTransformer.data.get(i).name);
                        provinceSKU.add(collectionTransformer.data.get(i).sku);
                    }
                    registerActivity.setProvinceName(provinceName);
                    registerActivity.setProvinceSKU(provinceSKU);
                    provinceSpinner.setItem(provinceName);
                }
            }
        } catch (Exception e) {
            PSGC.getDefault().getProvince(registerActivity);
        }
    }

    @Subscribe
    public void onResponse(PSGC.ProvinceBySKUResponse response) {
        try {
            SingleTransformer<PSGCModel> singleTransformer = response.getData(SingleTransformer.class);
            if (singleTransformer.status) {
                if (provinceName.isEmpty() && provinceSKU.isEmpty()) {
                    provinceName.add(singleTransformer.data.name);
                    provinceSKU.add(singleTransformer.data.sku);
                    registerActivity.setProvinceName(provinceName);
                    registerActivity.setProvinceSKU(provinceSKU);
                    provinceSpinner.setItem(provinceName);
                    provinceSpinner.setSelection(0);
                }
            }
        } catch (Exception e) {
            PSGC.getDefault().getProvinceBySKU(registerActivity,"D01");
        }
    }

    @Subscribe
    public void onResponse(PSGC.MunicipalityResponse response) {
        CollectionTransformer<PSGCModel> collectionTransformer = response.getData(CollectionTransformer.class);
        if (collectionTransformer.status) {
            try {
                if (cityName.isEmpty() && citySKU.isEmpty()) {
                    for (int i = 0; i < collectionTransformer.data.size(); i++) {
                        cityName.add(collectionTransformer.data.get(i).name);
                        citySKU.add(collectionTransformer.data.get(i).sku);
                    }
                    registerActivity.setCityName(cityName);
                    registerActivity.setCitySKU(citySKU);
                    citySpinner.setItem(cityName);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void forDisplay(String date) {
        dobTXT.setText(date);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        try {
            provinceSpinner.setHint(provinceSpinner.getSelectedItem());
            //Log.d("provincePosition", String.valueOf(position));
            setpSKU(provinceSKU.get(position));
            registerActivity.setProvincePosition(position);

            PSGC.getDefault().getMunicipalityBySKU(registerActivity, getpSKU());
            cityName.clear();
            citySKU.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public String getpSKU() {
        return pSKU;
    }

    public void setpSKU(String pSKU) {
        this.pSKU = pSKU;
    }

    public String getcSKU() {
//        if(registerActivity.getCitySKU().isEmpty()){
//            int cityPosition = 0;
//            for (int i = 1; i < cityName.size(); i++) {
//                if (citySpinner.getSelectedItem().toString().equalsIgnoreCase(cityName.get(i))) {
//                    cityPosition = i;
//                }
//            }
//            // Log.d("citySKU", citySKU.get(cityPosition));
//            return citySKU.get(cityPosition);
//        }else{
//            return registerActivity.getCitySKU().get(registerActivity.getCityPosition());
//        }
        return cSKU;
    }

    public void setcSKU(String cSKU) {
        this.cSKU = cSKU;
    }

    private void softSave() {
        try {
            registerActivity.setFname(fnameET.getText().toString());
            if (!mnameET.getText().toString().isEmpty()) {
                registerActivity.setMname(mnameET.getText().toString());
            }
            registerActivity.setLname(lnameET.getText().toString());
            registerActivity.setGender(spinnerGender.getSelectedItem().toString());
            registerActivity.setDob(dobTXT.getText().toString());
            registerActivity.setProvince(provinceSpinner.getSelectedItem());
            registerActivity.setProvinceSku(getpSKU());
            registerActivity.setCity(citySpinner.getSelectedItem());
            registerActivity.setCitySku(getcSKU());
            registerActivity.setBrgy(brgyET.getText().toString());
            registerActivity.setStreet(streetET.getText().toString());
            registerActivity.setZipcode(zipcodeET.getText().toString());

            apiCall();

            Log.d("province", provinceSpinner.getSelectedItem() + " " + getpSKU());
            Log.d("city", citySpinner.getSelectedItem() + " " + getcSKU());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onResponse(Auth.ValidateCodeResponse response) {
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if (baseTransformer.status) {
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
            registerActivity.openStep2Fragment();
        } else {
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.FAILED);
            if (!ErrorResponseManger.first(baseTransformer.error.firstname).equals("")) {
                fnameET.setError(ErrorResponseManger.first(baseTransformer.error.firstname));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.lastname).equals("")) {
                lnameET.setError(ErrorResponseManger.first(baseTransformer.error.lastname));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.birthdate).equals("")) {
                dobTXT.setError(ErrorResponseManger.first(baseTransformer.error.birthdate));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.brgy).equals("")) {
                brgyET.setError(ErrorResponseManger.first(baseTransformer.error.brgy));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.brgy_name).equals("")) {
                brgyET.setError(ErrorResponseManger.first(baseTransformer.error.brgy_name));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.street_name).equals("")) {
                streetET.setError(ErrorResponseManger.first(baseTransformer.error.street_name));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.zipcode).equals("")) {
                zipcodeET.setError(ErrorResponseManger.first(baseTransformer.error.zipcode));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.middlename).equals("")) {
                mnameET.setError(ErrorResponseManger.first(baseTransformer.error.middlename));
            }
//            if(provinceSpinner.getSelectedItem().isEmpty()){
//                provinceSpinner.setErrorText("Field is required.");
//            }
//            if(citySpinner.getSelectedItem().isEmpty()){
//                citySpinner.setErrorText("Field is required.");
//            }
        }
    }

    private void apiCall() {
        apiRequest = Auth.getDefault().validate(registerActivity)
                .addParameter(Keys.FNAME, fnameET.getText().toString())
                .addParameter(Keys.LNAME, lnameET.getText().toString())
                .addParameter(Keys.GENDER, spinnerGender.getSelectedItem().toString())
                .addParameter(Keys.BIRTHDATE, dobTXT.getText().toString())
                .addParameter(Keys.PROVINCE_NAME, provinceSpinner.getSelectedItem())
                .addParameter(Keys.PROVINCE_SKU, getpSKU())
                .addParameter(Keys.BRGY_NAME, brgyET.getText().toString())
                .addParameter(Keys.STREET_NAME, streetET.getText().toString())
                .addParameter(Keys.ZIPCODE, zipcodeET.getText().toString())
                .addParameter(Keys.MNAME, mnameET.getText().toString());
        if(citySpinner.getSelectedItem().toString() != null){
            apiRequest.addParameter(Keys.CITY_NAME, citySpinner.getSelectedItem());
        }
        if(getcSKU() != null){
            apiRequest.addParameter(Keys.CITY_SKU, getcSKU());
        }

                apiRequest.addParameter(Keys.STEP, "1")
                .showDefaultProgressDialog("Validating Step 1");
        apiRequest.execute();
    }

}
