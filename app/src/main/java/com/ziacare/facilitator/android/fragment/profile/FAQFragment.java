package com.ziacare.facilitator.android.fragment.profile;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.ProfileActivity;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.widget.WrapContentWebView;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class FAQFragment extends BaseFragment {
    public static final String TAG = FAQFragment.class.getName().toString();

    private ProfileActivity activity;

    @BindView(R.id.faqTXT)               WrapContentWebView faqTXT;

    public static FAQFragment newInstance() {
        FAQFragment fragment = new FAQFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_faq;
    }

    @Override
    public void onViewReady() {
        activity = (ProfileActivity) getContext();
        activity.setSettingsTitle("Frequently Asked Questions");
        faqTXT.loadDataWithBaseURL(null, getString(R.string.faq), "text/html", "utf-8", null);

    }

    @Override
    public void onStart() {
        super.onStart();
//        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
//        EventBus.getDefault().unregister(this);
        super.onStop();
    }


}
