package com.ziacare.facilitator.android.dialog;

import android.widget.EditText;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.vendor.android.base.BaseDialog;
import com.ziacare.facilitator.vendor.android.java.Keyboard;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ReferNoDialog extends BaseDialog {
	public static final String TAG = ReferNoDialog.class.getName().toString();

	private Callback callback;

	@BindView(R.id.refIDET) 				EditText refIDET;

	public static ReferNoDialog newInstance(Callback callback) {
		ReferNoDialog dialog = new ReferNoDialog();
		dialog.callback = callback;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_refer_no;
	}

	@Override
	public void onViewReady() {

	}

	public interface Callback{
		void onSuccess(String userCode);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}


	@OnClick(R.id.parentLayout)
	void parentLayout(){
		dismiss();
	}

	@OnClick(R.id.closeBTN)
	void closeBTN(){
		dismiss();
	}

	@OnClick(R.id.continueBTN)
	void continueBTN(){
		Keyboard.hideForceKeyboard(getBaseActivity(), refIDET);
		if(callback != null){
			if (!refIDET.getText().toString().trim().equals("")){
				callback.onSuccess(""+refIDET.getText().toString().trim());
				dismiss();
			} else {
				refIDET.setError("This field is required!");
			}
		}
	}

}
