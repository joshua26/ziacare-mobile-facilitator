package com.ziacare.facilitator.android.fragment.beneficiary;

import android.Manifest;
import android.view.View;

import androidx.core.app.ActivityCompat;

import com.google.zxing.Result;
import com.ziacare.facilitator.BuildConfig;
import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.BeneficiaryActivity;
import com.ziacare.facilitator.android.activity.ScanActivity;
import com.ziacare.facilitator.android.dialog.ReferNoDialog;
import com.ziacare.facilitator.android.fragment.DefaultFragment;
import com.ziacare.facilitator.android.fragment.scan.ScanZiacareCardFragment;
import com.ziacare.facilitator.data.model.api.TransactionHistoryModel;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.request.Beneficiary;
import com.ziacare.facilitator.request.Cards;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.Log;
import com.ziacare.facilitator.vendor.android.java.PermissionChecker;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;
import icepick.State;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ProcessKYCFragment extends BaseFragment implements ZXingScannerView.ResultHandler, ReferNoDialog.Callback {
    public static final String TAG = ProcessKYCFragment.class.getName().toString();

    private int PERMISSION_CAMERA = 787;

    private BeneficiaryActivity activity;

    @State
    String resultQR;

    @BindView(R.id.scannerView) 				ZXingScannerView scannerView;

    public static ProcessKYCFragment newInstance() {
        ProcessKYCFragment fragment = new ProcessKYCFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_scan;
    }

    @Override
    public void onViewReady() {
        activity = (BeneficiaryActivity) getContext();
        activity.getActivityIconBTN().setVisibility(View.GONE);
        if(PermissionChecker.checkPermissions(getActivity(), Manifest.permission.CAMERA, PERMISSION_CAMERA)){
            startCamera();
        }else{
            ActivityCompat.requestPermissions(activity, new String[] { Manifest.permission.CAMERA }, PERMISSION_CAMERA);
        }
    }

    private void startCamera(){
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    @Override
    public void handleResult(Result result) {
        if(result != null){
            Log.d("result",result.getText());
            switch (BuildConfig.FLAVOR){
                case "bataan":
                    Beneficiary.getDefault().scanRefNo(activity,result.getText(),"D01");
                    break;
                default:
                    Beneficiary.getDefault().scanRefNo(activity,result.getText());
                    break;
            }
        }
    }

    @OnClick(R.id.inputBTN)
    void inputBTN(){
        ReferNoDialog.newInstance(this).show(getChildFragmentManager(),TAG);
    }

    @Override
    public void onSuccess(String userCode) {
        switch (BuildConfig.FLAVOR){
            case "bataan":
                Beneficiary.getDefault().scanRefNo(activity,userCode,"D01");
                break;
            default:
                Beneficiary.getDefault().scanRefNo(activity,userCode);
                break;
        }
    }

    @Subscribe
    public void onResponse(Beneficiary.ScanUserCodeResponse response){
        SingleTransformer<TransactionHistoryModel> singleTransformer = response.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            activity.openBenePreviewFragment(singleTransformer.data);
        }
        else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
            scannerView.resumeCameraPreview(this);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setTitle("Scan QR Code");
		/*if(PermissionChecker.checkPermissions(getActivity(), Manifest.permission.CAMERA, PERMISSION_CAMERA)){
			startCamera();
		}*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CAMERA) {
            if(activity.isAllPermissionResultGranted(grantResults)){
                startCamera();
            }else{
                activity.onBackPressed();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }
}