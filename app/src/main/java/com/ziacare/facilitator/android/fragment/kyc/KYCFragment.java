package com.ziacare.facilitator.android.fragment.kyc;

import android.util.Log;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.KYCActivity;
import com.ziacare.facilitator.android.fragment.DefaultFragment;

import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.OnClick;

public class KYCFragment  extends BaseFragment {
    public static final String TAG = DefaultFragment.class.getName().toString();

    public static KYCFragment newInstance(String user_code) {
        KYCFragment fragment = new KYCFragment();
        fragment.user_code = user_code;
        return fragment;
    }

    KYCActivity activity;

    String user_code;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_kyc;
    }

    @Override
    public void onViewReady() {
        activity = (KYCActivity) getContext();
        activity.showNavbar(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @OnClick(R.id.homeBTN)
    void onHomeBTN(){
        activity.startScanActivity("scan", user_code);
    }

    @OnClick(R.id.backBTN)
    void onBackBTN(){
        activity.startMainActivity("home");
    }
}