package com.ziacare.facilitator.android.fragment.scan;

import android.Manifest;
import android.view.View;

import androidx.core.app.ActivityCompat;

import com.ziacare.facilitator.android.activity.ScanActivity;
import com.ziacare.facilitator.android.dialog.ReferNoDialog;
import com.ziacare.facilitator.data.model.api.UserModel;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.Log;
import com.ziacare.facilitator.vendor.android.java.PermissionChecker;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.transformer.SingleTransformer;
import com.google.zxing.Result;
import com.ziacare.facilitator.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;
import icepick.State;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ScanFragment extends BaseFragment implements ZXingScannerView.ResultHandler, ReferNoDialog.Callback {
	public static final String TAG = ScanFragment.class.getName().toString();

	private int PERMISSION_CAMERA = 787;

	private ScanActivity activity;

	@State String resultQR;

	@BindView(R.id.scannerView) 				ZXingScannerView scannerView;

	String user_code = "";

	public static ScanFragment newInstance(String user_code) {
		ScanFragment fragment = new ScanFragment();
		fragment.user_code = user_code;
		return fragment;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.fragment_scan;
	}

	@Override
	public void onViewReady() {
		activity = (ScanActivity) getContext();
		activity.getActivityIconBTN().setVisibility(View.GONE);
		if(PermissionChecker.checkPermissions(getActivity(), Manifest.permission.CAMERA, PERMISSION_CAMERA)){
			startCamera();
		}else{
			ActivityCompat.requestPermissions(activity, new String[] { Manifest.permission.CAMERA }, PERMISSION_CAMERA);
		}
	}

	private void startCamera(){
		scannerView.setResultHandler(this);
		scannerView.startCamera();
	}

	@Override
	public void handleResult(Result result) {
		if(result != null){
			Log.d("result",result.getText());
			Auth.getDefault().getprofileScan(getContext(), user_code, result.getText());
		}
	}

	@OnClick(R.id.inputBTN)
	void inputBTN(){
		ReferNoDialog.newInstance(this).show(getChildFragmentManager(),TAG);
	}

	@Override
	public void onSuccess(String userCode) {
		Auth.getDefault().getprofileScan(getContext(), user_code, userCode);
		//Toast.makeText(getContext(),userCode, Toast.LENGTH_LONG).show();
	}

	@Subscribe
	public void onResponse(Auth.ProfileScanResponse response){
		SingleTransformer<UserModel> singleTransformer = response.getData(SingleTransformer.class);
		if(singleTransformer.status){
			ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
			activity.startMainActivity("home");
		}
		else{
			ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
			scannerView.resumeCameraPreview(this);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	@Override
	public void onResume() {
		super.onResume();
		activity.setTitle("Connect Physical Card");
		/*if(PermissionChecker.checkPermissions(getActivity(), Manifest.permission.CAMERA, PERMISSION_CAMERA)){
			startCamera();
		}*/
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (requestCode == PERMISSION_CAMERA) {
			if(activity.isAllPermissionResultGranted(grantResults)){
				startCamera();
			}else{
				activity.onBackPressed();
			}
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		scannerView.stopCamera();
	}
}
