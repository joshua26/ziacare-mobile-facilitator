package com.ziacare.facilitator.android.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ziacare.facilitator.R;
import com.ziacare.facilitator.data.model.api.CardModel;
import com.ziacare.facilitator.data.model.api.CardModel;
import com.ziacare.facilitator.vendor.android.base.BaseRecylerViewAdapter;
import com.ziacare.facilitator.vendor.android.java.Log;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class CardListRecyclerViewAdapter  extends BaseRecylerViewAdapter<CardListRecyclerViewAdapter.ViewHolder, CardModel> implements Filterable {

    private ClickListener clickListener;
    List<CardModel> reportsList;
    List<CardModel> filteredReportList;

    public CardListRecyclerViewAdapter(Context context, List<CardModel> reportsList) {
        super(context);
        this.reportsList = reportsList;
        this.filteredReportList = reportsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_card));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(filteredReportList.get(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.adapterCON.setOnClickListener(this);

        try {
            holder.cardNumberTXT.setText(holder.getItem().card_number);
            holder.refCodeTXT.setText(holder.getItem().reference_code);
            holder.provinceTXT.setText(holder.getItem().province_name);
            holder.statusTXT.setText(holder.getItem().status);
            holder.redeemTXT.setText(holder.getItem().redeem_at);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        try {
            return filteredReportList.size();
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String searchString = charSequence.toString().toLowerCase();

                if (searchString.isEmpty()) {

                    filteredReportList = reportsList;

                } else {
                    List<CardModel> tempfilteredReportList = new ArrayList<>();

                    for (CardModel model : reportsList) {
                        try {
                            if (model.status.toLowerCase().contains(searchString) || model.reference_code.toLowerCase().contains(searchString) ||
                                    model.card_number.toLowerCase().contains(searchString) || model.province_name.toLowerCase().contains(searchString) ||
                                    model.redeem_at.toLowerCase().contains(searchString)) {
                                tempfilteredReportList.add(model);
                                Log.d("searched", tempfilteredReportList);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    filteredReportList = tempfilteredReportList;

                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredReportList;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredReportList = (ArrayList<CardModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder {

        @BindView(R.id.adapterCON) View adapterCON;
        @BindView(R.id.botNavView) View botNavView;
        @BindView(R.id.cardNumberTXT) TextView cardNumberTXT;
        @BindView(R.id.refCodeTXT) TextView refCodeTXT;
        @BindView(R.id.provinceTXT) TextView provinceTXT;
        @BindView(R.id.statusTXT) TextView statusTXT;
        @BindView(R.id.redeemTXT)  TextView redeemTXT;
        @BindView(R.id.imageView) ImageView imageView;

        public ViewHolder(View view) {
            super(view);
        }

        public CardModel getItem() {
            return (CardModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.adapterCON:
                if (clickListener != null) {
                    clickListener.onItemClick((CardModel) v.getTag());
                }
                break;
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(CardModel model);
    }
} 