package com.ziacare.facilitator.android.dialog;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ziacare.facilitator.R;
import com.ziacare.facilitator.data.model.api.RequestModel;
import com.ziacare.facilitator.data.model.api.UserModel;
import com.ziacare.facilitator.vendor.android.base.BaseDialog;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class AttachmentDialog extends BaseDialog {
	public static final String TAG = AttachmentDialog.class.getName().toString();

	private Callback callback;
	private Context context;
	private RequestModel model;
	private UserModel userModel;
	private int post = 0;

	@BindView(R.id.imageView)									ImageView imageView;
	@BindView(R.id.prevBTN) 									ImageView prevBTN;
	@BindView(R.id.nextBTN) 									ImageView nextBTN;

	public static AttachmentDialog newInstance(Context context, Callback callback, RequestModel model) {
		AttachmentDialog dialog = new AttachmentDialog();
		dialog.context = context;
		dialog.callback = callback;
		dialog.model = model;
		return dialog;
	}

	public static AttachmentDialog newInstance(Context context, UserModel model) {
		AttachmentDialog dialog = new AttachmentDialog();
		dialog.context = context;
		dialog.userModel = model;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_attachment;
	}

	@Override
	public void onViewReady() {
		try{
			if(!userModel.validID.filename.equals("")){
				setValidID(userModel);
			}
		}catch (NullPointerException e){
			post = post + 1;
			setData(post - 1);
		}
	}

	private void setData(int val) {
		Glide.with(context)
				.load(model.attachment.data.get(val).fullPath)
				.apply(new RequestOptions()
						.placeholder(R.drawable.user_ic)
						.error(R.drawable.user_ic))
				.into(imageView);
		if (model.attachment.data.size() > 1){
			if (post == 1){
				prevBTN.setVisibility(View.INVISIBLE);
				nextBTN.setVisibility(View.VISIBLE);
			} else {
				if (post != model.attachment.data.size()){
					prevBTN.setVisibility(View.VISIBLE);
					nextBTN.setVisibility(View.VISIBLE);
				} else {
					prevBTN.setVisibility(View.VISIBLE);
					nextBTN.setVisibility(View.INVISIBLE);
				}
			}
		} else {
			prevBTN.setVisibility(View.INVISIBLE);
			nextBTN.setVisibility(View.INVISIBLE);
		}
	}

	private void setValidID(UserModel model) {
		Glide.with(context)
				.load(model.validID.fullPath)
				.apply(new RequestOptions()
						.placeholder(R.drawable.user_ic)
						.error(R.drawable.user_ic))
				.into(imageView);
			prevBTN.setVisibility(View.INVISIBLE);
			nextBTN.setVisibility(View.INVISIBLE);
	}

	public interface Callback{
		void onSuccess(String date);
	}

	@OnClick(R.id.closeBTN)
	void closeBTN(){
		dismiss();
	}


	@OnClick(R.id.prevBTN)
	void prevBTN(){
		post = post - 1;
		setData(post - 1);
	}

	@OnClick(R.id.nextBTN)
	void nextBTN(){
		post = post + 1;
		setData(post - 1);
	}


	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}
}
