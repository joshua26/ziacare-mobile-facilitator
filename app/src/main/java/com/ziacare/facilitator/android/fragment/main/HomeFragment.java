package com.ziacare.facilitator.android.fragment.main;

import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.MainActivity;
import com.ziacare.facilitator.android.adapter.LimitedRecyclerViewAdapter;
import com.ziacare.facilitator.data.model.api.TransactionHistoryModel;
import com.ziacare.facilitator.data.model.api.UserModel;
import com.ziacare.facilitator.data.preference.UserData;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.Log;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.transformer.CollectionTransformer;
import com.ziacare.facilitator.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class HomeFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, LimitedRecyclerViewAdapter.ClickListener{
    public static final String TAG = HomeFragment.class.getName().toString();

    private MainActivity activity;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    private LinearLayoutManager linearLayoutManager;
    private LimitedRecyclerViewAdapter adapter;

    @BindView(R.id.nameTXT)                     TextView nameTXT;
    @BindView(R.id.imageIV)                     CircleImageView imageIV;
    @BindView(R.id.addressTXT)                  TextView addressTXT;
    @BindView(R.id.companyNameTXT)              TextView companyNameTXT;
    @BindView(R.id.jobTitleTXT)                 TextView jobTitleTXT;
    @BindView(R.id.userIDTXT)                   TextView userIDTXT;
    @BindView(R.id.placeHolderTXT)              TextView placeHolderTXT;
    @BindView(R.id.scannedTXT)                  TextView scannedTXT;
    @BindView(R.id.currentRequestRV)            RecyclerView currentRequestRV;
    @BindView(R.id.homeSRL)                     SwipeRefreshLayout homeSRL;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_home;
    }

    @Override
    public void onViewReady() {
        activity = (MainActivity) getContext();
        homeSRL.setColorSchemeResources(R.color.colorPrimary);
        setUpListView();
        getProfile();
    }

    private void getProfile(){
        Auth.getDefault().myProfile(activity);
    }
    private void setUpListView(){
        linearLayoutManager = new LinearLayoutManager(getContext());
        currentRequestRV.setLayoutManager(linearLayoutManager);
        homeSRL.setOnRefreshListener(this);
    }

    public void userInfo(UserModel data){
        Glide.with(activity)
                .load(data.avatar.thumb_path)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.user_ic)
                        .error(R.drawable.user_ic))
                .into(imageIV);
        nameTXT.setText(data.name);
        String address = data.streetName + " " + data.brgy + " " + data.city + " " + data.zipcode;
        addressTXT.setText(address);
        companyNameTXT.setText(data.birthday);
        jobTitleTXT.setText(data.dateCreated.dateDb);
        userIDTXT.setText(data.code);
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setTitle("Welcome " + UserData.getUserModel().firstname);
        getProfile();
        refreshList();
    }

    @OnClick(R.id.viewAllTXT)
    void viewAllTXT(){
        activity.startRegisterActivity("transact");
    }

    @OnClick(R.id.scanCardBTN)
    void scanCardBTN(){
        activity.startScanActivity("list",null);
    }

    @OnClick(R.id.kycBTN)
    void onKYCBTN(){
        activity.startBeneficiaryActivity("scan",null);
    }

    @OnClick(R.id.addBeneBTN)
    void addBeneBTN(){
        activity.startRegisterActivity("signup");
    }

    @Subscribe
    public void onResponse(Auth.TransListResponse response){
        CollectionTransformer<TransactionHistoryModel> collectionTransformer = response.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            adapter = new LimitedRecyclerViewAdapter(activity, collectionTransformer.data);
            currentRequestRV.setAdapter(adapter);
            adapter.setNewData(collectionTransformer.data);
            adapter.setClickListener(this::onItemClick);
            scannedTXT.setText(String.valueOf(collectionTransformer.total));
        }
    }

    @Subscribe
    public void onResponse(Auth.MyProfileResponse response){
        SingleTransformer<UserModel> singleTransformer = response.getData(SingleTransformer.class);
        if (singleTransformer.status){
            userInfo(singleTransformer.data);
            UserData.insert(singleTransformer.data);
        } else {
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }


    public void refreshList(){
        Auth.getDefault().transactionList(getContext(), UserData.getUserModel().code, homeSRL);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onItemClick(TransactionHistoryModel model) {
        if(model.kyc_status.equals("pending")){
            activity.startBeneficiaryActivity("preview",model);
        }
        else if(model.has_physical_card){
            activity.startBeneficiaryActivity("view",model);
        }else{
            activity.startScanActivity("scan",model.qrcode);
        }

       /* if(model.kyc_status.equals("pending")){
            activity.startKYCActivity("id", model.qrcode);
        }
        else{
            //ToastMessage.show(activity,"User Already Verified", ToastMessage.Status.SUCCESS);
            if(!model.has_physical_card){
                activity.startScanActivity("scan",model.qrcode);
            }
        }*/
    }
}
