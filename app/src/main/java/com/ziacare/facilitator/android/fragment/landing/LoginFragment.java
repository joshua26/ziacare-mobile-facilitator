package com.ziacare.facilitator.android.fragment.landing;

import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.ziacare.facilitator.BuildConfig;
import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.LandingActivity;
import com.ziacare.facilitator.data.model.api.UserModel;
import com.ziacare.facilitator.data.preference.UserData;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class
LoginFragment extends BaseFragment {
    public static final String TAG = LoginFragment.class.getName().toString();

    private LandingActivity landingActivity;

    @BindView(R.id.emailET)              EditText usernameET;
    @BindView(R.id.passET)              EditText passwordET;
    @BindView(R.id.loginBTN)                TextView loginBTN;
    @BindView(R.id.forgotBTN)               TextView forgotBTN;

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_login;
    }

    @Override
    public void onViewReady() {
        landingActivity = (LandingActivity) getContext();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        try{
            SingleTransformer<UserModel> singleTransformer = loginResponse.getData(SingleTransformer.class);
            if (singleTransformer.status){
                UserData.insert(singleTransformer.data);
                UserData.insert(UserData.AUTHORIZATION, singleTransformer.token);
                ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
                landingActivity.startMainActivity("home");
            } else{
                ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
            }
            Log.e("Message", UserData.getString(UserData.AUTHORIZATION));

        } catch(Exception e){

        }
    }

    @OnClick(R.id.loginBTN)
    void signInBTNClicked(){
        switch(BuildConfig.FLAVOR){
            case "bataan":
                Auth.getDefault().login(getContext(), usernameET.getText().toString(), passwordET.getText().toString(),"D01");
                break;
            default:
                Auth.getDefault().login(getContext(), usernameET.getText().toString(), passwordET.getText().toString());
                break;
        }
    }

    /*@OnClick(R.id.signUpBTN)
    void registerBTNClicked(){
        landingActivity.startRegisterActivity("signup");
    }*/

    @OnClick(R.id.forgotBTN)
    void forgotPassBTNClicked(){
        landingActivity.startRegisterActivity("forgot");
    }

}
