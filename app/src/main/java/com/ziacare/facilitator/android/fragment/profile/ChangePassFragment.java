package com.ziacare.facilitator.android.fragment.profile;

import android.widget.EditText;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.ProfileActivity;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ChangePassFragment extends BaseFragment {
    public static final String TAG = ChangePassFragment.class.getName().toString();

    private ProfileActivity activity;

    @BindView(R.id.oldPassET)                           EditText oldPassET;
    @BindView(R.id.passET)                              EditText passET;
    @BindView(R.id.confirmpassET)                       EditText confirmpassET;

    public static ChangePassFragment newInstance() {
        ChangePassFragment fragment = new ChangePassFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_change_pass;
    }

    @Override
    public void onViewReady() {
        activity = (ProfileActivity) getContext();
        activity.setSettingsTitle("Change Password");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @OnClick(R.id.saveBTN)
    void saveBTN(){
        Auth.getDefault().update_pass(activity,oldPassET.getText().toString().trim()
        ,passET.getText().toString().trim()
        ,confirmpassET.getText().toString().trim());
    }

    @Subscribe
    public void onResponse(Auth.UpdateAvatarResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if (baseTransformer.status){
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
            activity.onBackPressed();
        } else {
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.FAILED);
            if (!ErrorResponseManger.first(baseTransformer.error.current_password).equals("")){
                oldPassET.setError(ErrorResponseManger.first(baseTransformer.error.current_password));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.password).equals("")){
                passET.setError(ErrorResponseManger.first(baseTransformer.error.password));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.password_confirmation).equals("")){
                confirmpassET.setError(ErrorResponseManger.first(baseTransformer.error.password_confirmation));
            }

        }
    }
}
