package com.ziacare.facilitator.android.fragment.landing;

import static com.ziacare.facilitator.BuildConfig.FLAVOR;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.LandingActivity;
import com.ziacare.facilitator.data.model.api.UserModel;
import com.ziacare.facilitator.data.preference.UserData;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SplashFragment extends BaseFragment {
    public static final String TAG = SplashFragment.class.getName().toString();

    private LandingActivity landingActivity;
    private Runnable runnable;
    private Handler handler;
    private static final int PERMISSION_READ_STATE = 1;

    public static SplashFragment newInstance() {
        SplashFragment fragment = new SplashFragment();
        return fragment;
    }

    @BindView(R.id.splashIV)
    ImageView splashIV;

    @Override
    public void onViewReady() {
        landingActivity = (LandingActivity) getContext();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (PERMISSION_READ_STATE == requestCode) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                refreshToken();
            } else {
                Toast.makeText(getActivity(), "Permission not granted", Toast.LENGTH_LONG).show();
                landingActivity.onBackPressed();
            }
        }
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_splash;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

        if(FLAVOR.equalsIgnoreCase("bataan")){
            Glide.with(landingActivity)
                    .load(R.drawable.bataan_logo)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.bataan_logo)
                            .error(R.drawable.bataan_logo))
                    .into(splashIV);
        }
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(landingActivity, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(landingActivity,new String[] { Manifest.permission.READ_PHONE_STATE}, PERMISSION_READ_STATE);
        }
        else {
            refreshToken();
        }
    }

    private void refreshToken() {
        Auth.getDefault().refreshToken(getContext());
    }

    @Subscribe
    public void onResponse(Auth.RefreshTokenResponse response) {
        try{
            SingleTransformer<UserModel> userTransformer = response.getData(SingleTransformer.class);
            if (userTransformer.status) {
                UserData.insert(userTransformer.data);
                UserData.insert(UserData.AUTHORIZATION, userTransformer.token);
                landingActivity.startMainActivity("home");
                ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.SUCCESS);
            } else {
                landingActivity.openLoginFragment();
                UserData.clearData();
            }
        }catch(Exception e){
            landingActivity.openLoginFragment();
            UserData.clearData();
        }

    }
}
