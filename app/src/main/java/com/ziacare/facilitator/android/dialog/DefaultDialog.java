package com.ziacare.facilitator.android.dialog;

import android.content.Context;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.vendor.android.base.BaseDialog;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class DefaultDialog extends BaseDialog {
	public static final String TAG = DefaultDialog.class.getName().toString();

	private Callback callback;
	private Context context;

	public static DefaultDialog newInstance(Context context,Callback callback) {
		DefaultDialog dialog = new DefaultDialog();
		dialog.context = context;
		dialog.callback = callback;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_default;
	}

	@Override
	public void onViewReady() {

	}

	public interface Callback{
		void onSuccess(String date);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}
}
