package com.ziacare.facilitator.android.fragment.profile;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.ProfileActivity;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.widget.WrapContentWebView;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class PrivacyPolicyFragment extends BaseFragment {
    public static final String TAG = PrivacyPolicyFragment.class.getName().toString();

    private ProfileActivity activity;

    @BindView(R.id.policyTXT)               WrapContentWebView policyTXT;

    public static PrivacyPolicyFragment newInstance() {
        PrivacyPolicyFragment fragment = new PrivacyPolicyFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_privacy;
    }

    @Override
    public void onViewReady() {
        activity = (ProfileActivity) getContext();
        activity.setSettingsTitle("Privacy Policy");
//        policyTXT.setText(Html.fromHtml(getString(R.string.policy)));
        policyTXT.loadDataWithBaseURL(null, getString(R.string.policy), "text/html", "utf-8", null);

    }

    @Override
    public void onStart() {
        super.onStart();
//        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
//        EventBus.getDefault().unregister(this);
        super.onStop();
    }


}
