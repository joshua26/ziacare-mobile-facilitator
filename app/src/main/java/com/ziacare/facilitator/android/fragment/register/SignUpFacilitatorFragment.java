package com.ziacare.facilitator.android.fragment.register;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.asksira.bsimagepicker.BSImagePicker;
import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.RegisterActivity;
import com.ziacare.facilitator.android.dialog.CalendarDialog;
import com.ziacare.facilitator.android.dialog.ProfileDialog;
import com.ziacare.facilitator.config.Keys;
import com.ziacare.facilitator.data.preference.UserData;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.ImageManager;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.android.java.facedetect.FaceCenterCircleView.FaceCenterCrop;
import com.ziacare.facilitator.vendor.android.java.facedetect.Utils.Imageutils;
import com.ziacare.facilitator.vendor.android.java.facedetect.Utils.ProgressBarUtil.ProgressBarData;
import com.ziacare.facilitator.vendor.android.java.facedetect.Utils.ProgressBarUtil.ProgressUtils;
import com.ziacare.facilitator.vendor.server.request.APIRequest;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.util.ErrorResponseManger;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraOptions;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.VideoResult;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;
import static com.ziacare.facilitator.vendor.android.java.facedetect.Utils.Imageutils.GALEERY_REQUEST_CODE;
import static com.ziacare.facilitator.vendor.android.java.facedetect.Utils.Imageutils.SCANNER_REQUEST_CODE;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SignUpFacilitatorFragment extends BaseFragment implements BSImagePicker.OnSingleImageSelectedListener, ImageManager.Callback, CalendarDialog.DateTimePickerListener, ProfileDialog.Callback, View.OnClickListener, Imageutils.ImageAttachmentListener  {
    public static final String TAG = SignUpFacilitatorFragment.class.getName().toString();


    private static final int REQUEST_CAMERA_PERMISSION_RESULT = 0;
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION_RESULT = 1;

    private RegisterActivity registerActivity;
    private APIRequest apiRequest;
    private File videoFile;
    private File mVideoFolder;
    private String mVideoFileName;
    private AnimationDrawable splashAnimation;
    private String outputDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).getAbsolutePath();

    public static SignUpFacilitatorFragment newInstance() {
        SignUpFacilitatorFragment fragment = new SignUpFacilitatorFragment();
        return fragment;
    }

    @BindView(R.id.signUpBTN)
    TextView signUpBTN;
    @BindView(R.id.dobTXT)
    TextView dobTXT;
    @BindView(R.id.validIDTXT)
    TextView validIDTXT;
    @BindView(R.id.genderTXT)
    TextView genderTXT;
    //    @BindView(R.id.typeTXT)                         TextView typeTXT;
    @BindView(R.id.fnameET)
    EditText fnameET;
    @BindView(R.id.mnameET)
    EditText mnameET;
    @BindView(R.id.lnameET)
    EditText lnameET;
    @BindView(R.id.streetET)
    EditText streetET;
    @BindView(R.id.zipcodeET)
    EditText zipcodeET;
    @BindView(R.id.userET)
    EditText userET;
    @BindView(R.id.phoneET)
    EditText phoneET;
    @BindView(R.id.emailET)
    EditText emailET;
    @BindView(R.id.passET)
    EditText passET;
    @BindView(R.id.confirmpassET)
    EditText confirmpassET;
    @BindView(R.id.tinET)
    EditText tinET;
    @BindView(R.id.sssET)
    EditText sssET;
    @BindView(R.id.phicET)
    EditText phicET;
    @BindView(R.id.spinnerGender)
    Spinner spinnerGender;
    //    @BindView(R.id.spinnerType)                     Spinner spinnerType;
    @BindView(R.id.cityET)
    EditText cityET;
    @BindView(R.id.brgyET)
    EditText brgyET;
    @BindView(R.id.cameraLayout)
    View cameraLayout;
    @BindView(R.id.mainLayout)
    View mainLayout;
    @BindView(R.id.cameraView)
    CameraView cameraView;
    @BindView(R.id.start)
    ImageView start;
    @BindView(R.id.instructionsIMG)
    ImageView instructionsIMG;
    @BindView(R.id.instructionsTXT)
    TextView instructionsTXT;
    @BindView(R.id.profileIMG)          ImageView profileIMG;
    @BindView(R.id.uploadBTN)           TextView uploadBTN;
    @BindView(R.id.spinnerPWD)          Spinner spinnerPWD;
    @BindView(R.id.spinnerSenior)       Spinner spinnerSenior;
    //@BindView(R.id.videoTXT)                        TextView videoTXT;

    String dob;
    String fname;
    String mname;
    String lname;
    String street;
    String zipcode;
    String phone;
    String user;
    String email;
    String pass;
    String confirmpass;
    String tin;
    String sss;
    String phic;
    String gender;
    String userType;
    String brgy;
    String city;
    File file, profilePic;

    Imageutils imageutils;
    Imageutils.ImageAttachmentListener imageAttachmentListener;
    FaceCenterCrop faceCenterCrop;
    FaceCenterCrop.FaceCenterCropListener faceCenterCropListener;
    @BindView(R.id.ivProfile)
    CircleImageView ivProfile;
    @BindView(R.id.ivProfile2)
    CircleImageView ivProfile2;
    @BindView(R.id.ivProfile3)
    CircleImageView ivProfile3;
    File smile, left, right;
    ProgressUtils progressUtils;
    int num = 0;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_signup_facilitator;
    }

    @Override
    public void onViewReady() {
        registerActivity = (RegisterActivity) getContext();
        signUpBTN.setOnClickListener(this);
        dobTXT.setOnClickListener(this);
        validIDTXT.setOnClickListener(this);
        // videoTXT.setOnClickListener(this);
        start.setOnClickListener(this);

        createVideoFolder();
        checkWriteStoragePermission();

        //FOR FACE DETECTION
        imageutils = new Imageutils(registerActivity, this, true);
        imageutils.setImageAttachment_callBack(this);
        progressUtils = new ProgressUtils(registerActivity);
        faceCenterCrop = new FaceCenterCrop(registerActivity, 100, 100, 1);

        phoneET.addTextChangedListener(new TextWatcher() {
            int prevL = 0;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                prevL = phoneET.getText().toString().length();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int length = editable.length();
                if ((prevL < length) && (length == 3 || length == 7 || length == 11)) {
                    String data = phoneET.getText().toString();
                    phoneET.setText(data + "-");
                    phoneET.setSelection(length + 1);
                }
            }
        });
        cameraView.setVideoMaxDuration(2000);
        cameraView.setLifecycleOwner(getViewLifecycleOwner());


        cameraView.addCameraListener(new CameraListener() {
            @Override
            public void onCameraOpened(@NonNull CameraOptions options) {
                super.onCameraOpened(options);
            }

            @Override
            public void onCameraClosed() {
                super.onCameraClosed();
            }

            @Override
            public void onVideoTaken(@NonNull VideoResult result) {
                super.onVideoTaken(result);
                videoFile = result.getFile();
                // videoTXT.setText(result.getFile().getAbsolutePath());
            }

            @Override
            public void onVideoRecordingStart() {
                super.onVideoRecordingStart();
                start.setVisibility(View.INVISIBLE);
                Toast.makeText(getContext(), "Video Recording Started", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onVideoRecordingEnd() {
                super.onVideoRecordingEnd();
                start.setVisibility(View.VISIBLE);
                mainLayout.setVisibility(View.VISIBLE);
                cameraLayout.setVisibility(View.GONE);
                Toast.makeText(getContext(), "Video Recording Successful", Toast.LENGTH_LONG).show();

            }
        });

        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(registerActivity,
                R.array.gender, R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(adapter2);

        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(registerActivity,
                R.array.yesOrNo, R.layout.simple_spinner_item);
        adapter3.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinnerPWD.setAdapter(adapter3);
        spinnerSenior.setAdapter(adapter3);

    }

    private String getGender() {
        String value;
        switch (spinnerGender.getSelectedItem().toString()) {
            case "Female":
                value = "female";
                break;
            case "Male":
            default:
                value = "male";
                break;
        }
        return value;
    }

    private void createVideoFolder() {
        File movieFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        mVideoFolder = new File(movieFile, "malasakit");
        if (!mVideoFolder.exists()) {
            mVideoFolder.mkdirs();
        }
    }

    private void checkWriteStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                try {
                    createVideoFileName();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(getContext(), "app needs to be able to save videos", Toast.LENGTH_SHORT).show();
                }
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION_RESULT);
            }
        } else {
            try {
                createVideoFileName();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private File createVideoFileName() throws IOException {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String prepend = "VIDEO_" + timestamp + "_";
        videoFile = File.createTempFile(prepend, ".mp4", mVideoFolder);
        mVideoFileName = videoFile.getAbsolutePath();
        return videoFile;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CAMERA_PERMISSION_RESULT) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(getContext(),
                        "Application will not run without camera services", Toast.LENGTH_SHORT).show();
            }
            if (grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(),
                        "Application will not have audio on record", Toast.LENGTH_SHORT).show();
            }
        }
        if (requestCode == REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION_RESULT) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "Permission successfully granted!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "App needs to save video to run", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signUpBTN:
                preReg();
                break;
            case R.id.dobTXT:

                CalendarDialog.newInstance(this::forDisplay).show(getChildFragmentManager(), TAG);


                break;
            case R.id.validIDTXT:
                openFileChooser();
                break;
           /* case R.id.videoTXT:
                mainLayout.setVisibility(View.GONE);
                cameraLayout.setVisibility(View.VISIBLE);
                break;*/
            case R.id.start:
                try {
                    cameraView.takeVideo(createVideoFileName());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                instructionsIMG.setBackgroundResource(R.drawable.splash);
                splashAnimation = (AnimationDrawable) instructionsIMG.getBackground();
                new CountDownTimer(200, 200) {

                    public void onTick(long millisUntilFinished) {
                        splashAnimation.start();
                        instructionsTXT.setText("Look to your left");
                    }

                    public void onFinish() {
                        instructionsTXT.setText("Look to your right");
                    }
                }.start();
                break;
        }
    }


    public void preReg() {
        getClearError();

        dob = dobTXT.getText().toString();
        fname = fnameET.getText().toString().trim();
        mname = mnameET.getText().toString().trim();
        lname = lnameET.getText().toString().trim();
        street = streetET.getText().toString().trim();
        zipcode = zipcodeET.getText().toString().trim();
        phone = phoneET.getText().toString().trim().replace("-", "");

        user = userET.getText().toString().trim();
        email = emailET.getText().toString().trim();
        pass = passET.getText().toString().trim();
        confirmpass = confirmpassET.getText().toString().trim();
        tin = tinET.getText().toString().trim();
        sss = sssET.getText().toString().trim();
        phic = phicET.getText().toString().trim();
        gender = getGender();
//        userType = getUserType();
        brgy = brgyET.getText().toString().trim();
        city = cityET.getText().toString().trim();
        String facilitator_id = String.valueOf(UserData.getUserModel().userId);

        try {
            if (selfieChecker()) {
                apiRequest = Auth.getDefault().registerFacilitator(registerActivity)
                        .addParameter(Keys.FIRSTNAME, fname)
                        .addParameter(Keys.LASTNAME, lname)
                        .addParameter(Keys.MIDDLENAME, mname)
                        .addParameter(Keys.STREET_NAME, street)
                        .addParameter(Keys.ZIPCODE, zipcode)
                        .addParameter(Keys.PHONE_NUMBER, phone)
                        .addParameter(Keys.BIRTHDATE, dob)
                        //.addParameter(Keys.BRGY, brgy)
                        //.addParameter(Keys.CITY, city)
                        .addParameter(Keys.USERNAME, user)
                        .addParameter(Keys.EMAIL, email)
                        .addParameter(Keys.PASSWORD, pass)
                        .addParameter(Keys.PASSWORD_CONFIRM, confirmpass)
                        .addParameter(Keys.TIN_NO, tin)
                        .addParameter(Keys.SSS_NO, sss)
                        .addParameter(Keys.PHIC_NO, phic)
                        .addParameter(Keys.GENDER, gender)
                        .addParameter(Keys.TYPE, userType)
                        .addParameter(Keys.VALID_ID, file)
                        //.addParameter(Keys.VIDEO, videoFile)
                        .addParameter(Keys.FACILITATOR_ID, facilitator_id)
                        .addParameter("files[0][image]", getSmile())
                        .addParameter("files[0][type]", Keys.FRONT)
                        .addParameter("files[1][image]", getLeft())
                        .addParameter("files[1][type]", Keys.LEFT)
                        .addParameter("files[2][image]", getRight())
                        .addParameter("files[2][type]", Keys.RIGHT)
                        .addParameter(Keys.PWD,spinnerPWD.getSelectedItem().toString())
                        .addParameter(Keys.SENIOR,spinnerSenior.getSelectedItem().toString());
                        if(profilePic != null){
                            apiRequest.addParameter(Keys.SELFIE,profilePic);
                        }
                apiRequest.execute();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            Log.e("Faces", "null faces");
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        registerActivity.setTitle("Add Beneficiary");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.RegisterResponse loginResponse) {
        BaseTransformer baseTransformer = loginResponse.getData(BaseTransformer.class);
        if (baseTransformer.status) {
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
            registerActivity.startScanActivity("scan", baseTransformer.user_code);
        } else {
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.FAILED);
//            if (!ErrorResponseManger.first(baseTransformer.error.type).equals("")){
//                typeTXT.setError(ErrorResponseManger.first(baseTransformer.error.type));
//            }
            if (!ErrorResponseManger.first(baseTransformer.error.firstname).equals("")) {
                fnameET.setError(ErrorResponseManger.first(baseTransformer.error.firstname));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.lastname).equals("")) {
                lnameET.setError(ErrorResponseManger.first(baseTransformer.error.lastname));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.middlename).equals("")) {
                mnameET.setError(ErrorResponseManger.first(baseTransformer.error.middlename));
            }

            if (!ErrorResponseManger.first(baseTransformer.error.gender).equals("")) {
                genderTXT.setError(ErrorResponseManger.first(baseTransformer.error.gender));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.street_name).equals("")) {
                streetET.setError(ErrorResponseManger.first(baseTransformer.error.street_name));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.zipcode).equals("")) {
                zipcodeET.setError(ErrorResponseManger.first(baseTransformer.error.zipcode));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.phone_number).equals("")) {
                // phoneET.setError(ErrorResponseManger.first(baseTransformer.error.phone_number));
                phoneET.setError("Your Phone number is invalid format or already taken");
                phoneET.setHint(" Ex: 09123456789");
            }
            if (!ErrorResponseManger.first(baseTransformer.error.city).equals("")) {
                cityET.setError(ErrorResponseManger.first(baseTransformer.error.city));
            }

            if (!ErrorResponseManger.first(baseTransformer.error.brgy).equals("")) {
                brgyET.setError(ErrorResponseManger.first(baseTransformer.error.brgy));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.username).equals("")) {
                userET.setError(ErrorResponseManger.first(baseTransformer.error.username));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.email).equals("")) {
                emailET.setError(ErrorResponseManger.first(baseTransformer.error.email));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.password).equals("")) {
                passET.setError(ErrorResponseManger.first(baseTransformer.error.password));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.password_confirmation).equals("")) {
                confirmpassET.setError(ErrorResponseManger.first(baseTransformer.error.password_confirmation));
            }

            if (!ErrorResponseManger.first(baseTransformer.error.birthdate).equals("")) {
                dobTXT.setError(ErrorResponseManger.first(baseTransformer.error.birthdate));
            }


            if (!ErrorResponseManger.first(baseTransformer.error.valid_id).equals("")) {
                validIDTXT.setError(ErrorResponseManger.first(baseTransformer.error.valid_id));
            }

            if (!ErrorResponseManger.first(baseTransformer.error.tin_no).equals("")) {
                tinET.setError(ErrorResponseManger.first(baseTransformer.error.tin_no));
            }

            if (!ErrorResponseManger.first(baseTransformer.error.sss_no).equals("")) {
                sssET.setError(ErrorResponseManger.first(baseTransformer.error.sss_no));
            }

            if (!ErrorResponseManger.first(baseTransformer.error.phic_no).equals("")) {
                phicET.setError(ErrorResponseManger.first(baseTransformer.error.phic_no));
            }

        }
    }

    private void getClearError() {
//        typeTXT.setError(null);
        genderTXT.setError(null);
        dobTXT.setError(null);
        validIDTXT.setError(null);
    }


    private void openFileChooser() {
        BSImagePicker pickerDialog = new BSImagePicker.Builder("com.ziacare.facilitator.fileprovider").setTag("validID").build();
        pickerDialog.show(getChildFragmentManager(), "picker");

    }

    @OnClick(R.id.uploadBTN)
    void onUploadBTN(){
        BSImagePicker pickerDialog = new BSImagePicker.Builder("com.ziacare.facilitator.fileprovider").setTag("profile").build();
        pickerDialog.show(getChildFragmentManager(), "picker2");
    }

    @OnClick(R.id.profileIMG)
    void onProfileIMG(){
        BSImagePicker pickerDialog = new BSImagePicker.Builder("com.ziacare.facilitator.fileprovider").setTag("profile").build();
        pickerDialog.show(getChildFragmentManager(), "picker2");
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        Bitmap bitmap = null;
        try {
            if(tag.equals("validID")){
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);
                int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
                Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                ImageManager.getFileFromBitmap(getContext(), scaled, "image1", this);
            }
            else{
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);
                int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
                Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                ImageManager.getFileFromBitmap(getContext(), scaled, "image2", new ImageManager.Callback() {
                    @Override
                    public void success(File file) {
                        profilePic = file;
                    }
                });
                profileIMG.setImageBitmap(scaled);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void success(File file) {
        validIDTXT.setText(file.getName());
        this.file = file;
        validIDTXT.setError(null);
    }

    @Override
    public void forDisplay(String date) {
        dobTXT.setText(date);
        dobTXT.setError(null);
    }

    @Override
    public void onSuccess(ProfileDialog dialog) {
        registerActivity.onBackPressed();
        dialog.dismiss();
    }

    private FaceCenterCrop.FaceCenterCropListener getFaceCropResult() {
        if (faceCenterCropListener == null)
            faceCenterCropListener = new FaceCenterCrop.FaceCenterCropListener() {
                @Override
                public void onTransform(Bitmap updatedBitmap) {
                    Log.d("Time log", "Output is set");
                    ivProfile.setImageBitmap(updatedBitmap);
                    Toast.makeText(registerActivity, "We detected a face", Toast.LENGTH_SHORT).show();
                    progressUtils.dismissDialog();
                }

                @Override
                public void onFailure() {
                    Toast.makeText(registerActivity, "No face was detected", Toast.LENGTH_SHORT).show();
                    progressUtils.dismissDialog();

                }
            };

        return faceCenterCropListener;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: ");
        try {
            super.onActivityResult(requestCode, resultCode, data);
            imageutils.onActivityResult(requestCode, resultCode, data, num);

            if (requestCode == SCANNER_REQUEST_CODE && resultCode == RESULT_OK) {
                Log.d(TAG, "onActivityResult: " + SCANNER_REQUEST_CODE);
            } else if (requestCode == GALEERY_REQUEST_CODE && resultCode == RESULT_OK) {
                Log.d(TAG, "onActivityResult: " + GALEERY_REQUEST_CODE);

            }
        } catch (Exception ex) {
            Toast.makeText(registerActivity, ex.toString(),
                    Toast.LENGTH_SHORT).show();
        }

    }

    @OnClick(R.id.ivProfile)
    public void onViewClicked() {
        num = 1;
        imageutils.imagepicker(1, num);
    }

    @OnClick(R.id.ivProfile2)
    public void onViewClicked2() {
        num = 2;
        imageutils.imagepicker(1, num);
    }

    @OnClick(R.id.ivProfile3)
    public void onViewClicked3() {
        num = 3;
        imageutils.imagepicker(1, num);
    }

    @Override
    public void image_attachment(int from, String filename, Bitmap file, Uri uri, int num) {
        Log.d(TAG, "getImageAttachmentCallback: " + from + "\n" + num);

        if (from == SCANNER_REQUEST_CODE) {
            if (num == 1) {
                ivProfile.setImageBitmap(file);
                BitmapDrawable drawable = (BitmapDrawable) ivProfile.getDrawable();
                Bitmap bitmap = drawable.getBitmap();
                setSmile(imageutils.bitmapToFile(bitmap, "smile"));
            }
            if (num == 2) {
                ivProfile2.setImageBitmap(file);
                BitmapDrawable drawable = (BitmapDrawable) ivProfile2.getDrawable();
                Bitmap bitmap = drawable.getBitmap();
                setLeft(imageutils.bitmapToFile(bitmap, "left"));
            }
            if (num == 3) {
                ivProfile3.setImageBitmap(file);
                BitmapDrawable drawable = (BitmapDrawable) ivProfile3.getDrawable();
                Bitmap bitmap = drawable.getBitmap();
                setRight(imageutils.bitmapToFile(bitmap, "right"));
            }
        } else if (from == GALEERY_REQUEST_CODE) {
            Log.d("Time log", "IA callback triggered");

            ProgressBarData progressBarData = new ProgressBarData.ProgressBarBuilder()
                    .setCancelable(true)
                    .setProgressMessage("Processing")
                    .setProgressMessageColor(Color.parseColor("#4A4A4A"))
                    .setBackgroundViewColor(Color.parseColor("#FFFFFF"))
                    .setProgressbarTintColor(Color.parseColor("#FAC42A")).build();

            // ivProfile.setImageBitmap(file);

            progressUtils.showDialog(progressBarData);

            faceCenterCrop.detectFace(file, getFaceCropResult());
        }
    }

    public boolean selfieChecker() {
        if (this.smile != null && this.left == null && this.right == null) {
            ToastMessage.show(registerActivity, "Please upload Left and Right selfie", ToastMessage.Status.FAILED);
            return false;
        } else if (this.smile == null && this.left != null && this.right == null) {
            ToastMessage.show(registerActivity, "Please upload Front and Right selfie", ToastMessage.Status.FAILED);
            return false;
        } else if (this.smile == null && this.left == null && this.right != null) {
            ToastMessage.show(registerActivity, "Please upload Front and Left selfie", ToastMessage.Status.FAILED);
            return false;
        } else if (this.smile != null && this.left != null && this.right == null) {
            ToastMessage.show(registerActivity, "Please upload Right selfie", ToastMessage.Status.FAILED);
            return false;
        } else if (this.smile != null && this.left == null && this.right != null) {
            ToastMessage.show(registerActivity, "Please upload Left selfie", ToastMessage.Status.FAILED);
            return false;
        } else if (this.smile == null && this.left != null && this.right != null) {
            ToastMessage.show(registerActivity, "Please upload Front selfie", ToastMessage.Status.FAILED);
            return false;
        } else if (this.smile == null && this.left == null && this.right == null) {
            ToastMessage.show(registerActivity, "Please upload a selfie", ToastMessage.Status.FAILED);
            return false;
        } else {
            return true;
        }
    }

    public File getSmile() {
        return smile;
    }

    public void setSmile(File smile) {
        this.smile = smile;
    }

    public File getLeft() {
        return left;
    }

    public void setLeft(File left) {
        this.left = left;
    }

    public File getRight() {
        return right;
    }

    public void setRight(File right) {
        this.right = right;
    }
}
