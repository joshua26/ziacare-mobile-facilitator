package com.ziacare.facilitator.android.fragment.beneficiary;

import android.util.Log;
import android.widget.TextView;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.BeneficiaryActivity;
import com.ziacare.facilitator.android.fragment.DefaultFragment;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class GeneratedNewPasswordFragment extends BaseFragment {
    public static final String TAG = GeneratedNewPasswordFragment.class.getName().toString();

    public static GeneratedNewPasswordFragment newInstance(String password) {
        GeneratedNewPasswordFragment fragment = new GeneratedNewPasswordFragment();
        fragment.password = password;
        return fragment;
    }

    BeneficiaryActivity activity;

    @BindView(R.id.generatedPasswordTXT) TextView generatedPasswordTXT;

    String password;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_generate_new_password;
    }

    @Override
    public void onViewReady() {
        activity = (BeneficiaryActivity) getContext();

        generatedPasswordTXT.setText(password);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        activity.setTitle("Generated New Password");
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }
}
