package com.ziacare.facilitator.android.fragment.main;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.RegisterActivity;
import com.ziacare.facilitator.android.adapter.TransactionRecyclerViewAdapter;
import com.ziacare.facilitator.data.model.api.TransactionHistoryModel;
import com.ziacare.facilitator.data.preference.UserData;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.ziacare.facilitator.vendor.android.java.Log;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.request.APIRequest;
import com.ziacare.facilitator.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class TransactionFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, TransactionRecyclerViewAdapter.ClickListener {
    public static final String TAG = TransactionFragment.class.getName().toString();

    private RegisterActivity activity;
    private TransactionRecyclerViewAdapter adapter;
    private LinearLayoutManager manager;

    @BindView(R.id.swipeRL)         SwipeRefreshLayout swipeRL;
    @BindView(R.id.recyclerView)    RecyclerView recyclerView;
    @BindView(R.id.placeHolderTXT)  TextView placeHolderTXT;
    @BindView(R.id.progressBar)     ProgressBar progressBar;
    @BindView(R.id.searchET)        EditText searchET;

    public static TransactionFragment newInstance() {
        TransactionFragment fragment = new TransactionFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_transact;
    }

    @Override
    public void onViewReady() {
        activity = (RegisterActivity) getContext();
        activity.setTitle("Registered Users");
        //setAdapter();

        searchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(searchET.getText().toString().trim() != null && searchET.getText().length() != 0){
                    adapter.getFilter().filter(charSequence.toString());
                }
                else{
                    refreshList();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private void setAdapter() {
        manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        swipeRL.setOnRefreshListener(this);
        recyclerView.getRecycledViewPool().clear();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    public void refreshList() {
        Auth.getDefault().transactionList(getContext(), UserData.getUserModel().code, swipeRL);
        searchET.setEnabled(false);
    }

    @Subscribe
    public void onResponse(Auth.TransListResponse response){
        CollectionTransformer<TransactionHistoryModel> collectionTransformer = response.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            adapter = new TransactionRecyclerViewAdapter(getContext(), collectionTransformer.data);
            adapter.setClickListener(this);
            adapter.setNewData(collectionTransformer.data);
            setAdapter();
            searchET.setEnabled(true);
        }
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onItemClick(TransactionHistoryModel model) {
        if(model.kyc_status.equals("pending")){
            activity.startBeneficiaryActivity("preview",model);
        }
        else if(model.has_physical_card){
            activity.startBeneficiaryActivity("view",model);
        }else{
            activity.startScanActivity("scan",model.qrcode);
        }
    }

}
