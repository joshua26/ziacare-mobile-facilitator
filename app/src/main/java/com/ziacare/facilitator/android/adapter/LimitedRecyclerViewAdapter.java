package com.ziacare.facilitator.android.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ziacare.facilitator.R;
import com.ziacare.facilitator.data.model.api.TransactionHistoryModel;
import com.ziacare.facilitator.vendor.android.base.BaseRecylerViewAdapter;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class LimitedRecyclerViewAdapter extends BaseRecylerViewAdapter<LimitedRecyclerViewAdapter.ViewHolder, TransactionHistoryModel> {

    private TransactionRecyclerViewAdapter.ClickListener clickListener;
    List<TransactionHistoryModel> allList;
    List<TransactionHistoryModel> filteredList;

    private final int limit = 5;


    public LimitedRecyclerViewAdapter(Context context, List<TransactionHistoryModel> allList) {
        super(context);
        this.allList = allList;
        this.filteredList = allList;
    }

    @Override
    public LimitedRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LimitedRecyclerViewAdapter.ViewHolder(getDefaultView(parent, R.layout.adapter_transac));
    }

    @Override
    public void onBindViewHolder(LimitedRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.setItem(filteredList.get(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.adapterCON.setOnClickListener(this);

        try{
            Glide.with(getContext())
                    .load(filteredList.get(position).avatar.thumbPath)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.user_ic)
                            .error(R.drawable.user_ic))
                    .into(holder.imageView);

            String name = filteredList.get(position).firstname + " " + filteredList.get(position).lastname;

            holder.nameTXT.setText(name);

            String qrcode = filteredList.get(position).qrcode;
            String address = filteredList.get(position).address;


            if(qrcode == null){
                qrcode = "Not Verified";
            }
            if(address == null){
                address = filteredList.get(position).streetName + " " + filteredList.get(position).brgy  + " " + filteredList.get(position).city;
            }
            holder.refNumTXT.setText(qrcode);
            holder.addressTXT.setText(address);
            holder.kycTXT.setText("KYC Verified: "+ filteredList.get(position).kyc_status);
            if(filteredList.get(position).has_physical_card){
                holder.cardTXT.setVisibility(View.VISIBLE);
                holder.cardTXT.setText("Has physical card");
            }else{
                holder.cardTXT.setVisibility(View.GONE);
            }

        }catch (NullPointerException e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if(filteredList.size() > limit){
            return limit;
        }
        else
        {
            return filteredList.size();
        }
    }

    public int getCount(){
        return super.getItemCount();
    }


    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.adapterCON)
        View adapterCON;
        @BindView(R.id.botNavView)                  View botNavView;
        @BindView(R.id.nameTXT)                     TextView nameTXT;
        @BindView(R.id.refNumTXT)                     TextView refNumTXT;
        @BindView(R.id.addressTXT)                  TextView addressTXT;
        @BindView(R.id.kycTXT)                     TextView kycTXT;
        @BindView(R.id.cardTXT)                     TextView cardTXT;
        @BindView(R.id.imageIV)
        CircleImageView imageView;

        public ViewHolder(View view) {
            super(view);
        }

        public TransactionHistoryModel getItem() {
            return (TransactionHistoryModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.adapterCON:
                if (clickListener != null) {
                    clickListener.onItemClick((TransactionHistoryModel) v.getTag());
                }
                break;
        }
    }

    public void setClickListener(TransactionRecyclerViewAdapter.ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(TransactionHistoryModel model);
    }
}