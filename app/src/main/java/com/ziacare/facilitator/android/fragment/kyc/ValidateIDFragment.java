package com.ziacare.facilitator.android.fragment.kyc;

import android.graphics.Bitmap;
import android.net.Uri;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.asksira.bsimagepicker.BSImagePicker;
import com.ziacare.facilitator.BuildConfig;
import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.KYCActivity;
import com.ziacare.facilitator.data.model.api.RequestModel;

import com.ziacare.facilitator.request.KYC;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.android.java.facedetect.Utils.Imageutils;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class ValidateIDFragment extends BaseFragment implements BSImagePicker.OnSingleImageSelectedListener {
    public static final String TAG = ValidateIDFragment.class.getName().toString();

    public static ValidateIDFragment newInstance(String user_code,boolean isAddBene) {
        ValidateIDFragment fragment = new ValidateIDFragment();
        fragment.isAddBene = isAddBene;
        fragment.user_code = user_code;
        return fragment;
    }

    KYCActivity activity;
    Imageutils imageutils;

    String user_code;
    boolean isAddBene;

    @BindView(R.id.spinnerID) Spinner spinnerID;
    @BindView(R.id.idIV) ImageView idIV;

    ArrayList<String> idList, idCode;
    String codeID;
    File id;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_validate_id;
    }

    @Override
    public void onViewReady() {
        activity = (KYCActivity) getContext();

        imageutils = new Imageutils(activity);

        idList = new ArrayList<>();
        idCode = new ArrayList<>();

        spinnerID.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!idCode.isEmpty()){
                    codeID = idCode.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        activity.setTitle("Upload ID");
        if(idList.isEmpty()){
            KYC.getDefault().getValidIDList(activity);
        }
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(KYC.ValidIDListResponse response){
        CollectionTransformer<RequestModel> collectionTransformer = response.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            if(idList.isEmpty()){
                for(RequestModel model : collectionTransformer.data){
                    idList.add(model.name);
                    idCode.add(model.code);
                }
                idList.add("Others");
                idCode.add("others");
                spinnerID.setAdapter(new ArrayAdapter<String>(activity,android.R.layout.simple_spinner_dropdown_item, idList));
            }
        }else{
            ToastMessage.show(activity,collectionTransformer.msg,ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(KYC.ValidateIDResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if(baseTransformer.status){
            activity.setId(id);
            activity.setIdCode(codeID);
            ToastMessage.show(activity,baseTransformer.msg,ToastMessage.Status.SUCCESS);
            activity.openSelfieFragment(user_code,true);
        }else{
            ToastMessage.show(activity,baseTransformer.msg,ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(KYC.UploadIDResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(activity,baseTransformer.msg,ToastMessage.Status.SUCCESS);
            if(isAddBene){
                activity.openKYCFragment(user_code);
            }else{
                activity.onBackPressed();
            }
            //activity.openSelfieFragment(user_code);
        }else{
            ToastMessage.show(activity,baseTransformer.msg,ToastMessage.Status.FAILED);
        }
    }

    @OnClick(R.id.addBTN)
    void onAddIDBTN(){
        BSImagePicker pickerDialog = new BSImagePicker.Builder(BuildConfig.APPLICATION_ID +".fileprovider").build();
        pickerDialog.show(getChildFragmentManager(), "picker");
    }

    @OnClick(R.id.submitBTN)
    void onSubmitBTN(){
        //KYC.getDefault().validateID(activity,id,codeID,user_code);
        KYC.getDefault().uploadID(activity,id,codeID,user_code);
       // Log.d("Validate ID Data", codeID);
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {

        //idIV.setImageURI(uri);                 .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE))
        Bitmap bitmap = imageutils.getImage_FromUri(uri,680,720);
        idIV.setImageBitmap(bitmap);

        File file = new File(uri.getPath());
        id = file;

        //Toast.makeText(activity, Formatter.formatShortFileSize(getContext(),id.length()), Toast.LENGTH_SHORT).show();
        Log.d("id file size",Formatter.formatShortFileSize(getContext(),id.length()));
        Log.d("id bitmap size",Formatter.formatShortFileSize(getContext(),bitmap.getByteCount()));

    }


}