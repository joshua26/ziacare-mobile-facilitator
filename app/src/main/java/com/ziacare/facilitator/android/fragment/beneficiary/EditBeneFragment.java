package com.ziacare.facilitator.android.fragment.beneficiary;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import com.chivorn.smartmaterialspinner.SmartMaterialSpinner;
import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.BeneficiaryActivity;
import com.ziacare.facilitator.android.fragment.DefaultFragment;
import com.ziacare.facilitator.data.model.api.PSGCModel;
import com.ziacare.facilitator.data.model.api.TransactionHistoryModel;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.request.Beneficiary;
import com.ziacare.facilitator.request.PSGC;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.transformer.CollectionTransformer;
import com.ziacare.facilitator.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class EditBeneFragment extends BaseFragment {
    public static final String TAG = EditBeneFragment.class.getName().toString();

    public static EditBeneFragment newInstance(TransactionHistoryModel model) {
        EditBeneFragment fragment = new EditBeneFragment();
        fragment.model = model;
        return fragment;
    }

    BeneficiaryActivity activity;
    TransactionHistoryModel model;

    @BindView(R.id.userIDTXT) TextView userIDTXT;
    @BindView(R.id.firstNameTXT) TextView firstNameTXT;
    @BindView(R.id.lastNameTXT) TextView lastNameTXT;
    @BindView(R.id.middleNameTXT) TextView middleNameTXT;
    @BindView(R.id.birthdateTXT) TextView birthdateTXT;
    @BindView(R.id.genderTXT) TextView genderTXT;
    @BindView(R.id.provinceTXT) TextView provinceTXT;
    @BindView(R.id.cityTXT) TextView cityTXT;
    @BindView(R.id.brgyET) EditText brgyET;
    @BindView(R.id.streetET) EditText streetET;
    @BindView(R.id.zipcodeET) EditText zipcodeET;
    @BindView(R.id.emailET) EditText emailET;
    @BindView(R.id.phoneET) EditText phoneET;
    @BindView(R.id.tinET) EditText tinET;
    @BindView(R.id.sssET) EditText sssET;
    @BindView(R.id.phicET) EditText phicET;

    @BindView(R.id.provinceSpinner) SmartMaterialSpinner<String> provinceSpinner;
    @BindView(R.id.citySpinner) SmartMaterialSpinner<String> citySpinner;

    ArrayList<String> provinceName, provinceSKU, cityName, citySKU;
    String pSKU, cSKU;

    String province, city;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_edit_bene;
    }

    @Override
    public void onViewReady() {
        activity = (BeneficiaryActivity) getContext();
        setDetails(model);

        provinceName = new ArrayList<>();
        provinceSKU = new ArrayList<>();
        cityName = new ArrayList<>();
        citySKU = new ArrayList<>();

        //PSGC.getDefault().getProvince(activity);
        PSGC.getDefault().getMunicipalityBySKU(activity,model.province_code);

        provinceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                provinceSpinner.setHint(provinceSpinner.getSelectedItem());
                setpSKU(provinceSKU.get(position));
                PSGC.getDefault().getMunicipality(activity, position + 1);
                cityName.clear();
                citySKU.clear();
                province = provinceSpinner.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                citySpinner.setHint(citySpinner.getSelectedItem());
                setcSKU(citySKU.get(position));
                city = citySpinner.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        activity.setTitle("Edit Beneficiary Details");
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void setDetails(TransactionHistoryModel model) {
        userIDTXT.setText(model.qrcode);
        firstNameTXT.setText(model.firstname);
        lastNameTXT.setText(model.lastname);
        middleNameTXT.setText(model.middlename);
        birthdateTXT.setText(model.birthdate);
        genderTXT.setText(model.gender);
        provinceTXT.setText(model.province);
        cityTXT.setText(model.city);
        brgyET.setText(model.brgy);
        streetET.setText(model.streetName);
        zipcodeET.setText(model.zipcode);
        emailET.setText(model.email);
        phoneET.setText(model.phoneNumber);
        tinET.setText(model.tinNo);
        sssET.setText(model.sssNo);
        phicET.setText(model.phicNo);
    }

    @OnClick(R.id.saveBTN)
    void onSaveBTN(){
        Beneficiary.getDefault().editRecord(activity,
                model.qrcode,
                getcSKU(),
                city,
                "",
                brgyET.getText().toString(),
                streetET.getText().toString(),
                zipcodeET.getText().toString(),
                phoneET.getText().toString(),
                emailET.getText().toString(),
                tinET.getText().toString(),
                sssET.getText().toString(),
                phicET.getText().toString());
    }

    @Subscribe
    public void onResponse(PSGC.ProvinceResponse response) {
        CollectionTransformer<PSGCModel> collectionTransformer = response.getData(CollectionTransformer.class);
        if (collectionTransformer.status) {
            if (provinceName.isEmpty() && provinceSKU.isEmpty()) {
                for (int i = 0; i < collectionTransformer.data.size(); i++) {
                    provinceName.add(collectionTransformer.data.get(i).name);
                    provinceSKU.add(collectionTransformer.data.get(i).sku);
                   /* if(province.equalsIgnoreCase(collectionTransformer.data.get(i).name)){
                        provinceSpinner.setHint(collectionTransformer.data.get(i).name);
                    }*/
                }

                provinceSpinner.setItem(provinceName);
            }
        }
    }

    @Subscribe
    public void onResponse(PSGC.MunicipalityResponse response) {
        CollectionTransformer<PSGCModel> collectionTransformer = response.getData(CollectionTransformer.class);
        if (collectionTransformer.status) {
            if (cityName.isEmpty() && citySKU.isEmpty()) {
                for (int i = 0; i < collectionTransformer.data.size(); i++) {
                    cityName.add(collectionTransformer.data.get(i).name);
                    citySKU.add(collectionTransformer.data.get(i).sku);
                    /*if(city.equalsIgnoreCase(collectionTransformer.data.get(i).name)){
                        citySpinner.setHint(collectionTransformer.data.get(i).name);
                    }*/
                }
                citySpinner.setItem(cityName);
            }
        }
    }

    @Subscribe
    public void onResponse(Beneficiary.EditRecordResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
            activity.onBackPressed();
        }else{
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.FAILED);
            ErrorResponseManger.first(brgyET,baseTransformer.error.brgy_name);
            ErrorResponseManger.first(streetET,baseTransformer.error.street_name);
            ErrorResponseManger.first(zipcodeET,baseTransformer.error.zipcode);
            ErrorResponseManger.first(phoneET,baseTransformer.error.phone_number);
            ErrorResponseManger.first(emailET,baseTransformer.error.email);
            ErrorResponseManger.first(tinET,baseTransformer.error.tin_no);
            ErrorResponseManger.first(sssET,baseTransformer.error.sss_no);
            ErrorResponseManger.first(phicET,baseTransformer.error.phic_no);
        }
    }

    public String getpSKU() {
        return pSKU;
    }

    public void setpSKU(String pSKU) {
        this.pSKU = pSKU;
    }

    public String getcSKU() {
        return cSKU;
    }

    public void setcSKU(String cSKU) {
        this.cSKU = cSKU;
    }

    @OnClick(R.id.provinceTXT)
    void onProvinceTXT(){
       /* provinceTXT.setVisibility(View.GONE);
        provinceSpinner.setVisibility(View.VISIBLE);*/
    }

    @OnClick(R.id.cityTXT)
    void onCityTXT(){
        cityTXT.setVisibility(View.GONE);
        citySpinner.setVisibility(View.VISIBLE);
    }

}
