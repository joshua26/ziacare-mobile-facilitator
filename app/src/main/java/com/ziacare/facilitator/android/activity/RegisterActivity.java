package com.ziacare.facilitator.android.activity;

import android.view.View;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.fragment.main.TransactionFragment;
import com.ziacare.facilitator.android.fragment.register.ForgotPasswordFragment;
import com.ziacare.facilitator.android.fragment.register.OTPFragment;
import com.ziacare.facilitator.android.fragment.register.ResetPasswordFragment;
import com.ziacare.facilitator.android.fragment.register.SignUpFacilitatorFragment;
import com.ziacare.facilitator.android.fragment.register.SignUpStep1Fragment;
import com.ziacare.facilitator.android.fragment.register.SignUpStep2Fragment;
import com.ziacare.facilitator.android.fragment.register.SignUpStep3Fragment;
import com.ziacare.facilitator.android.route.RouteActivity;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class RegisterActivity extends RouteActivity {
    public static final String TAG = RegisterActivity.class.getName().toString();

    @BindView(R.id.titleHeader)   View titleHeader;

    String dob;
    String fname;
    String mname;
    String lname;
    String street;
    String zipcode;
    String phone;
    String user;
    String email;
    String pass;
    String confirmpass;
    String tin;
    String sss;
    String phic;
    String gender;
    String userType;
    String brgy;
    String brgySku;
    String city;
    String citySku;
    String province;
    String provinceSku;
    int provincePosition, cityPosition;
    ArrayList<String> provinceName,provinceSKU, cityName, citySKU;

    boolean counterFlag = true;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_register;
    }

    @Override
    public void onViewReady() {

        provinceName = new ArrayList<>();
        provinceSKU = new ArrayList<>();
        cityName = new ArrayList<>();
        citySKU = new ArrayList<>();

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "signup":
                //openSignUpFacilitatorFragment();
                openStep1Fragment();
                break;
            case "forgot":
                openForgotPassword();
                break;
            case "transact":
                openTransactionFragment();
                break;
            default:
                break;
        }
    }

    public void openSignUpFacilitatorFragment(){ switchFragment(SignUpFacilitatorFragment.newInstance(),false); }
    public void openForgotPassword(){ switchFragment(ForgotPasswordFragment.newInstance()); }
    public void openTransactionFragment(){switchFragment(TransactionFragment.newInstance());}

    public void openStep1Fragment(){switchFragment(SignUpStep1Fragment.newInstance());}
    public void openStep2Fragment(){switchFragment(SignUpStep2Fragment.newInstance());}
    public void openStep3Fragment(){switchFragment(SignUpStep3Fragment.newInstance());}
    public void openOTPFragment(){switchFragment(OTPFragment.newInstance());}

    public void openResetPasswordFragment(String phone){switchFragment(ResetPasswordFragment.newInstance(phone));}

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getConfirmpass() {
        return confirmpass;
    }

    public void setConfirmpass(String confirmpass) {
        this.confirmpass = confirmpass;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    public String getSss() {
        return sss;
    }

    public void setSss(String sss) {
        this.sss = sss;
    }

    public String getPhic() {
        return phic;
    }

    public void setPhic(String phic) {
        this.phic = phic;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getBrgy() {
        return brgy;
    }

    public void setBrgy(String brgy) {
        this.brgy = brgy;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBrgySku() {
        return brgySku;
    }

    public void setBrgySku(String brgySku) {
        this.brgySku = brgySku;
    }

    public String getCitySku() {
        return citySku;
    }

    public void setCitySku(String citySku) {
        this.citySku = citySku;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getProvinceSku() {
        return provinceSku;
    }

    public void setProvinceSku(String provinceSku) {
        this.provinceSku = provinceSku;
    }

    public boolean isCounterFlag() {
        return counterFlag;
    }

    public void setCounterFlag(boolean counterFlag) {
        this.counterFlag = counterFlag;
    }

    public int getProvincePosition() {
        return provincePosition;
    }

    public void setProvincePosition(int provincePosition) {
        this.provincePosition = provincePosition;
    }

    public int getCityPosition() {
        return cityPosition;
    }

    public void setCityPosition(int cityPosition) {
        this.cityPosition = cityPosition;
    }

    public ArrayList<String> getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(ArrayList<String> provinceName) {
        this.provinceName = provinceName;
    }

    public ArrayList<String> getProvinceSKU() {
        return provinceSKU;
    }

    public void setProvinceSKU(ArrayList<String> provinceSKU) {
        this.provinceSKU = provinceSKU;
    }

    public ArrayList<String> getCityName() {
        return cityName;
    }

    public void setCityName(ArrayList<String> cityName) {
        this.cityName = cityName;
    }

    public ArrayList<String> getCitySKU() {
        return citySKU;
    }

    public void setCitySKU(ArrayList<String> citySKU) {
        this.citySKU = citySKU;
    }

    @Override
    public void onBackPressed() {
        if(isCounterFlag()){
            super.onBackPressed();
        }
        //super.onBackPressed();
    }

}
