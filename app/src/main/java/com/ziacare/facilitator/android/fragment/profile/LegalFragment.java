package com.ziacare.facilitator.android.fragment.profile;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.ProfileActivity;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.widget.WrapContentWebView;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class LegalFragment extends BaseFragment {
    public static final String TAG = LegalFragment.class.getName().toString();

    private ProfileActivity activity;

    @BindView(R.id.legalTXT)               WrapContentWebView legalTXT;

    public static LegalFragment newInstance() {
        LegalFragment fragment = new LegalFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_legal;
    }

    @Override
    public void onViewReady() {
        activity = (ProfileActivity) getContext();
        activity.setSettingsTitle("Legal Information");
        legalTXT.loadDataWithBaseURL(null, getString(R.string.legal), "text/html", "utf-8", null);

    }

    @Override
    public void onStart() {
        super.onStart();
//        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
//        EventBus.getDefault().unregister(this);
        super.onStop();
    }


}
