package com.ziacare.facilitator.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ziacare.facilitator.R;
import com.ziacare.facilitator.data.model.api.TransactionHistoryModel;
import com.ziacare.facilitator.vendor.android.base.BaseRecylerViewAdapter;
import com.ziacare.facilitator.vendor.android.java.Log;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class TransactionRecyclerViewAdapter extends BaseRecylerViewAdapter<TransactionRecyclerViewAdapter.ViewHolder, TransactionHistoryModel> implements Filterable {

    private ClickListener clickListener;
    List<TransactionHistoryModel> reportsList;
    List<TransactionHistoryModel> filteredReportList;

    public TransactionRecyclerViewAdapter(Context context, List<TransactionHistoryModel> reportsList) {
        super(context);
        this.reportsList = reportsList;
        this.filteredReportList = reportsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_transac));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(filteredReportList.get(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.adapterCON.setOnClickListener(this);

        try {
            Glide.with(getContext())
                    .load(filteredReportList.get(position).avatar.thumbPath)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.user_ic)
                            .error(R.drawable.user_ic))
                    .into(holder.imageView);

            holder.nameTXT.setText(filteredReportList.get(position).firstname + " " + filteredReportList.get(position).lastname);

            String qrcode = filteredReportList.get(position).qrcode;
            String address = filteredReportList.get(position).address;
          //  String covid_status = filteredReportList.get(position).beneficiary.covid_status;
           // String vaccinated = filteredReportList.get(position).beneficiary.is_vaccinated;

            if (qrcode == null) {
                qrcode = "Not Verified";
            }
            if (address == null) {
                address = filteredReportList.get(position).streetName + " " + filteredReportList.get(position).brgy + " " + filteredReportList.get(position).city;
            }
            holder.refNumTXT.setText(qrcode);
            holder.addressTXT.setText(address);
            holder.kycTXT.setText("KYC Verified: "+ filteredReportList.get(position).kyc_status);
            if(filteredReportList.get(position).has_physical_card){
                holder.cardTXT.setVisibility(View.VISIBLE);
                holder.cardTXT.setText("Has physical card");
            }else{
                holder.cardTXT.setVisibility(View.GONE);
            }


        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        try {
            return filteredReportList.size();
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String searchString = charSequence.toString().toLowerCase();

                if (searchString.isEmpty()) {

                    filteredReportList = reportsList;

                } else {
                    List<TransactionHistoryModel> tempfilteredReportList = new ArrayList<>();

                    for (TransactionHistoryModel model : reportsList) {
                        try {
                            if (model.firstname.toLowerCase().contains(searchString) || model.lastname.toLowerCase().contains(searchString) ||
                                    model.qrcode.toLowerCase().contains(searchString)) {
                                tempfilteredReportList.add(model);
                                Log.d("searched", tempfilteredReportList);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    filteredReportList = tempfilteredReportList;

                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredReportList;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredReportList = (ArrayList<TransactionHistoryModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder {

        @BindView(R.id.adapterCON)
        View adapterCON;
        @BindView(R.id.botNavView)
        View botNavView;
        @BindView(R.id.nameTXT)
        TextView nameTXT;
        @BindView(R.id.refNumTXT)
        TextView refNumTXT;
        @BindView(R.id.addressTXT)
        TextView addressTXT;
        @BindView(R.id.kycTXT)                     TextView kycTXT;
        @BindView(R.id.cardTXT)                     TextView cardTXT;
        @BindView(R.id.imageIV)
        CircleImageView imageView;

        public ViewHolder(View view) {
            super(view);
        }

        public TransactionHistoryModel getItem() {
            return (TransactionHistoryModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.adapterCON:
                if (clickListener != null) {
                    clickListener.onItemClick((TransactionHistoryModel) v.getTag());
                }
                break;
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(TransactionHistoryModel model);
    }
} 
