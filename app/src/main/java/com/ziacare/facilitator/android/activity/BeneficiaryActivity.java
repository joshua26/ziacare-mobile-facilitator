package com.ziacare.facilitator.android.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.fragment.DefaultFragment;
import com.ziacare.facilitator.android.fragment.beneficiary.BenePreviewFragment;
import com.ziacare.facilitator.android.fragment.beneficiary.EditBeneFragment;
import com.ziacare.facilitator.android.fragment.beneficiary.GeneratedNewPasswordFragment;
import com.ziacare.facilitator.android.fragment.beneficiary.ProcessKYCFragment;
import com.ziacare.facilitator.android.fragment.beneficiary.ViewBeneFragment;
import com.ziacare.facilitator.android.route.RouteActivity;
import com.ziacare.facilitator.data.model.api.TransactionHistoryModel;

public class BeneficiaryActivity extends RouteActivity {
    public static final String TAG = BeneficiaryActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_beneficiary;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onViewReady() {
        showBackButton(true);
        getActivityBackBTN().setImageTintList(getResources().getColorStateList(R.color.colorPrimary));
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "preview":
                openBenePreviewFragment(getFragmentBundle().getParcelable("model"));
                break;
            case "view":
                openViewBeneFragment(getFragmentBundle().getParcelable("model"));
                break;
            default:
                openProcessKYCFragment();
                break;
        }
    }

    public void openProcessKYCFragment(){
        switchFragment(ProcessKYCFragment.newInstance(),false);
    }

    public void openBenePreviewFragment(TransactionHistoryModel model){switchFragment(BenePreviewFragment.newInstance(model));}
    public void openViewBeneFragment(TransactionHistoryModel model){switchFragment(ViewBeneFragment.newInstance(model));}
    public void openGenerateNewPasswordFragment(String password){switchFragment(GeneratedNewPasswordFragment.newInstance(password));}
    public void openEditBeneFragment(TransactionHistoryModel model){switchFragment(EditBeneFragment.newInstance(model));}
}
