package com.ziacare.facilitator.android.activity;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.fragment.DefaultFragment;
import com.ziacare.facilitator.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class DefaultActivity extends RouteActivity {
    public static final String TAG = DefaultActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_default;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "login":

                break;
            default:
                openDefaultFragment();
                break;
        }
    }

    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }
}
