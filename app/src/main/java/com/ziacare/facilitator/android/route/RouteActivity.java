package com.ziacare.facilitator.android.route;

import android.content.Intent;
import android.os.Bundle;

import com.ziacare.facilitator.android.activity.BeneficiaryActivity;
import com.ziacare.facilitator.android.activity.KYCActivity;
import com.ziacare.facilitator.android.activity.LandingActivity;
import com.ziacare.facilitator.android.activity.MainActivity;
import com.ziacare.facilitator.android.activity.ProfileActivity;
import com.ziacare.facilitator.android.activity.RegisterActivity;
import com.ziacare.facilitator.android.activity.ScanActivity;
import com.ziacare.facilitator.data.model.api.TransactionHistoryModel;
import com.ziacare.facilitator.vendor.android.base.BaseActivity;
import com.ziacare.facilitator.vendor.android.base.RouteManager;

/**
 * Created by Labyalo on 2/6/2017.
 */

public class RouteActivity extends BaseActivity {

    public void startMainActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(MainActivity.class)
                .addActivityTag("main")
                .addFragmentTag(fragmentTAG)
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                finish();
    }

    public void startRegisterActivity(String fragmentTAG){
        Bundle bundle = new Bundle();
        //bundle.putString("scanCode",scanCode);
        RouteManager.Route.with(this)
                .addActivityClass(RegisterActivity.class)
                .addActivityTag("register")
                .addFragmentTag(fragmentTAG)
               // .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startLandingActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(LandingActivity.class)
                .addActivityTag("landing")
                .addFragmentTag(fragmentTAG)
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
    }

    public void startProfileActivity(String fragmentTAG){
        Bundle bundle = new Bundle();
        RouteManager.Route.with(this)
                .addActivityClass(ProfileActivity.class)
                .addActivityTag("cash_in")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startScanActivity(String fragmentTAG, String scanCode){
        Bundle bundle = new Bundle();
        bundle.putString("scanCode",scanCode);
        RouteManager.Route.with(this)
                .addActivityClass(ScanActivity.class)
                .addActivityTag("scanActivity")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startKYCActivity(String fragmentTAG, String user_code,boolean isAddBene){
        Bundle bundle = new Bundle();
        bundle.putString("user_code",user_code);
        bundle.putBoolean("isAddBene",isAddBene);
        RouteManager.Route.with(this)
                .addActivityClass(KYCActivity.class)
                .addActivityTag("kyc")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startBeneficiaryActivity(String fragmentTAG, TransactionHistoryModel model){
        Bundle bundle = new Bundle();
        bundle.putParcelable("model",model);
        RouteManager.Route.with(this)
                .addActivityClass(BeneficiaryActivity.class)
                .addActivityTag("bene")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }
}
