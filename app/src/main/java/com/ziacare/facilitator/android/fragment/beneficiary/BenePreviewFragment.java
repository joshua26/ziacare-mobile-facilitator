package com.ziacare.facilitator.android.fragment.beneficiary;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ziacare.facilitator.R;
import com.ziacare.facilitator.android.activity.BeneficiaryActivity;
import com.ziacare.facilitator.android.fragment.DefaultFragment;
import com.ziacare.facilitator.data.model.api.TransactionHistoryModel;
import com.ziacare.facilitator.request.Auth;
import com.ziacare.facilitator.request.Beneficiary;
import com.ziacare.facilitator.vendor.android.base.BaseFragment;
import com.ziacare.facilitator.vendor.android.java.RoundedLinearLayout;
import com.ziacare.facilitator.vendor.android.java.ToastMessage;
import com.ziacare.facilitator.vendor.server.transformer.BaseTransformer;
import com.ziacare.facilitator.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

public class BenePreviewFragment extends BaseFragment {
    public static final String TAG = BenePreviewFragment.class.getName().toString();

    public static BenePreviewFragment newInstance(TransactionHistoryModel model) {
        BenePreviewFragment fragment = new BenePreviewFragment();
        fragment.model = model;
        return fragment;
    }

    BeneficiaryActivity activity;

    TransactionHistoryModel model;

    @BindView(R.id.userIDTXT) TextView userIDTXT;
    @BindView(R.id.firstNameTXT) TextView firstNameTXT;
    @BindView(R.id.lastNameTXT) TextView lastNameTXT;
    @BindView(R.id.middleNameTXT) TextView middleNameTXT;
    @BindView(R.id.birthdateTXT) TextView birthdateTXT;
    @BindView(R.id.selfieStatusTXT) TextView selfieStatusTXT;
    @BindView(R.id.idStatusTXT) TextView idStatusTXT;
    @BindView(R.id.hasPhysicalCardTXT) TextView hasPhysicalCardTXT;

    @BindView(R.id.firstLine) View firstLine;
    @BindView(R.id.secondLine) View secondLine;
    @BindView(R.id.thirdLine) View thirdLine;

    @BindView(R.id.uploadIDBTN) RoundedLinearLayout uploadIDBTN;
    @BindView(R.id.uploadSelfieBTN) RoundedLinearLayout uploadSelfieBTN;

    @BindView(R.id.kycLayout) LinearLayout kycLayout;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_bene_preview;
    }

    @Override
    public void onViewReady() {
        activity = (BeneficiaryActivity) getContext();
        setDetails(model);

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        activity.setTitle("Beneficiary Account");
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        Beneficiary.getDefault().scanRefNo(activity,model.qrcode);
    }

    @Subscribe
    public void onResponse(Beneficiary.ScanUserCodeResponse response){
        SingleTransformer<TransactionHistoryModel> singleTransformer = response.getData(SingleTransformer.class);
        if(singleTransformer.status){
            setDetails(singleTransformer.data);
        }
        else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(Beneficiary.GenerateNewPasswordResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
            activity.openGenerateNewPasswordFragment(baseTransformer.new_password);
        }else{
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    private void setDetails(TransactionHistoryModel model){
        userIDTXT.setText(model.qrcode);
        firstNameTXT.setText(model.firstname);
        lastNameTXT.setText(model.lastname);
        middleNameTXT.setText(model.middlename);
        birthdateTXT.setText(model.birthdate);
        selfieStatusTXT.setText(model.selfie_status);
        idStatusTXT.setText(model.id_status);

        if (model.has_physical_card) {
            hasPhysicalCardTXT.setText("Yes");
        } else {
            hasPhysicalCardTXT.setText("No");
        }

        if(model.selfie_status.equals("verified")){
            firstLine.setVisibility(View.GONE);
            uploadSelfieBTN.setVisibility(View.GONE);
        }else{
            firstLine.setVisibility(View.VISIBLE);
            uploadSelfieBTN.setVisibility(View.VISIBLE);
        }

        if(model.id_status.equals("verified")){
            thirdLine.setVisibility(View.GONE);
            uploadIDBTN.setVisibility(View.GONE);
        }else{
            thirdLine.setVisibility(View.VISIBLE);
            uploadIDBTN.setVisibility(View.VISIBLE);
        }

        if(model.kyc_status.equals("verified")){
            kycLayout.setVisibility(View.GONE);
        }else{
            kycLayout.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.viewProfileBTN)
    void onViewProfileBTN(){
        activity.openViewBeneFragment(model);
    }

    @OnClick(R.id.uploadIDBTN)
    void onUploadIDBTN(){
        activity.startKYCActivity("id",model.qrcode,false);
    }

    @OnClick(R.id.uploadSelfieBTN)
    void onUploadSelfieBTN(){
        activity.startKYCActivity("selfie",model.qrcode,false);
    }

    @OnClick(R.id.generatePassBTN)
    void onGenerate(){
        Beneficiary.getDefault().generateNewPassword(activity,model.qrcode);
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }
}
