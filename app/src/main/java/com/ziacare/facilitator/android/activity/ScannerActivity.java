package com.ziacare.facilitator.android.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;
import com.ziacare.facilitator.R;
import com.ziacare.facilitator.vendor.android.java.facedetect.FaceCenterCircleView.FaceCenterCrop;
import com.ziacare.facilitator.vendor.android.java.facedetect.FaceDetectionUtil.FaceDetectionProcessor;
import com.ziacare.facilitator.vendor.android.java.facedetect.FaceDetectionUtil.FaceDetectionResultListener;
import com.ziacare.facilitator.vendor.android.java.facedetect.FaceDetectionUtil.common.CameraSource;
import com.ziacare.facilitator.vendor.android.java.facedetect.FaceDetectionUtil.common.CameraSourcePreview;
import com.ziacare.facilitator.vendor.android.java.facedetect.FaceDetectionUtil.common.FrameMetadata;
import com.ziacare.facilitator.vendor.android.java.facedetect.FaceDetectionUtil.common.GraphicOverlay;
import com.ziacare.facilitator.vendor.android.java.facedetect.Utils.Imageutils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ziacare.facilitator.vendor.android.java.facedetect.Utils.FaceDetectionScanner.Constants.KEY_CAMERA_PERMISSION_GRANTED;
import static com.ziacare.facilitator.vendor.android.java.facedetect.Utils.FaceDetectionScanner.Constants.PERMISSION_REQUEST_CAMERA;

public class ScannerActivity extends AppCompatActivity {

    String TAG = "ScannerActivity";

    @BindView(R.id.barcodeOverlay)
    GraphicOverlay barcodeOverlay;
    @BindView(R.id.preview)
    CameraSourcePreview preview;
    @BindView(R.id.btnCapture)
    Button btnCapture;

    @BindView(R.id.text)
    TextView text;

    private CameraSource mCameraSource = null;

    FaceDetectionProcessor faceDetectionProcessor;
    FaceDetectionResultListener faceDetectionResultListener = null;

    public static Bitmap bmpCapturedImage;
    List<FirebaseVisionFace> capturedFaces;

    FaceCenterCrop faceCenterCrop;
    FaceCenterCrop.FaceCenterCropListener faceCenterCropListener;

    boolean hasFrontCam = false;
    int cameraFacing = CameraSource.CAMERA_FACING_FRONT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getWindow() != null) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            Log.e(TAG, "Barcode scanner could not go into fullscreen mode!");
        }


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        ButterKnife.bind(this);

        /*if(getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)){
            hasFrontCam = true;
            cameraFacing = CameraSource.CAMERA_FACING_FRONT;
        }else{
            hasFrontCam = false;
            cameraFacing = CameraSource.CAMERA_FACING_BACK;
        }*/

        faceCenterCrop = new FaceCenterCrop(this, 100, 100, 1);


        if (preview != null)
            if (preview.isPermissionGranted(true, mMessageSender))
                new Thread(mMessageSender).start();
    }

    private void createCameraSource() {

        // To initialise the detector

        FirebaseVisionFaceDetectorOptions options =
                new FirebaseVisionFaceDetectorOptions.Builder()
                        .setClassificationMode(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
                        .enableTracking()
                        .build();

        FirebaseVisionFaceDetector detector = FirebaseVision.getInstance().getVisionFaceDetector(options);

        // To connect the camera resource with the detector

        mCameraSource = new CameraSource(this, barcodeOverlay);
        mCameraSource.setFacing(CameraSource.CAMERA_FACING_BACK);


        // FaceContourDetectorProcessor faceDetectionProcessor = new FaceContourDetectorProcessor(detector);

        faceDetectionProcessor = new FaceDetectionProcessor(detector);
        faceDetectionProcessor.setFaceDetectionResultListener(getFaceDetectionListener());

        mCameraSource.setMachineLearningFrameProcessor(faceDetectionProcessor);

        startCameraSource();
    }

    boolean isSmiling = false;
    boolean isLookingLeft = false;
    boolean isLookingRight = false;

    private FaceDetectionResultListener getFaceDetectionListener() {
        if (faceDetectionResultListener == null)
            faceDetectionResultListener = new FaceDetectionResultListener() {
                @Override
                public void onSuccess(@Nullable Bitmap originalCameraImage, @NonNull List<FirebaseVisionFace> faces, @NonNull FrameMetadata frameMetadata, @NonNull GraphicOverlay graphicOverlay) {
                    boolean isEnable;
                    isEnable = faces.size() > 0;

                    Log.e("num", String.valueOf(num));

                    for (FirebaseVisionFace face : faces) {
                        switch (num) {
                            case 1:
                                text.setText("Smile");
                                if (face.getSmilingProbability() > 0.5) {
                                    isSmiling = true;
                                } else {
                                    isSmiling = false;
                                }
                                break;
                            case 2:
                                text.setText("Look Left");
                                if (face.getHeadEulerAngleY() > 12) {
                                    isLookingRight = true;
                                } else {
                                    isLookingRight = false;
                                }

                                break;
                            case 3:
                                text.setText("Look Right");
                                if (face.getHeadEulerAngleY() < -12) {
                                    isLookingLeft = true;
                                } else {
                                    isLookingLeft = false;
                                }


                                break;
                            default:
                                isSmiling = false;
                                isLookingLeft = false;
                                isLookingRight = false;
                                break;
                        }

                        // To get the results

                        Log.d(TAG, "Face bounds : " + face.getBoundingBox());

                        // To get this, we have to set the ClassificationMode attribute as ALL_CLASSIFICATIONS

                        Log.d(TAG, "Left eye open probability : " + face.getLeftEyeOpenProbability());
                        Log.d(TAG, "Right eye open probability : " + face.getRightEyeOpenProbability());
                        Log.d(TAG, "Smiling probability : " + face.getSmilingProbability());

                        // To get this, we have to enableTracking

                        Log.d(TAG, "Face ID : " + face.getTrackingId());

                    }

                    runOnUiThread(() -> {
                        Log.d(TAG, "button enable true ");
                        bmpCapturedImage = originalCameraImage;
                        capturedFaces = faces;
                        if (num == 1) {
                            btnCapture.setEnabled(isConditionMet(isSmiling));
                        } else if (num == 2) {
                            btnCapture.setEnabled(isConditionMet(isLookingRight));
                           /* if (isLookingRight) {
                                btnCapture.setEnabled(isEnable);
                            } else {
                                btnCapture.setEnabled(false);
                            }*/
                        } else if (num == 3) {
                            btnCapture.setEnabled(isConditionMet(isLookingLeft));
                           /* if (isLookingLeft) {
                                btnCapture.setEnabled(isEnable);
                            } else {
                                btnCapture.setEnabled(false);
                            }*/
                        } else {
                            btnCapture.setEnabled(isEnable);
                        }
                    });
                }

                @Override
                public void onFailure(@NonNull Exception e) {

                }
            };

        return faceDetectionResultListener;
    }

    private void startCameraSource() {

        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());

        Log.d(TAG, "startCameraSource: " + code);

//        if (code != ConnectionResult.SUCCESS) {
//            Dialog dlg =
//                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, PERMISSION_REQUEST_CAMERA);
//            dlg.show();
//        }

        if (mCameraSource != null && preview != null && barcodeOverlay != null) {
            try {
                Log.d(TAG, "startCameraSource: ");
                preview.start(mCameraSource, barcodeOverlay);
            } catch (IOException e) {
                mCameraSource.setFacing(CameraSource.CAMERA_FACING_BACK);
                Log.d(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        } else
            Log.d(TAG, "startCameraSource: not started");

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        Log.d(TAG, "onRequestPermissionsResult: " + requestCode);
        preview.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * Restarts the camera.
     */
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
        startCameraSource();
    }

    /**
     * Stops the camera.
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (preview != null)
            preview.stop();
    }

    /**
     * Releases the resources associated with the camera source, the associated detector, and the
     * rest of the processing pipeline.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCameraSource != null) {
            mCameraSource.release();
        }
    }

    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            Log.d(TAG, "handleMessage: ");

            if (preview != null)
                createCameraSource();

        }
    };

    int num;
    private final Runnable mMessageSender = () -> {
        Log.d(TAG, "mMessageSender: ");
        Message msg = mHandler.obtainMessage();
        Bundle bundle = new Bundle();
        bundle.putBoolean(KEY_CAMERA_PERMISSION_GRANTED, false);
        num = getIntent().getExtras().getInt("num");
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    };


    private FaceCenterCrop.FaceCenterCropListener getFaceCropResult() {
        if (faceCenterCropListener == null)
            faceCenterCropListener = new FaceCenterCrop.FaceCenterCropListener() {
                @Override
                public void onTransform(Bitmap updatedBitmap) {


                    Log.d(TAG, "onTransform: ");

                    try {
                        File capturedFile = new File(getFilesDir(), "newImage.jpg");

                        Imageutils imageutils = new Imageutils(ScannerActivity.this);
                        imageutils.store_image(capturedFile, updatedBitmap);

                        Intent currentIntent = getIntent();
                        currentIntent.putExtra("image", capturedFile.getAbsolutePath());
                        setResult(RESULT_OK, currentIntent);
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure() {
                    Toast.makeText(ScannerActivity.this, "No face found", Toast.LENGTH_SHORT).show();
                }
            };

        return faceCenterCropListener;
    }

    @OnClick(R.id.btnCapture)
    public void onViewClicked() {
        Log.d(TAG, "onViewClicked: ");
        if (faceCenterCrop != null)
            faceCenterCrop.transform(bmpCapturedImage, faceCenterCrop.getCenterPoint(capturedFaces), getFaceCropResult());
    }

    private boolean isConditionMet(boolean b) {
        if (b)
            return true;
        else
            return false;
    }

}