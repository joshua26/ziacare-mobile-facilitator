package com.ziacare.facilitator.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.ziacare.facilitator.vendor.android.base.AndroidModel;

public class PSGCModel extends AndroidModel {

    @SerializedName("province_id")
    public int province_id;

    @SerializedName("name")
    public String name;

    @SerializedName("sku")
    public String sku;

    @SerializedName("citymun_id")
    public int citymun_id;

    @SerializedName("is_capital")
    public int is_capital;

    @SerializedName("type")
    public String type;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public PSGCModel convertFromJson(String json) {
        return convertFromJson(json, PSGCModel.class);
    }
}
