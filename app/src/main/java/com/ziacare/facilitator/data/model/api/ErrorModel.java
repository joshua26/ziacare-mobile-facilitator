package com.ziacare.facilitator.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.ziacare.facilitator.vendor.android.base.AndroidModel;

import java.util.List;


public class ErrorModel extends AndroidModel {

    @SerializedName("email")
    public List<String> email;
    @SerializedName("phone_number")
    public List<String> phone_number;
    @SerializedName("birthdate")
    public List<String> birthdate;
    @SerializedName("current_password")
    public List<String> current_password;
    @SerializedName("password_confirmation")
    public List<String> password_confirmation;
    @SerializedName("username")
    public List<String> username;
    @SerializedName("firstname")
    public List<String> firstname;
    @SerializedName("city")
    public List<String> city;
    @SerializedName("brgy")
    public List<String> brgy;
    @SerializedName("middlename")
    public List<String> middlename;
    @SerializedName("lastname")
    public List<String> lastname;
    @SerializedName("bio")
    public List<String> bio;
    @SerializedName("emergency_contact")
    public List<String> emergency_contact;
    @SerializedName("qty")
    public List<String> qty;
    @SerializedName("tin_no")
    public List<String> tin_no;
    @SerializedName("sss_no")
    public List<String> sss_no;
    @SerializedName("phic_no")
    public List<String> phic_no;
    @SerializedName("password")
    public List<String> password;
    @SerializedName("old_password")
    public List<String> old_password;
    @SerializedName("name")
    public List<String> name;
    @SerializedName("contact_number")
    public List<String> contact_number;
    @SerializedName("company_name")
    public List<String> company_name;
    @SerializedName("description")
    public List<String> description;
    @SerializedName("file")
    public List<String> file;
    @SerializedName("video_file")
    public List<String> video_file;
    @SerializedName("question")
    public List<String> question;
    @SerializedName("answer_a")
    public List<String> answer_a;
    @SerializedName("answer_b")
    public List<String> answer_b;
    @SerializedName("answer_c")
    public List<String> answer_c;
    @SerializedName("answer_d")
    public List<String> answer_d;
    @SerializedName("correct_answer")
    public List<String> correct_answer;
    @SerializedName("job_position")
    public List<String> job_position;
    @SerializedName("salary")
    public List<String> salary;
    @SerializedName("company_address")
    public List<String> company_address;
    @SerializedName("type")
    public List<String> type;
    @SerializedName("gender")
    public List<String> gender;
    @SerializedName("street_name")
    public List<String> street_name;
    @SerializedName("zipcode")
    public List<String> zipcode;
    @SerializedName("valid_id")
    public List<String> valid_id;
    @SerializedName("company")
    public List<String> company;
    @SerializedName("address")
    public List<String> address;
    @SerializedName("business_email")
    public List<String> business_email;
    @SerializedName("business_phone_number")
    public List<String> business_phone_number;
    @SerializedName("sec_dti")
    public List<String> sec_dti;
    @SerializedName("amount")
    public List<String> amount;
    @SerializedName("emergency_name")
    public List<String> emergency_name;
    @SerializedName("emergency_relationship")
    public List<String> emergency_relationship;
    @SerializedName("brgy_name")
    public List<String> brgy_name;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ErrorModel convertFromJson(String json) {
        return convertFromJson(json, ErrorModel.class);
    }

}
