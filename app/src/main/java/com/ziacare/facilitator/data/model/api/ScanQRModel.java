package com.ziacare.facilitator.data.model.api;

import com.ziacare.facilitator.vendor.android.base.AndroidModel;
import com.google.gson.annotations.SerializedName;

public class ScanQRModel extends AndroidModel {

    @SerializedName("user_id")
    public int user_id;

    @SerializedName("qrcode")
    public String qrcode;

    @SerializedName("organization_id")
    public String organization_id;

    @SerializedName("role")
    public String role;

    @SerializedName("user_type")
    public String user_type;

    @SerializedName("type")
    public String type;

    @SerializedName("code")
    public String code;

    @SerializedName("title")
    public String title;

    @SerializedName("firstname")
    public String firstname;

    @SerializedName("lastname")
    public String lastname;

    @SerializedName("middlename")
    public String middlename;

    @SerializedName("name")
    public String name;

    @SerializedName("username")
    public String username;

    @SerializedName("phone_number")
    public String phone_number;

    @SerializedName("tel_no")
    public String tel_no;

    @SerializedName("email")
    public String email;

    @SerializedName("recovery_email")
    public String recovery_email;

    @SerializedName("gender")
    public String gender;

    @SerializedName("birthdate")
    public String birthdate;

    @SerializedName("city")
    public String city;

    @SerializedName("city_code")
    public String city_code;

    @SerializedName("brgy")
    public String brgy;

    @SerializedName("brgy_code")
    public String brgy_code;

    @SerializedName("street_name")
    public String street_name;

    @SerializedName("zipcode")
    public String zipcode;

    @SerializedName("tin_no")
    public String tin_no;

    @SerializedName("sss_no")
    public String sss_no;

    @SerializedName("phic_no")
    public String phic_no;

    @SerializedName("company")
    public String company;

    @SerializedName("address")
    public String address;

    @SerializedName("business_email")
    public String business_email;

    @SerializedName("business_phone_number")
    public String business_phone_number;

    @SerializedName("sec_dti")
    public String sec_dti;

    @SerializedName("prc_license")
    public String prc_license;

    @SerializedName("doh")
    public String doh;

    @SerializedName("dfa")
    public String dfa;

    @SerializedName("donor_type")
    public String donor_type;

    @SerializedName("sub_type")
    public String sub_type;

    @SerializedName("emergency_name")
    public String emergency_name;

    @SerializedName("emergency_contact")
    public String emergency_contact;

    @SerializedName("emergency_relationship")
    public String emergency_relationship;

    @SerializedName("total_credit")
    public String total_credit;

    @SerializedName("total_voucher")
    public String total_voucher;

    @SerializedName("total_donated_amount")
    public String total_donated_amount;

    @SerializedName("current_balance")
    public String current_balance;

    @SerializedName("available_balance")
    public String available_balance;

    @SerializedName("date_created")
    public DateCreated dateCreated;

    @SerializedName("avatar")
    public Avatar avatar;

    @SerializedName("valid_id")
    public ValidId validId;

    @SerializedName("covid_status")
    public String covid_status;

    @SerializedName("is_vaccinated")
    public String is_vaccinated;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ScanQRModel convertFromJson(String json) {
        return convertFromJson(json, ScanQRModel.class);
    }

    public static class DateCreated {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class Avatar {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }

    public static class ValidId {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }
}
