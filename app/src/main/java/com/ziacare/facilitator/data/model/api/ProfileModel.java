package com.ziacare.facilitator.data.model.api;

import com.ziacare.facilitator.vendor.android.base.AndroidModel;
import com.google.gson.annotations.SerializedName;

public class ProfileModel  extends AndroidModel {

    @SerializedName("code")
    public String code;

    @SerializedName("qrcode")
    public String qrcode;

    @SerializedName("reference_id")
    public String reference_id;

    @SerializedName("firstname")
    public String firstname;

    @SerializedName("middlename")
    public String middlename;

    @SerializedName("lastname")
    public String lastname;

    @SerializedName("gender")
    public String gender;

    @SerializedName("job_title")
    public String jobTitle;

    @SerializedName("bday")
    public String birthday;

    @SerializedName("company_name")
    public String companyName;

    @SerializedName("address")
    public String address;

    @SerializedName("email")
    public String email;

    @SerializedName("contact_number")
    public String contactNumber;

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("updated_at")
    public String updated_at;

    @SerializedName("deleted_at")
    public String deleted_at;

    @SerializedName("name")
    public String name;

    @SerializedName("path")
    public String path;

    @SerializedName("directory")
    public String directory;

    @SerializedName("filename")
    public String filename;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ProfileModel convertFromJson(String json) {
        return convertFromJson(json, ProfileModel.class);
    }


}