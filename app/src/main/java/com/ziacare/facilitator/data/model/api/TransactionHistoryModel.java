package com.ziacare.facilitator.data.model.api;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.ziacare.facilitator.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class TransactionHistoryModel extends AndroidModel implements Parcelable {


    @SerializedName("qrcode")
    public String qrcode;

    @SerializedName("user_id")
    public int userId;

    @SerializedName("kyc_status")
    public String kyc_status;

    @SerializedName("code")
    public String code;

    @SerializedName("firstname")
    public String firstname;

    @SerializedName("lastname")
    public String lastname;

    @SerializedName("total_credit")
    public String totalCredit;

    @SerializedName("selfie_status")
    public String selfie_status;

    @SerializedName("id_status")
    public String id_status;

    @SerializedName("phone_number_verified")
    public boolean phone_number_verified;

    @SerializedName("account_status")
    public String account_status;

    @SerializedName("has_physical_card")
    public boolean has_physical_card;

    @SerializedName("organization_id")
    public int organization_id;

    @SerializedName("upcoming_events")
    public int upcoming_events;

    @SerializedName("type")
    public String type;

    @SerializedName("user_type")
    public String user_type;

    @SerializedName("role")
    public String role;

    @SerializedName("total_voucher")
    public String totalVoucher;

    @SerializedName("current_balance")
    public String currentBalance;

    @SerializedName("available_balance")
    public String availableBalance;

    @SerializedName("middlename")
    public String middlename;

    @SerializedName("name")
    public String name;

    @SerializedName("username")
    public String username;

    @SerializedName("is_pwd")
    public String is_pwd;

    @SerializedName("is_senior")
    public String is_senior;

    @SerializedName("phone_number")
    public String phoneNumber;

    @SerializedName("tel_no")
    public String tel_no;

    @SerializedName("emergency_name")
    public String emergencyName;


    @SerializedName("emergency_contact")
    public String emergencyContact;


    @SerializedName("emergency_relationship")
    public String emergencyRelationship;

    @SerializedName("email")
    public String email;

    @SerializedName("gender")
    public String gender;

    @SerializedName("birthdate")
    public String birthdate;

    @SerializedName("province")
    public String province;

    @SerializedName("province_code")
    public String province_code;

    @SerializedName("city")
    public String city;

    @SerializedName("city_code")
    public String cityCode;

    @SerializedName("brgy")
    public String brgy;

    @SerializedName("brgy_code")
    public String brgyCode;

    @SerializedName("street_name")
    public String streetName;

    @SerializedName("zipcode")
    public String zipcode;

    @SerializedName("tin_no")
    public String tinNo;

    @SerializedName("sss_no")
    public String sssNo;

    @SerializedName("phic_no")
    public String phicNo;

    @SerializedName("company")
    public String company;

    @SerializedName("address")
    public String address;

    @SerializedName("donor_type")
    public String donor_type;

    @SerializedName("business_email")
    public String businessEmail;

    @SerializedName("business_phone_number")
    public String businessPhoneNumber;

    @SerializedName("sec_dti")
    public String secDti;

    @SerializedName("prc_license")
    public String prcLicense;

    @SerializedName("doh")
    public String doh;

    @SerializedName("dfa")
    public String dfa;

    @SerializedName("date_created")
    public DateCreated dateCreated;

    @SerializedName("avatar")
    public Avatar avatar;

    @SerializedName("valid_id")
    public ValidID validID;

    protected TransactionHistoryModel(Parcel in) {
        qrcode = in.readString();
        userId = in.readInt();
        kyc_status = in.readString();
        code = in.readString();
        firstname = in.readString();
        lastname = in.readString();
        totalCredit = in.readString();
        selfie_status = in.readString();
        id_status = in.readString();
        phone_number_verified = in.readByte() != 0;
        account_status = in.readString();
        has_physical_card = in.readByte() != 0;
        organization_id = in.readInt();
        upcoming_events = in.readInt();
        type = in.readString();
        user_type = in.readString();
        role = in.readString();
        totalVoucher = in.readString();
        currentBalance = in.readString();
        availableBalance = in.readString();
        middlename = in.readString();
        name = in.readString();
        username = in.readString();
        is_pwd = in.readString();
        is_senior = in.readString();
        phoneNumber = in.readString();
        tel_no = in.readString();
        emergencyName = in.readString();
        emergencyContact = in.readString();
        emergencyRelationship = in.readString();
        email = in.readString();
        gender = in.readString();
        birthdate = in.readString();
        province = in.readString();
        province_code = in.readString();
        city = in.readString();
        cityCode = in.readString();
        brgy = in.readString();
        brgyCode = in.readString();
        streetName = in.readString();
        zipcode = in.readString();
        tinNo = in.readString();
        sssNo = in.readString();
        phicNo = in.readString();
        company = in.readString();
        address = in.readString();
        donor_type = in.readString();
        businessEmail = in.readString();
        businessPhoneNumber = in.readString();
        secDti = in.readString();
        prcLicense = in.readString();
        doh = in.readString();
        dfa = in.readString();
    }

    public static final Creator<TransactionHistoryModel> CREATOR = new Creator<TransactionHistoryModel>() {
        @Override
        public TransactionHistoryModel createFromParcel(Parcel in) {
            return new TransactionHistoryModel(in);
        }

        @Override
        public TransactionHistoryModel[] newArray(int size) {
            return new TransactionHistoryModel[size];
        }
    };

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public TransactionHistoryModel convertFromJson(String json) {
        return convertFromJson(json, TransactionHistoryModel.class);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(qrcode);
        dest.writeInt(userId);
        dest.writeString(kyc_status);
        dest.writeString(code);
        dest.writeString(firstname);
        dest.writeString(lastname);
        dest.writeString(totalCredit);
        dest.writeString(selfie_status);
        dest.writeString(id_status);
        dest.writeByte((byte) (phone_number_verified ? 1 : 0));
        dest.writeString(account_status);
        dest.writeByte((byte) (has_physical_card ? 1 : 0));
        dest.writeInt(organization_id);
        dest.writeInt(upcoming_events);
        dest.writeString(type);
        dest.writeString(user_type);
        dest.writeString(role);
        dest.writeString(totalVoucher);
        dest.writeString(currentBalance);
        dest.writeString(availableBalance);
        dest.writeString(middlename);
        dest.writeString(name);
        dest.writeString(username);
        dest.writeString(is_pwd);
        dest.writeString(is_senior);
        dest.writeString(phoneNumber);
        dest.writeString(tel_no);
        dest.writeString(emergencyName);
        dest.writeString(emergencyContact);
        dest.writeString(emergencyRelationship);
        dest.writeString(email);
        dest.writeString(gender);
        dest.writeString(birthdate);
        dest.writeString(province);
        dest.writeString(province_code);
        dest.writeString(city);
        dest.writeString(cityCode);
        dest.writeString(brgy);
        dest.writeString(brgyCode);
        dest.writeString(streetName);
        dest.writeString(zipcode);
        dest.writeString(tinNo);
        dest.writeString(sssNo);
        dest.writeString(phicNo);
        dest.writeString(company);
        dest.writeString(address);
        dest.writeString(donor_type);
        dest.writeString(businessEmail);
        dest.writeString(businessPhoneNumber);
        dest.writeString(secDti);
        dest.writeString(prcLicense);
        dest.writeString(doh);
        dest.writeString(dfa);
    }

    public static class DateCreated {
        @SerializedName("date_db")
        public String dateDb;

        @SerializedName("month_year")
        public String monthYear;

        @SerializedName("time_passed")
        public String timePassed;

        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class Avatar {
        @SerializedName("path")
        public String path;

        @SerializedName("filename")
        public String filename;

        @SerializedName("directory")
        public String directory;

        @SerializedName("full_path")
        public String fullPath;

        @SerializedName("thumb_path")
        public String thumbPath;
    }

    public static class ValidID {
        @SerializedName("path")
        public String path;

        @SerializedName("filename")
        public String filename;

        @SerializedName("directory")
        public String directory;

        @SerializedName("full_path")
        public String fullPath;

        @SerializedName("thumb_path")
        public String thumbPath;
    }
}
