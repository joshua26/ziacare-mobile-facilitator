package com.ziacare.facilitator.data.model.api;

import android.graphics.Bitmap;

import com.ziacare.facilitator.vendor.android.base.AndroidModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SampleModel extends AndroidModel {

    @SerializedName("qrcode")
    public String qrcode;
    @SerializedName("name")
    public String name;
    @SerializedName("address")
    public String address;
    @SerializedName("id_no")
    public String idNo;
    @SerializedName("dob")
    public String dob;
    @SerializedName("date_issued")
    public String dateIssued;
    @SerializedName("gender")
    public String gender;
    @SerializedName("email")
    public String email;
    @SerializedName("mobile_no")
    public String mobileNo;
    @SerializedName("tel_no")
    public String telNo;
    @SerializedName("label")
    public String label;
    @SerializedName("date")
    public String date;
    @SerializedName("total")
    public String total;
    @SerializedName("type")
    public String type;
    @SerializedName("bitmap")
    public Bitmap bitmap;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public SampleModel convertFromJson(String json) {
        return convertFromJson(json, SampleModel.class);
    }
}
