package com.ziacare.facilitator.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.ziacare.facilitator.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class UserModel extends AndroidModel {

    @SerializedName("user_id")
    public int userId;

    @SerializedName("qrcode")
    public String qrcode;

    @SerializedName("organization_id")
    public int organization_id;

    @SerializedName("role")
    public String role;

    @SerializedName("user_type")
    public String user_type;

    @SerializedName("type")
    public String type;

    @SerializedName("code")
    public String code;

    @SerializedName("title")
    public String title;

    @SerializedName("firstname")
    public String firstname;

    @SerializedName("lastname")
    public String lastname;

    @SerializedName("middlename")
    public String middlename;

    @SerializedName("name")
    public String name;

    @SerializedName("username")
    public String username;

    @SerializedName("phone_number")
    public String phone_number;

    @SerializedName("tel_no")
    public String tel_no;

    @SerializedName("email")
    public String email;

    @SerializedName("recovery_email")
    public String recovery_email;

    @SerializedName("gender")
    public String gender;

    @SerializedName("birthdate")
    public String birthday;


    @SerializedName("city")
    public String city;

    @SerializedName("city_code")
    public String cityCode;

    @SerializedName("brgy")
    public String brgy;

    @SerializedName("brgy_code")
    public String brgyCode;

    @SerializedName("street_name")
    public String streetName;

    @SerializedName("zipcode")
    public String zipcode;

    @SerializedName("tin_no")
    public String tinNo;

    @SerializedName("sss_no")
    public String sssNo;

    @SerializedName("phic_no")
    public String phicNo;

    @SerializedName("company")
    public String company;

    @SerializedName("address")
    public String address;

    @SerializedName("business_email")
    public String businessEmail;

    @SerializedName("business_phone_number")
    public String businessPhoneNumber;

    @SerializedName("sec_dti")
    public String secDti;

    @SerializedName("prc_license")
    public String prcLicense;

    @SerializedName("doh")
    public String doh;

    @SerializedName("dfa")
    public String dfa;

    @SerializedName("is_vaccinated")
    public String is_vaccinated;

    @SerializedName("covid_status")
    public String covid_status;

    @SerializedName("donor_type")
    public String donor_type;

    @SerializedName("sub_type")
    public String sub_type;

    @SerializedName("emergency_name")
    public String emergency_name;

    @SerializedName("emergency_contact")
    public String emergency_contact;

    @SerializedName("emergency_relationship")
    public String emergency_relationship;

    @SerializedName("total_credit")
    public String total_credit;

    @SerializedName("total_voucher")
    public String total_voucher;

    @SerializedName("total_donated_amount")
    public String total_donated_amount;

    @SerializedName("current_balance")
    public String current_balance;

    @SerializedName("available_balance")
    public String available_balance;

    @SerializedName("date_created")
    public DateCreated dateCreated;

    @SerializedName("valid_id")
    public ValidID validID;

    @SerializedName("avatar")
    public Avatar avatar;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public UserModel convertFromJson(String json) {
        return convertFromJson(json, UserModel.class);
    }

    public static class DateCreated {
        @SerializedName("date_db")
        public String dateDb;

        @SerializedName("month_year")
        public String monthYear;

        @SerializedName("time_passed")
        public String timePassed;

        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class Avatar{
        @SerializedName("path")
        public String path;

        @SerializedName("filename")
        public String filename;

        @SerializedName("directory")
        public String directory;

        @SerializedName("full_path")
        public String full_path;

        @SerializedName("thumb_path")
        public String thumb_path;
    }

    public static class ValidID{
        @SerializedName("path")
        public String path;

        @SerializedName("filename")
        public String filename;

        @SerializedName("directory")
        public String directory;

        @SerializedName("full_path")
        public String fullPath;

        @SerializedName("thumb_path")
        public String thumbPath;
    }

}
