package com.ziacare.facilitator.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.ziacare.facilitator.vendor.android.base.AndroidModel;

import java.util.List;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class RequestModel extends AndroidModel {

    @SerializedName("code")
    public String code;
    @SerializedName("name")
    public String name;
    @SerializedName("amount")
    public String amount;
    @SerializedName("remaining_amount")
    public String remainingAmount;
    @SerializedName("donated_amount")
    public String donatedAmount;
    @SerializedName("display_amount")
    public String displayAmount;
    @SerializedName("display_remaining_amount")
    public String displayRemainingAmount;
    @SerializedName("display_donated_amount")
    public String displayDonatedAmount;
    @SerializedName("type")
    public String type;
    @SerializedName("status")
    public String status;
    @SerializedName("remarks")
    public String remarks;
    @SerializedName("direct_request")
    public boolean directRequest;
    @SerializedName("date_created")
    public DateCreated dateCreated;
    @SerializedName("biller")
    public Biller biller;
    @SerializedName("billed")
    public Billed billed;
    @SerializedName("attachment")
    public Attachment attachment;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public RequestModel convertFromJson(String json) {
        return convertFromJson(json, RequestModel.class);
    }

    public static class DateCreated {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class Biller {
        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("user_id")
            public int userId;
            @SerializedName("organization_id")
            public int organizationId;
            @SerializedName("role")
            public String role;
            @SerializedName("type")
            public String type;
            @SerializedName("code")
            public String code;
            @SerializedName("firstname")
            public String firstname;
            @SerializedName("lastname")
            public String lastname;
            @SerializedName("middlename")
            public String middlename;
            @SerializedName("name")
            public String name;
            @SerializedName("username")
            public String username;
            @SerializedName("phone_number")
            public String phoneNumber;
            @SerializedName("tel_no")
            public String telNo;
            @SerializedName("email")
            public String email;
            @SerializedName("gender")
            public String gender;
            @SerializedName("birthdate")
            public String birthdate;
            @SerializedName("city")
            public String city;
            @SerializedName("city_code")
            public String cityCode;
            @SerializedName("brgy")
            public String brgy;
            @SerializedName("brgy_code")
            public String brgyCode;
            @SerializedName("street_name")
            public String streetName;
            @SerializedName("zipcode")
            public String zipcode;
            @SerializedName("tin_no")
            public String tinNo;
            @SerializedName("sss_no")
            public String sssNo;
            @SerializedName("phic_no")
            public String phicNo;
            @SerializedName("company")
            public String company;
            @SerializedName("address")
            public String address;
            @SerializedName("business_email")
            public String businessEmail;
            @SerializedName("business_phone_number")
            public String businessPhoneNumber;
            @SerializedName("sec_dti")
            public String secDti;
            @SerializedName("prc_license")
            public String prcLicense;
            @SerializedName("doh")
            public String doh;
            @SerializedName("dfa")
            public String dfa;
            @SerializedName("emergency_name")
            public String emergencyName;
            @SerializedName("emergency_contact")
            public String emergencyContact;
            @SerializedName("emergency_relationship")
            public String emergencyRelationship;
            @SerializedName("total_credit")
            public String totalCredit;
            @SerializedName("total_voucher")
            public String totalVoucher;
            @SerializedName("total_donated_amount")
            public String totalDonatedAmount;
            @SerializedName("current_balance")
            public String currentBalance;
            @SerializedName("available_balance")
            public String availableBalance;
            @SerializedName("date_created")
            public DateCreatedX dateCreated;
            @SerializedName("avatar")
            public Avatar avatar;
            @SerializedName("valid_id")
            public ValidId validId;

            public static class DateCreatedX {
                @SerializedName("date_db")
                public String dateDb;
                @SerializedName("month_year")
                public String monthYear;
                @SerializedName("time_passed")
                public String timePassed;
                @SerializedName("timestamp")
                public String timestamp;
            }

            public static class Avatar {
                @SerializedName("path")
                public String path;
                @SerializedName("filename")
                public String filename;
                @SerializedName("directory")
                public String directory;
                @SerializedName("full_path")
                public String fullPath;
                @SerializedName("thumb_path")
                public String thumbPath;
            }

            public static class ValidId {
                @SerializedName("path")
                public String path;
                @SerializedName("filename")
                public String filename;
                @SerializedName("directory")
                public String directory;
                @SerializedName("full_path")
                public String fullPath;
                @SerializedName("thumb_path")
                public String thumbPath;
            }
        }
    }

    public static class Billed {
        @SerializedName("data")
        public DataX data;

        public static class DataX {
            @SerializedName("user_id")
            public int userId;
            @SerializedName("organization_id")
            public int organizationId;
            @SerializedName("role")
            public String role;
            @SerializedName("type")
            public String type;
            @SerializedName("code")
            public String code;
            @SerializedName("firstname")
            public String firstname;
            @SerializedName("lastname")
            public String lastname;
            @SerializedName("middlename")
            public String middlename;
            @SerializedName("name")
            public String name;
            @SerializedName("username")
            public String username;
            @SerializedName("phone_number")
            public String phoneNumber;
            @SerializedName("tel_no")
            public String telNo;
            @SerializedName("email")
            public String email;
            @SerializedName("gender")
            public String gender;
            @SerializedName("birthdate")
            public String birthdate;
            @SerializedName("city")
            public String city;
            @SerializedName("city_code")
            public String cityCode;
            @SerializedName("brgy")
            public String brgy;
            @SerializedName("brgy_code")
            public String brgyCode;
            @SerializedName("street_name")
            public String streetName;
            @SerializedName("zipcode")
            public String zipcode;
            @SerializedName("tin_no")
            public String tinNo;
            @SerializedName("sss_no")
            public String sssNo;
            @SerializedName("phic_no")
            public String phicNo;
            @SerializedName("company")
            public String company;
            @SerializedName("address")
            public String address;
            @SerializedName("business_email")
            public String businessEmail;
            @SerializedName("business_phone_number")
            public String businessPhoneNumber;
            @SerializedName("sec_dti")
            public String secDti;
            @SerializedName("prc_license")
            public String prcLicense;
            @SerializedName("doh")
            public String doh;
            @SerializedName("dfa")
            public String dfa;
            @SerializedName("emergency_name")
            public String emergencyName;
            @SerializedName("emergency_contact")
            public String emergencyContact;
            @SerializedName("emergency_relationship")
            public String emergencyRelationship;
            @SerializedName("total_credit")
            public String totalCredit;
            @SerializedName("total_voucher")
            public String totalVoucher;
            @SerializedName("total_donated_amount")
            public String totalDonatedAmount;
            @SerializedName("current_balance")
            public String currentBalance;
            @SerializedName("available_balance")
            public String availableBalance;
            @SerializedName("date_created")
            public DateCreatedXX dateCreated;
            @SerializedName("avatar")
            public AvatarX avatar;
            @SerializedName("valid_id")
            public ValidIdX validId;

            public static class DateCreatedXX {
                @SerializedName("date_db")
                public String dateDb;
                @SerializedName("month_year")
                public String monthYear;
                @SerializedName("time_passed")
                public String timePassed;
                @SerializedName("timestamp")
                public String timestamp;
            }

            public static class AvatarX {
                @SerializedName("path")
                public String path;
                @SerializedName("filename")
                public String filename;
                @SerializedName("directory")
                public String directory;
                @SerializedName("full_path")
                public String fullPath;
                @SerializedName("thumb_path")
                public String thumbPath;
            }

            public static class ValidIdX {
                @SerializedName("path")
                public String path;
                @SerializedName("filename")
                public String filename;
                @SerializedName("directory")
                public String directory;
                @SerializedName("full_path")
                public String fullPath;
                @SerializedName("thumb_path")
                public String thumbPath;
            }
        }
    }

    public static class Attachment {
        @SerializedName("data")
        public List<DataXX> data;

        public static class DataXX {
            @SerializedName("id")
            public int idX;
            @SerializedName("bill_id")
            public int billId;
            @SerializedName("filename")
            public String filename;
            @SerializedName("path")
            public String path;
            @SerializedName("directory")
            public String directory;
            @SerializedName("full_path")
            public String fullPath;
            @SerializedName("thumb_path")
            public String thumbPath;
        }
    }
}
