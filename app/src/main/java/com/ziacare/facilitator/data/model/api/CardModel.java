package com.ziacare.facilitator.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.ziacare.facilitator.vendor.android.base.AndroidModel;

public class CardModel extends AndroidModel {

    @SerializedName("facilitator_id")
    public int facilitator_id;

    @SerializedName("status")
    public String status;

    @SerializedName("reference_code")
    public String reference_code;

    @SerializedName("card_number")
    public String card_number;

    @SerializedName("province_name")
    public String province_name;

    @SerializedName("redeem_at")
    public String redeem_at;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public CardModel convertFromJson(String json) {
        return convertFromJson(json, CardModel.class);
    }

}