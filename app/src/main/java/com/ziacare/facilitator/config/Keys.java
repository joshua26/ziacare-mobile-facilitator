package com.ziacare.facilitator.config;

/**
 * Created by Labyalo on 8/7/2017.
 */

public class Keys {
    public static final String DEVICE_ID = "device_id";
    public static final String DEVICE_NAME = "device_name";
    public static final String DEVICE_REG_ID = "device_reg_id";
    public static final String PAGE = "page";
    public static final String PER_PAGE = "per_page";
    public static final String USER_ID = "user_id";
    public static final String FIRSTNAME = "firstname";
    public static final String MIDDLENAME = "middlename";
    public static final String LASTNAME = "lastname";
    public static final String GENDER = "gender";
    public static final String JOBTITLE = "job_title";
    public static final String COMPANYNAME = "company_name";
    public static final String ADDRESS = "address";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String CONTACTNUMBER = "contact_number";
    public static final String BIRTHDAY = "bday";
    public static final String USERCODE = "user_code";
    public static final String QRCODE = "qrcode";
    public static final String CURRENT_PASSWORD = "current_password";
    public static final String PASSWORD_CONFIRMATION = "password_confirmation";
    public static final String FILE = "file";
    public static final String REFERENCE_CODE = "reference_code";
    public static final String AVATAR = "avatar";
    public static final String FACILITATOR_ID = "facilitator_id";

    public static final String STREET_NAME = "street_name";
    public static final String ZIPCODE = "zipcode";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String BIRTHDATE = "birthdate";
    public static final String PROVINCE_SKU = "province_sku";
    public static final String PROVINCE_NAME = "province_name";
    public static final String CITY_NAME = "city_name";
    public static final String CITY_SKU = "city_sku";
    public static final String BRGY_NAME = "brgy_name";
    public static final String BRGY_SKU = "brgy_sku";
    public static final String USERNAME = "username";
    public static final String PASSWORD_CONFIRM = "password_confirmation";
    public static final String TIN_NO = "tin_no";
    public static final String SSS_NO = "sss_no";
    public static final String PHIC_NO = "phic_no";
    public static final String TYPE = "type";
    public static final String VALID_ID = "valid_id";
    public static final String VIDEO = "video";
    public static final String TEL_NO = "tel_no";
    public static final String EMERGENCY_NAME = "emergency_name";
    public static final String EMERGENCY_CONTACT = "emergency_contact";
    public static final String EMERGENCY_RELATIONSHIP = "emergency_relationship";

    public static final String PWD = "is_pwd";
    public static final String SENIOR = "is_senior";
    public static final String SELFIE = "selfie";

    public static final String FNAME = "firstname";
    public static final String MNAME = "middlename";
    public static final String LNAME = "lastname";
    public static final String STEP = "step";
    public static final String OTP = "otp";

    //face detection
    public static final String IMAGES = "files";
    public static final String LEFT = "left_angle";
    public static final String RIGHT = "right_angle";
    public static final String FRONT = "front_pic";

    //PSGC
    public static final String PSGC_AUTHORIZATION = "HkCrJxGa5M8udwHYbDyMF2kscmzhSVtDwBxjXNPxLETScLAFf5P87zLUxqWTKQ3bRcvLCNd4e9n3xpNCwGxsrfgj6fMRtCsUuxWr6gRAj2kRcVwxKfH5TDx8YWF4r2cP";
    public static final String REFERENCE = "reference";

    //KYC
    public static final String IMAGE = "image";
    public static final String DOCUMENT_TYPE = "document_type";
    public static final String ID_TYPE = "id_type";
    public static final String LEFT_PIC = "left_pic";
    public static final String RIGHT_PIC = "right_pic";
    public static final String FRONT_PIC = "front_pic";
}
