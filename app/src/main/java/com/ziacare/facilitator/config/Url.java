package com.ziacare.facilitator.config;

import com.ziacare.facilitator.vendor.android.java.security.SecurityLayer;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Url extends SecurityLayer {

    public static final String PRODUCTION_URL = decrypt("http://54.251.59.206");
    public static final String DEBUG_URL = decrypt("http://ziacare.ziademo.com");
 //54.251.59.206 - ziacare

    public static final String PSGC_URL = "https://psgc.ziapay.ph";

    public static final String APP = App.production ? PRODUCTION_URL : DEBUG_URL;

    //forgot pass
    public static final String getForgotPass() {return "/api/facilitator/auth/forgot-password.json";}
    public static final String getResetPassword(){return "/api/facilitator/auth/reset-password.json";}
    public static final String getForgotPassOTP(){return "/api/facilitator/auth/forgot-password.json";}

   public static final String getRegisterFacilitator() {return "/api/facilitator/auth/register.json";}

   public static final String getAllRegistered(){return "/api/facilitator/profile/registered-user.json";}

   public static final String getProfileScan(){return "/api/facilitator/auth/scan.json";}

    public static final String getLogin() {return "/api/facilitator/auth/login.json";}

    public static final String getLogout() {return "/api/facilitator/auth/logout.json";}

    public static final String getRefreshToken(){return "/api/facilitator/auth/refresh-token.json";}

    public static final String getUpdatePassword(){return "/api/facilitator/profile/edit-password.json";}

    public static final String getUpdateProfile(){return "/api/facilitator/profile/edit-profile.json";}

    public static final String getUpdateAvatar(){return "/api/facilitator/profile/edit-avatar.json";}

    public static final String getMyProfile(){return "/api/facilitator/profile/show.json";}

    //new create
    public static final String getValidate(){return "/api/facilitator/beneficiary/pre-validate.json";}
    public static final String getCreateAccount(){return "/api/facilitator/beneficiary/create.json";}
    public static final String getOTPVerification(){return "/api/facilitator/beneficiary/otp.json";}

    public static final String getAttachCard(){return "/api/facilitator/beneficiary/activate.json";}

    public static final String getListOfRegistered(){return "/api/facilitator/beneficiary/all.json";}

    //psgc
    public static final String getProvince(){return "/api/v1/psgc/province";}
    public static final String getProvinceInfo(){return "/api/v1/psgc/province/info";}
    public static final String getMunicipality(){return "/api/v1/psgc/citymun";}

    //KYC
    public static final String getValidateSelfie(){return "/api/facilitator/beneficiary/kyc/validate-selfie.json";}
    public static final String getValidateID(){return "/api/facilitator/beneficiary/kyc/validate-id.json";}
    public static final String getValidIDList(){return "/api/setting/valid-id.json"; }
    public static final String getKYC(){return "/api/facilitator/beneficiary/kyc/verify.json";}

    //new kyc
    public static final String uploadID(){return "/api/facilitator/beneficiary/kyc/upload-id.json";}
    public static final String uploadSelfie(){return "/api/facilitator/beneficiary/kyc/upload-selfie.json";}

    //cards
    public static final String getListOfCards(){return "/api/facilitator/card-management/all.json";}
    public static final String getScanCard(){return "/api/facilitator/card-management/scan.json";}

    //for bulk uploads
    public static final String scanRefNo(){return "/api/facilitator/beneficiary/scan.json";}
    public static final String editRecord(){return "/api/facilitator/beneficiary/update.json";}
    public static final String generateNewPassword(){return "/api/facilitator/beneficiary/generate-password.json";}

}
